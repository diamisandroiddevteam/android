package ru.diamis.you_to_me_i_to_you.activities.sales;

import android.support.v4.app.Fragment;

import ru.diamis.you_to_me_i_to_you.DAL.Sale;
import ru.diamis.you_to_me_i_to_you.Interfaces.ICallbacks;

public class SaleListActivity extends SingleFragmentActivity
        implements ICallbacks<Sale> {

    @Override
    protected Fragment createFragment() {
        return new SaleListFragment();
    }
//
//    @Override
//    protected int getLayoutResId() {
//        return R.layout.activity_masterdetail;
//    }
//
    @Override
    public void onSelect(Sale sale) {
//        if (findViewById(R.id.detailFragmentContainer) == null) {
//            // start an instance of CrimePagerActivity
//            Intent intent = new Intent(this, SalePagerActivity.class);
//            intent.putExtra(SaleFragment.EXTRA_SALE_ID, sale.getId());
//            startActivity(intent);
//        } else {
//            FragmentManager fm = getSupportFragmentManager();
//            FragmentTransaction ft = fm.beginTransaction();
//
//            Fragment oldDetail = fm.findFragmentById(R.id.detailFragmentContainer);
//
//            if (oldDetail != null) {
//                ft.remove(oldDetail);
//            }
//
//            if (sale != null) {
//                Fragment newDetail = SaleFragment.newInstance(sale.getId());
//                ft.add(R.id.detailFragmentContainer, newDetail);
//            }
//            ft.commit();
//        }
    }
//
    @Override
    public void onCreate(Sale sale) {
//        Intent intent = new Intent(this, SalesActivity.class);
//        intent.putExtra(SaleFragment.EXTRA_SALE_ID, sale.getId());
//        startActivity(intent);
    }
//
    @Override
    public void onUpdate(Sale crime) {
//        SaleService saleService = new  SaleService();
//                saleService.update(crime);
//
//        FragmentManager fm = getSupportFragmentManager();
//        SaleListFragment listFragment = (SaleListFragment)
//                fm.findFragmentById(R.id.fragmentContainer);
//
    }

    @Override
    public void onDelete(Sale sale) {
//        FragmentManager fm = getSupportFragmentManager();
//        FragmentTransaction ft = fm.beginTransaction();
//
//        Fragment currentDetail = fm.findFragmentById(R.id.detailFragmentContainer);
//
//        if (currentDetail != null) {
//            ft.remove(currentDetail);
//        }
//
//        ft.commit();
    }
}
