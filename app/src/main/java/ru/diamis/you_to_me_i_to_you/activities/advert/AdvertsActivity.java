package ru.diamis.you_to_me_i_to_you.activities.advert;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import ru.diamis.you_to_me_i_to_you.DAL.Adverts.Advert;
import ru.diamis.you_to_me_i_to_you.Enums.AdvertStatusEnum;
import ru.diamis.you_to_me_i_to_you.Filters.AdvertFilter;
import ru.diamis.you_to_me_i_to_you.Interfaces.IAdvertService;
import ru.diamis.you_to_me_i_to_you.Interfaces.OnListFragmentInteractionListener;
import ru.diamis.you_to_me_i_to_you.R;
import ru.diamis.you_to_me_i_to_you.activities.advert.adapters.AdvertListPageAdapter;
import ru.diamis.you_to_me_i_to_you.services.AdvertService;

public class AdvertsActivity extends AppCompatActivity implements OnListFragmentInteractionListener<Advert> {
    public static final String TAG = "AdvertsActivity";
    IAdvertService advertService = new AdvertService();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_adverts);
        this.setTitle(getString(R.string.my_adverts_or_services));
        TabLayout tabLayout = findViewById(R.id.advert_tab_layout);
        AdvertFilter filter = new AdvertFilter();

        filter.SetStatus(AdvertStatusEnum.Confirmed);
        int isActiveAvertsCount  = advertService.GetCount(filter);
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.is_active).concat(" ("+isActiveAvertsCount+")")));

        filter.SetStatus(AdvertStatusEnum.OnModerate);
        int onModerateCount  = advertService.GetCount(filter);
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.on_moderate).concat(" ("+onModerateCount+")")));

        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.is_archived)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.is_deleted)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = findViewById(R.id.advert_view_pager);
        final AdvertListPageAdapter adapter = new AdvertListPageAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });


    }
    @Override
    public void onListFragmentInteraction(Advert item) {
        Toast toast = Toast.makeText(this,item.getUuid().toString(),Toast.LENGTH_SHORT);
        toast.show();

        Intent intent = new Intent(this, AdvertDetailActivity.class);
        intent.putExtra("SELECTED_ADVERT_UUID", item.getUuid());
        intent.putExtra("SELECTED_ADVERT_PRICE", item.getPrice());
        intent.putExtra("SELECTED_ADVERT_DESCRIPTION", item.getDescription());
        startActivity(intent);
    }
}

