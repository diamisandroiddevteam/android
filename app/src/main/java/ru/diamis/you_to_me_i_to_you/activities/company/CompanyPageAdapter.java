package ru.diamis.you_to_me_i_to_you.activities.company;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import ru.diamis.you_to_me_i_to_you.activities.company.fragments.CompanyAddressFragment;
import ru.diamis.you_to_me_i_to_you.activities.company.fragments.CompanyGaleryFragment;
import ru.diamis.you_to_me_i_to_you.activities.company.fragments.CompanyRequisitesFragment;

public class CompanyPageAdapter extends FragmentPagerAdapter {
    int numberOfTabs;
    public CompanyPageAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.numberOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new CompanyRequisitesFragment();
            case 1:
                return new CompanyAddressFragment();
            case 2:
                return new CompanyGaleryFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }
}
