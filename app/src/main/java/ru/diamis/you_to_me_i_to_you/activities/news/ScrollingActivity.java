package ru.diamis.you_to_me_i_to_you.activities.news;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import ru.diamis.you_to_me_i_to_you.R;
import ru.diamis.you_to_me_i_to_you.activities.common.CommonAppActivity;

public class ScrollingActivity extends CommonAppActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        InitBottomAppBar();
    }
}
