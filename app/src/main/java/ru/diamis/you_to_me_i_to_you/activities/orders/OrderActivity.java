package ru.diamis.you_to_me_i_to_you.activities.orders;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.UUID;

import ru.diamis.you_to_me_i_to_you.DAL.Order;
import ru.diamis.you_to_me_i_to_you.R;

public class OrderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_order);

        Intent intent = getIntent();
//

        Order order = new Order();
        if (savedInstanceState != null) {
            UUID uuid = UUID.fromString(intent.getStringExtra("SELECTED_ORDER_UUID"));
        order.setUuid(uuid.toString());
        order.setPrice(intent.getDoubleExtra("SELECTED_ORDER_PRICE", 0.0));
        order.setDescription(intent.getStringExtra("SELECTED_ORDER_DESCRIPTION"));
        // order.setCreateDate(DateTimeHelper.getFormattedDate(intent.getStringExtra("SELECTED_ORDER_CREATE_DATE"));
        } else order = new Order();
        TextView txtViewItemNumber = findViewById(R.id.order_item_number);
        TextView txtViewItemContent = findViewById(R.id.order_description);
        TextView txtViewOrderPrice = findViewById(R.id.textViewOrderPrice);
        TextView textViewOrderStatus = findViewById(R.id.textViewOrderStatus);
        TextView textViewOrderCreateDate = findViewById(R.id.textViewOrderCreateDate);


        txtViewItemNumber.setText(new StringBuilder().append(getString(R.string.order)).append(" № ").append(order.getUuid().toString()).toString());
        txtViewItemContent.setText(order.getDescription());
        txtViewOrderPrice.setText(new StringBuilder().append(order.getPrice()));
        textViewOrderStatus.setText(new StringBuilder().append(order.getOrderState()));
        textViewOrderCreateDate.setText(new StringBuilder().append(order.getOrderState()));


        Button btnAcceptOrder = findViewById(R.id.btnAcceptOrder);
        btnAcceptOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: AcceptOrder request
                Toast.makeText(getApplicationContext(), getString(R.string.will_be_in_next_version), Toast.LENGTH_SHORT).show();
            }
        });

        Button btnRepeatOrder = findViewById(R.id.btnRepeatOrder);
        btnRepeatOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: RepeatOrder request
                Toast.makeText(getApplicationContext(), getString(R.string.will_be_in_next_version), Toast.LENGTH_SHORT).show();
            }
        });
        Button btnOpenChat = findViewById(R.id.btnOpenDisput);
        btnOpenChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: OpenChat with current customer
                Toast.makeText(getApplicationContext(), getString(R.string.will_be_in_next_version), Toast.LENGTH_SHORT).show();
            }
        });



    }
}
