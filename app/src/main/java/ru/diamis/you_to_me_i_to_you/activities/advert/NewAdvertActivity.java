package ru.diamis.you_to_me_i_to_you.activities.advert;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import ru.diamis.you_to_me_i_to_you.DAL.Category;
import ru.diamis.you_to_me_i_to_you.R;
import ru.diamis.you_to_me_i_to_you.activities.common.CommonAppActivity;
import ru.diamis.you_to_me_i_to_you.activities.filter.FilterAdapter;
import ru.diamis.you_to_me_i_to_you.services.CategoryService;

public class NewAdvertActivity extends CommonAppActivity {
    public static final String TAG = "NewAdvertActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_advert);
        CategoryService categoryService = new CategoryService();
        List<Category> answer = categoryService.Get(null);

        String[] categoryNamesArray= new String [answer.size()];
        for (int i=0; i<answer.size(); i++){
            Category category = answer.get(i);
            categoryNamesArray[i]=category.getName();
            Log.i(TAG,"data["+i+"] "+  categoryNamesArray[i]);
        }
        ArrayAdapter<String> adapterCategoryData = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,  categoryNamesArray);
        adapterCategoryData.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        Spinner spinnerCategory = findViewById(R.id.spinner);
        spinnerCategory.setAdapter(adapterCategoryData);
        spinnerCategory.setPrompt("Title");
        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
             Toast.makeText(getApplicationContext(),"Selected " + position,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });


        Button btnPublish = findViewById(R.id.btnPublish);
        btnPublish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), getString(R.string.will_be_in_next_version), Toast.LENGTH_SHORT).show();
            }
        });

        InitBottomAppBar();
    }


}
