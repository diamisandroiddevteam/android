package ru.diamis.you_to_me_i_to_you.activities.sales;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;


import java.util.List;
import java.util.UUID;

import ru.diamis.you_to_me_i_to_you.DAL.Sale;
import ru.diamis.you_to_me_i_to_you.Interfaces.ICallbacks;
import ru.diamis.you_to_me_i_to_you.R;
import ru.diamis.you_to_me_i_to_you.services.SaleService;

public class SalePagerActivity extends AppCompatActivity implements ICallbacks<Sale> {
    private ViewPager mViewPager;
    private List<Sale> sales;
    SaleService saleService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewPager = new ViewPager(this);
        mViewPager.setId(R.id.viewPager);
        setContentView(mViewPager);
        saleService = new SaleService();
        sales =   saleService.getItems();

        FragmentManager fm = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fm) {
            @Override
            public Fragment getItem(int position) {
                //Sale sale = saleService.Get(position);
                return SaleFragment.newInstance(0);
            }

            @Override
            public int getCount() {
                return saleService.getItems().size();
            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }

            @Override
            public void onPageSelected(int position) {

               // setCrimeTitle(saleService.Get(position));
            }
        });

        final UUID crimeId = (UUID) getIntent().getSerializableExtra(SaleFragment.EXTRA_SALE_ID);
        for (int i = 0; i < saleService.getItems().size(); i++) {
            if (saleService.getItems().get(i).getUuid().equals(crimeId)) {
                mViewPager.setCurrentItem(i); // it is an initial pager item
               // setCrimeTitle(saleService.Get(i));
                break;
            }
        }
    }

    private void setCrimeTitle(Sale sale) {
        if ((sale.getTitle() != null)) {
            setTitle(sale.getTitle()); // set the title of the current pager item
        } else {
            setTitle("");
        }
    }

    @Override
    public void onSelect(Sale sale) {

    }

    @Override
    public void onCreate(Sale sale) {

    }

    @Override
    public void onUpdate(Sale sale) {
        saleService.update(sale);
    }

    @Override
    public void onDelete(Sale sale) {
        saleService.Delete(sale);
    }


}
