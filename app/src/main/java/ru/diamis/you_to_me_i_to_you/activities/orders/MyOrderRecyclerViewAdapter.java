package ru.diamis.you_to_me_i_to_you.activities.orders;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import ru.diamis.you_to_me_i_to_you.DAL.Order;
import ru.diamis.you_to_me_i_to_you.Enums.OrderStateEnum;
import ru.diamis.you_to_me_i_to_you.Interfaces.OnListFragmentInteractionListener;
import ru.diamis.you_to_me_i_to_you.R;

public class MyOrderRecyclerViewAdapter extends RecyclerView.Adapter<MyOrderRecyclerViewAdapter.ViewHolder> {

    private final List<Order> mValues;
    private final OnListFragmentInteractionListener mListener;

    MyOrderRecyclerViewAdapter(List<Order> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_order, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Resources resources = holder.mView.getContext().getResources();
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(String.format("№ %s", mValues.get(position).getUuid().toString()));
        holder.mOrderDescription.setText(mValues.get(position).getDescription());
        String orderState;
        OrderStateEnum orderStateEnum = mValues.get(position).getOrderState();
        switch (orderStateEnum) {
            case Opened:
                orderState = resources.getString(R.string.order_opened);
                break;
            case Confirmed:
                orderState = resources.getString(R.string.order_confirmed);
                break;
            case Processed:
                orderState = resources.getString(R.string.order_processed);
                break;
            case Paid:
                orderState = resources.getString(R.string.order_paid);
                break;
            case Canceled:
                orderState = resources.getString(R.string.order_canceled);
                break;
            default:
                orderState = resources.getString(R.string.order_unsuspected);
                break;
        }
        holder.mOrderState.setText(String.format("%s: %s", resources.getString(R.string.order_state), orderState));
        String price = String.valueOf(String.format("%,.2f", (mValues.get(position).getPrice()))) + " " + resources.getString(R.string.rub);
        holder.mOrderPrice.setText(price);

        holder.mOrderCreateDate.setText(String.format("%s :%s", resources.getString(R.string.createdate), mValues.get(position).getCreateDate()));
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView mIdView;

        final TextView mOrderState;
        final TextView mOrderPrice;
        final TextView mOrderCreateDate;
        final TextView mOrderDescription;
        public final Button btnAcceptOrder;
        public final Button  btnRepeatOrder;
        public final Button  btnOpenDisput;
        public Order mItem;

        ViewHolder(View view) {
            super(view);
            final Resources res = view.getContext().getResources();
            mView = view;
            mIdView = view.findViewById(R.id.order_item_number);
            mOrderState = view.findViewById(R.id.textViewOrderStatus);
            mOrderPrice = view.findViewById(R.id.textViewOrderPrice);
            mOrderCreateDate = view.findViewById(R.id.textViewOrderCreateDate);
            mOrderDescription = view.findViewById(R.id.order_description);
            btnAcceptOrder = view.findViewById(R.id.btnAcceptOrder);
           // btnAcceptOrder.setBackgroundColor(res.getColor(R.color.button_color));
            btnAcceptOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //TODO: AcceptOrder request
                    Toast.makeText(v.getContext(), res.getString(R.string.will_be_in_next_version), Toast.LENGTH_SHORT).show();

                }
            });

            btnRepeatOrder= view.findViewById(R.id.btnRepeatOrder);
           // btnRepeatOrder.setBackgroundColor(res.getColor(R.color.button_color));
            btnRepeatOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //TODO: RepeatOrder request
                    Toast.makeText(v.getContext(), res.getString(R.string.will_be_in_next_version), Toast.LENGTH_SHORT).show();
                }
            });

            btnOpenDisput= view.findViewById(R.id.btnOpenDisput);
          //  btnOpenDisput.setBackgroundColor(res.getColor(R.color.button_color));
            btnOpenDisput.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //TODO: OpenChat with current customer
                    Toast.makeText(v.getContext(), res.getString(R.string.will_be_in_next_version), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
