package ru.diamis.you_to_me_i_to_you.activities.advert.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import ru.diamis.you_to_me_i_to_you.activities.advert.AdvertsListFragment;

public class AdvertListPageAdapter extends FragmentPagerAdapter {
    int numberOfTabs;
    List<Fragment> fragments;

    public AdvertListPageAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.numberOfTabs = NumOfTabs;
        fragments = new ArrayList<Fragment>();
        fragments.add( new AdvertsListFragment());
        fragments.add( new AdvertsListFragment());
        fragments.add( new AdvertsListFragment());
        fragments.add( new AdvertsListFragment());
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
            case 1:
            case 2:
            case 3:
                return fragments.get(position);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }
}
