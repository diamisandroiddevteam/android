package ru.diamis.you_to_me_i_to_you.activities.common;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import ru.diamis.you_to_me_i_to_you.R;
import ru.diamis.you_to_me_i_to_you.activities.account.CategoryActivity;
import ru.diamis.you_to_me_i_to_you.activities.account.UserActivity;
import ru.diamis.you_to_me_i_to_you.activities.advert.AdvertsActivity;
import ru.diamis.you_to_me_i_to_you.activities.advert.NewAdvertActivity;
import ru.diamis.you_to_me_i_to_you.activities.chat.MemberListActivity;
import ru.diamis.you_to_me_i_to_you.activities.company.InfoActivity;
import ru.diamis.you_to_me_i_to_you.activities.news.NewsActivity;

public class CommonAppActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void runActivity(Class<?> cls) {
        Intent intent = new Intent(getBaseContext(), cls);
        startActivity(intent);
    }

    private void runActivity(AppCompatActivity activity) {
        Intent intent = new Intent(getBaseContext(), activity.getClass());
        startActivity(intent);
    }

    //TODO decompise and move to mvp
    protected void InitHeaderAppBar() {
        ProcessUserLogo(findViewById(R.id.toolbar_logo_user));
        ProcessMenu(findViewById(R.id.toolbar_logo_menu));
    }

    private void ProcessUserLogo(View view) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), UserActivity.class);
                startActivity(intent);
            }
        });
    }

    private void ProcessMenu(View view) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CategoryActivity.class);
                startActivity(intent);
            }
        });
    }

    //
    protected void InitBottomAppBar() {
        BottomNavigationViewEx bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.enableAnimation(false);
        bottomNavigationView.enableShiftingMode(false);
        bottomNavigationView.enableItemShiftingMode(false);
        bottomNavigationView.setTextVisibility(false);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.will_be_in_next_version), Toast.LENGTH_LONG);

                switch (item.getItemId()) {
                    case R.id.action_user_info:
                        startActivity(new Intent(getApplicationContext(), InfoActivity.class));
                        break;
                    case R.id.action_news:
                        //TODO check
                        startActivity(new Intent(getApplicationContext(), NewsActivity.class));
                        break;
                    case R.id.action_new_item:
                        //TODO check
                        startActivity(new Intent(getApplicationContext(), NewAdvertActivity.class));
                        break;
                    case R.id.action_favourite:
                        //TODO check
                        startActivity(new Intent(getApplicationContext(), AdvertsActivity.class));
                        break;
                    case R.id.action_chat:
                        //TODO check
                        startActivity(new Intent(getApplicationContext(), MemberListActivity.class));
                        break;
                    default:
                        toast.show();
                        break;
                }
                return false;
            }
        });
    }

}
