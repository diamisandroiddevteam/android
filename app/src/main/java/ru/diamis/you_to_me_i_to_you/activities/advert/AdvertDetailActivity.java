package ru.diamis.you_to_me_i_to_you.activities.advert;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ShareCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ShareActionProvider;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.plus.PlusShare;

import java.util.UUID;

import ru.diamis.you_to_me_i_to_you.DAL.Adverts.Advert;
import ru.diamis.you_to_me_i_to_you.DAL.User;
import ru.diamis.you_to_me_i_to_you.Filters.AdvertFilter;
import ru.diamis.you_to_me_i_to_you.Filters.UserFilter;
import ru.diamis.you_to_me_i_to_you.R;
import ru.diamis.you_to_me_i_to_you.activities.chat.ChatActivity;
import ru.diamis.you_to_me_i_to_you.activities.common.CommonAppActivity;
import ru.diamis.you_to_me_i_to_you.services.AdvertService;
import ru.diamis.you_to_me_i_to_you.services.UsersService;

public class AdvertDetailActivity extends CommonAppActivity {
    public static final String TAG = "AdvertDetailActivity";
    private ShareActionProvider mShareActionProvider;
    AdvertService advertService = new AdvertService();
    UsersService usersService = new UsersService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_advert);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Advert advert = new Advert();
        Intent intent = getIntent();
//        if(bundle.getSerializable("SELECTED_ADVERT")!= null)
//        {
//            advert = (Advert) bundle.getSerializable("SELECTED_ADVERT");
//            if (advert==null){
//                Toast.makeText(getApplicationContext(),"error in receive advert",Toast.LENGTH_SHORT).show();
//                Log.e(TAG,"error in receive advert");
//
//            }
//        } else {

//        }
if (savedInstanceState!=null) {
    String uuid = savedInstanceState.getString("SELECTED_ADVERT_UUID");
    advert.setUuid(UUID.fromString(uuid));
    advert.setPrice(savedInstanceState.getDouble("SELECTED_ADVERT_PRICE", 0.0));
    advert.setDescription(savedInstanceState.getString("SELECTED_ADVERT_DESCRIPTION"));
    advert.setExtendedDescription(savedInstanceState.getString("SELECTED_ADVERT_EXTENDED_DESCRIPTION"));
    //advert.setCreateDate(intent.getData("SELECTED_ADVERT_CREATE_DATE"));
//            advert.setLastUpdateDate(intent.getData("SELECTED_ADVERT_UPDATE_DATE"));
    advert.setViewCount(savedInstanceState.getInt("SELECTED_ADVERT_VIEW_COUNT", 0));

}
else advert = new Advert();
        Resources resources = getResources();
        ImageView imageView = findViewById(R.id.advertDetailImage);
        TextView txtViewItemNumber =  findViewById(R.id.adverts_item_number);
        TextView txtViewItemContent = findViewById(R.id.advert_content);
        txtViewItemNumber.setText(advert.getUuid().toString());
        txtViewItemContent.setText(advert.getDescription());
        TextView advertPrice = findViewById(R.id.textViewPrice);
        advertPrice.setText(String.format("%s %s", String.valueOf(String.format("%,.2f", (advert.getPrice()))), resources.getString(R.string.rub)));

        Button btnCall = findViewById(R.id.btnCall);
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Advert currentAdvert = advertService.Get(new AdvertFilter()).get(0);
                UserFilter userfilter = new UserFilter();
                userfilter.SetId(currentAdvert.getOwnerId());
                User user = usersService.Get(userfilter).get(0);
                try
                {
                    Intent callContact = new Intent(Intent.ACTION_DIAL,
                            Uri.parse("tel:" + user.getPhone()));
                    startActivity(callContact);
                } catch(Exception e){
                    Log.e(TAG,e.getMessage());
                    Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.not_find_supported_application), Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        });


        Button btnChat = findViewById(R.id.btnChat);
        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO open chat with current supplier
               startActivity(new Intent(getApplicationContext(), ChatActivity.class));
            }
        });

        Button btnComplain = findViewById(R.id.btnComplain);
        btnComplain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO btnComplain request
                Toast.makeText(getApplicationContext(), getString(R.string.will_be_in_next_version), Toast.LENGTH_SHORT).show();
            }
        });

        Button btnSubscribeOnSame = findViewById(R.id.btnSubscribeOnSame);
        btnSubscribeOnSame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO SubscribeOnSame request
                Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.will_be_in_next_version), Toast.LENGTH_SHORT);
                toast.show();
            }
        });



        UUID currentAdvertUuid = advert.getUuid();
        try {
            // currentAdvertUuid = UUID.fromString(bundle.getString("UUID"));
            Log.d(TAG,"current uuid = "+currentAdvertUuid);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            //TODO delete after correct resend of intents;
            currentAdvertUuid = UUID.randomUUID();
        }

        AdvertFilter advertFilter = new AdvertFilter();
        advertFilter.SetId(currentAdvertUuid);
        int AllCustomerAdvertsCount = advertService.GetCount(advertFilter);
        Button btnAllCustomerAdverts = findViewById(R.id.btnAllCustomerAdverts);
        btnAllCustomerAdverts.setText(btnAllCustomerAdverts.getText() + " " + "(" + AllCustomerAdvertsCount + ")");
        btnAllCustomerAdverts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),AllCustomerAdvertsActivity.class);
                intent.putExtra("USER_UUID","");
                startActivity(intent);
            }
        });

        Button btnBarter = findViewById(R.id.btnBarter);
        btnBarter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),ChatActivity.class);
                startActivity( intent);
            }
        });
        ImageButton vkShare = findViewById(R.id.imageViewShareVk);
        vkShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO share with vk
                Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.will_be_in_next_version), Toast.LENGTH_LONG);
                toast.show();
            }

        });
        ImageButton OkShare = findViewById(R.id.imageViewShareOk);
        OkShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO share with ok
                Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.will_be_in_next_version), Toast.LENGTH_LONG);
                toast.show();
            }
        });
        ImageButton GooglePlusShare = findViewById(R.id.imageViewShareGooglePlus);
        GooglePlusShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent shareIntent = new PlusShare.Builder(getApplicationContext())
                        .setType("text/plain")
                        .setText("Welcome to the Google+ platform.")
                        .setContentUrl(Uri.parse("https://developers.google.com/+/"))
                        .getIntent();

                startActivityForResult(shareIntent, 0);
            }
        });

        ImageButton FacebookShare = findViewById(R.id.imageViewShareFacebook);
        FacebookShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO share with facebook
                Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.will_be_in_next_version), Toast.LENGTH_LONG);
                toast.show();
            }
        });

        TabLayout tabLayout = findViewById(R.id.tabLayoutAdvertInfo);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Fragment fragment = null;
                switch (tab.getPosition()){
                    case 0: fragment = new MapFragment();
                    break;
                }
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
              //  ft.replace(R.id.tab_layout, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.commit();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        // Call to update the share intent

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent intent = new Intent(getApplicationContext(),ChatActivity.class);
                startActivity(intent);
            }
        });

        Button btnShare = findViewById(R.id.btnShare);
        btnShare.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                Share();
            }
        });

    }
    private String getShareString() {
        String  shareString="suck my dick";
        return shareString;
    }
    private void Share(){
        Intent intent = ShareCompat.IntentBuilder.from(this)
                .setText(getShareString())
                .setSubject("hui")
                .setType("text/plain")
                .getIntent();
        intent = Intent.createChooser(intent, getString(R.string.send_report));
        startActivity(intent);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate menu resource file.
        getMenuInflater().inflate(R.menu.activity_info_share_toolbar, menu);

        // Locate MenuItem with ShareActionProvider
        MenuItem item = menu.findItem(R.id.menu_item_share);

        // Fetch and store ShareActionProvider
        mShareActionProvider = (ShareActionProvider) item.getActionProvider();

        // Return true to display menu
        return true;
    }

    private void setShareIntent(Intent shareIntent) {
        if (mShareActionProvider != null) {
            mShareActionProvider.setShareIntent(shareIntent);
        }
    }
}
