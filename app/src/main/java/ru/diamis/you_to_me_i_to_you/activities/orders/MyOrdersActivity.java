package ru.diamis.you_to_me_i_to_you.activities.orders;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.widget.Toast;

import ru.diamis.you_to_me_i_to_you.DAL.Order;
import ru.diamis.you_to_me_i_to_you.Interfaces.OnListFragmentInteractionListener;
import ru.diamis.you_to_me_i_to_you.R;
import ru.diamis.you_to_me_i_to_you.activities.common.CommonAppActivity;

public class MyOrdersActivity extends CommonAppActivity implements OnListFragmentInteractionListener<Order> {
    public static final String TAG = "MyOrdersActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);
        InitHeaderAppBar();
        TabLayout tabLayout = findViewById(R.id.order_tab_layout);

        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.list_of_buys)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.list_of_sales)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = findViewById(R.id.order_view_pager);
        final OrderPageAdapter adapter = new OrderPageAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        InitBottomAppBar();
    }

    @Override
    public void onListFragmentInteraction(Order item) {
        Toast toast = Toast.makeText(this,item.getUuid().toString(),Toast.LENGTH_SHORT);
        toast.show();

        Intent intent = new Intent(this,OrderActivity.class);
        intent.putExtra("SELECTED_ORDER_UUID", item.getUuid());
        intent.putExtra("SELECTED_ORDER_PRICE", item.getPrice());
        intent.putExtra("SELECTED_ORDER_CREATE_DATE", item.getCreateDate());
        intent.putExtra("SELECTED_ORDER_DESCRIPTION", item.getDescription());
        intent.putExtra("SELECTED_ORDER_STATE", item.getOrderState());

        startActivity(intent);


    }
}


