package ru.diamis.you_to_me_i_to_you.activities.filter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import ru.diamis.you_to_me_i_to_you.activities.filter.FragmentFilters.Agriculture.AgricultureFilterCommonFragment;
import ru.diamis.you_to_me_i_to_you.activities.filter.FragmentFilters.AutoFilterFragment;
import ru.diamis.you_to_me_i_to_you.activities.filter.FragmentFilters.ClothesFilterFragment;
import ru.diamis.you_to_me_i_to_you.activities.filter.FragmentFilters.CommonFilterFragment;
import ru.diamis.you_to_me_i_to_you.activities.filter.FragmentFilters.EstateFilterFragment;

public class FilterAdapter extends FragmentPagerAdapter {
    int numberOfTabs;
    public FilterAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.numberOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new AgricultureFilterCommonFragment();
            case 1:
                return new EstateFilterFragment();
            case 3:
                return new AutoFilterFragment();
            case 8:
            case 9:
                return new ClothesFilterFragment();
            default:
                return new CommonFilterFragment();
        }
    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }
}
