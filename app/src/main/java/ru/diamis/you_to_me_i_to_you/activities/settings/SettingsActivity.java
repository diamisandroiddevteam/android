package ru.diamis.you_to_me_i_to_you.activities.settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

import java.util.UUID;

import ru.diamis.you_to_me_i_to_you.DAL.User;
import ru.diamis.you_to_me_i_to_you.Exception.ServiceException;
import ru.diamis.you_to_me_i_to_you.Filters.UserFilter;
import ru.diamis.you_to_me_i_to_you.Helpers.ToastHelper;
import ru.diamis.you_to_me_i_to_you.R;
import ru.diamis.you_to_me_i_to_you.activities.account.TechSupportActivity;
import ru.diamis.you_to_me_i_to_you.activities.auth.RegistryActivity;
import ru.diamis.you_to_me_i_to_you.activities.common.CommonAppActivity;
import ru.diamis.you_to_me_i_to_you.services.UsersService;

public class SettingsActivity extends CommonAppActivity implements CompoundButton.OnCheckedChangeListener {
    public static final String TAG = "SettingsActivity";
    UsersService usersService;
    Button btnTechSupport;
    Button btnLogOut;
    String newString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                newString = null;
            } else {
                newString = extras.getString("USER_UUID");
            }
        } else {
            newString = (String) savedInstanceState.getSerializable("USER_UUID");
        }
        Context context = getApplicationContext();
        ToastHelper.ShowMessage(context, "UUID is: " + newString);

        processSwitch(R.id.switchSales);
        processSwitch(R.id.switchNews);
        processSwitch(R.id.switchAdverts);


        btnTechSupport = findViewById(R.id.btnTechSupport);
        btnTechSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),TechSupportActivity.class));

            }
        });
        btnLogOut = findViewById(R.id.btnLogOut);
        btnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserFilter filter = new UserFilter();
                try {
                    if (newString != null)
                        if (!newString.isEmpty()) {
                            filter.SetId(UUID.fromString(newString));
                            usersService.Delete(filter);

                            User user = usersService.Get(filter).get(0);
                            if (user != null) {
                                if ((user.getUuid().toString().equals(newString)) && (user.isDeleted())) {
                                    runActivity(RegistryActivity.class);
                                } else {
                                    throw new ServiceException("Error in delete user ");
                                }
                            }
                    }
                } catch (ServiceException e) {
                    Log.e(TAG, e.getMessage());
                }

            }
        });

        Button saveChanges = findViewById(R.id.btnSavePasswordChanges);

        saveChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO Сохранить измененния Request
                ToastHelper.ShowMessage(getApplicationContext(),getString(R.string.changes_are_saved));
            }
        });

    }

    private void processSwitch(int id) {
        Switch _switch = findViewById(id);
        if (_switch != null) {
            _switch.setOnCheckedChangeListener(this);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        Context context = getApplicationContext();
        if (b) {
            ToastHelper.ShowMessage(context, getString(R.string.congratulations_you_are_subscribe_on) + " " + compoundButton.getText().toString().toLowerCase());
        } else {
            ToastHelper.ShowMessage(context, getString(R.string.success_unsubscribe_from) + " " + compoundButton.getText().toString().toLowerCase());
        }
    }
}
