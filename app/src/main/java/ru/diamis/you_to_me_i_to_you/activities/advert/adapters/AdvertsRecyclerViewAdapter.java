package ru.diamis.you_to_me_i_to_you.activities.advert.adapters;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ru.diamis.you_to_me_i_to_you.DAL.Adverts.Advert;
import ru.diamis.you_to_me_i_to_you.Helpers.DateTimeHelper;
import ru.diamis.you_to_me_i_to_you.Interfaces.OnListFragmentInteractionListener;
import ru.diamis.you_to_me_i_to_you.R;


public class AdvertsRecyclerViewAdapter extends RecyclerView.Adapter<AdvertsRecyclerViewAdapter.ViewHolder> {

    private final List<Advert> mValues;
    private final OnListFragmentInteractionListener mListener;

    public AdvertsRecyclerViewAdapter(List<Advert> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_advert, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Resources resources = holder.mView.getContext().getResources();
        holder.mItem = mValues.get(position);
      //  holder.mIdView.setText(mValues.get(position).getUuid().toString());
        holder.mContentView.setText(mValues.get(position).getDescription());
        holder.mAdvertTitle.setText(mValues.get(position).getTitle());
        holder.mViewCount.setText(String.valueOf(mValues.get(position).getViewCount()));
        String price = String.valueOf(String.format("%,.2f", (mValues.get(position).getPrice()))) + " " + resources.getString(R.string.rub);
        holder.mPrice.setText(price);
        holder.mCreateDateTime.setText(DateTimeHelper.getFormattedDate(mValues.get(position).getCreateDate()));
        holder.mUpdatedDateTime.setText(DateTimeHelper.getFormattedDate(mValues.get(position).getLastUpdateDate()));
        holder.mAdvertAvatar.setImageBitmap(mValues.get(position).getImage());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private final View mView;
      //  private final TextView mIdView;
        private final TextView mAdvertTitle;
        private final TextView mViewCount;
        private final TextView mPrice;
        private final TextView mCreateDateTime;
        private final TextView mUpdatedDateTime;
        private final ImageView mAdvertAvatar;
        private final TextView mContentView;
        public Advert mItem;

        private ViewHolder(View view) {
            super(view);
            mView = view;
           // mIdView = view.findViewById(R.id.adverts_item_number);
            mContentView = view.findViewById(R.id.textViewAdvertDescription);
            mAdvertTitle = view.findViewById(R.id.textViewAdvertTitle);
            mViewCount = view.findViewById(R.id.textViewCount);
            mPrice = view.findViewById(R.id.adverts_price);
            mCreateDateTime = view.findViewById(R.id.textViewPostedTime);
            mUpdatedDateTime = view.findViewById(R.id.textViewUpdateTime);
            mAdvertAvatar = view.findViewById(R.id.imageViewAdvertAvatar);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }

    }
}
