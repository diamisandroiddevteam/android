package ru.diamis.you_to_me_i_to_you.activities.sales;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import ru.diamis.you_to_me_i_to_you.DAL.Sale;
import ru.diamis.you_to_me_i_to_you.Interfaces.ICallbacks;
import ru.diamis.you_to_me_i_to_you.Interfaces.OnListFragmentInteractionListener;

public class SalesActivity extends AppCompatActivity implements ICallbacks<Sale>, OnListFragmentInteractionListener<Sale>{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentManager fragMgr = getSupportFragmentManager();
        FragmentTransaction fragTrans = fragMgr.beginTransaction();

        SaleListFragment myFragment = new SaleListFragment();//my custom fragment

        fragTrans.replace(android.R.id.content, myFragment);
        fragTrans.addToBackStack(null);
        fragTrans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragTrans.commit();

//        Button btnNewSale = findViewById(R.id.btnNewSale);
//        btnNewSale.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getApplicationContext(),SaleDetailActivity.class);
//                startActivity(intent);
//            }
//        });

    }

    @Override
    public void onSelect(Sale sale) {

    }

    @Override
    public void onCreate(Sale sale) {

    }

    @Override
    public void onUpdate(Sale sale) {
    }

    @Override
    public void onDelete(Sale sale) {
    }

    @Override
    public void onListFragmentInteraction(Sale item) {
        Toast toast = Toast.makeText(this,item.getId(),Toast.LENGTH_SHORT);
        toast.show();

        Intent intent = new Intent(this,SaleDetailActivity.class);
        intent.putExtra("DEVICE_NAME", item.getId());
        intent.putExtra("DEVICE_ADDRESS",item.getTitle());
        startActivity(intent);
    }

}
