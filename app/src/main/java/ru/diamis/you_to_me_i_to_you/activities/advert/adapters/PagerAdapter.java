package ru.diamis.you_to_me_i_to_you.activities.advert.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import ru.diamis.you_to_me_i_to_you.Filters.AdvertFilter;
import ru.diamis.you_to_me_i_to_you.Helpers.PagerAdapterInjector;
import ru.diamis.you_to_me_i_to_you.activities.advert.AdvertsListFragment;
import ru.diamis.you_to_me_i_to_you.activities.advert.MapFragment;

public class PagerAdapter extends FragmentPagerAdapter {
    int numberOfTabs;
    private AdvertFilter filter;
    public PagerAdapter(FragmentManager fm) {
        super(fm);
        this.numberOfTabs = 2;
    }

    public PagerAdapter(PagerAdapterInjector pagerAdapterInjector){
        super(pagerAdapterInjector.fm);
        this.numberOfTabs = pagerAdapterInjector.NumOfTabs ;
        this.filter = pagerAdapterInjector.filter;

    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                AdvertsListFragment fragment= new AdvertsListFragment();
                fragment.setFilter(filter);
                return fragment;
            case 1:
                return new MapFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }
}


