package ru.diamis.you_to_me_i_to_you.activities.company;

import android.os.Bundle;

import ru.diamis.you_to_me_i_to_you.R;
import ru.diamis.you_to_me_i_to_you.activities.common.CommonAppActivity;

public class InfoActivity extends CommonAppActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        InitBottomAppBar();
    }

}
