package ru.diamis.you_to_me_i_to_you.activities.advert;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import ru.diamis.you_to_me_i_to_you.DAL.Adverts.Advert;
import ru.diamis.you_to_me_i_to_you.Interfaces.OnListFragmentInteractionListener;


public class AllCustomerAdvertsActivity extends AppCompatActivity implements OnListFragmentInteractionListener<Advert> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentManager fragMgr = getSupportFragmentManager();
        FragmentTransaction fragTrans = fragMgr.beginTransaction();

        AdvertsListFragment myFragment = new AdvertsListFragment();
        if (savedInstanceState == null) {
            fragTrans.replace(android.R.id.content, myFragment);
            fragTrans.addToBackStack(null);
            fragTrans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            fragTrans.commit();
        }
    }

    @Override
    public void onListFragmentInteraction(Advert item) {
        Toast toast = Toast.makeText(this,item.getUuid().toString(),Toast.LENGTH_SHORT);
        toast.show();
//
        Intent intent = new Intent(this, AdvertDetailActivity.class);
//        intent.putExtra("SELECTED_ADVERT",item);
//        intent.putExtra("DEVICE_NAME", item.getUuid());
//        intent.putExtra("DEVICE_ADDRESS",item.getTitle());
        startActivity(intent);
    }
}