package ru.diamis.you_to_me_i_to_you.activities.account;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKAccessTokenTracker;

import ru.diamis.you_to_me_i_to_you.DAL.Adverts.Advert;
import ru.diamis.you_to_me_i_to_you.DAL.User;
import ru.diamis.you_to_me_i_to_you.Filters.UserFilter;
import ru.diamis.you_to_me_i_to_you.Interfaces.OnListFragmentInteractionListener;
import ru.diamis.you_to_me_i_to_you.R;
import ru.diamis.you_to_me_i_to_you.activities.advert.AdvertDetailActivity;
import ru.diamis.you_to_me_i_to_you.activities.advert.AdvertsActivity;
import ru.diamis.you_to_me_i_to_you.activities.advert.adapters.PagerAdapter;
import ru.diamis.you_to_me_i_to_you.activities.chat.ChatActivity;
import ru.diamis.you_to_me_i_to_you.activities.common.CommonAppActivity;
import ru.diamis.you_to_me_i_to_you.activities.company.CompanyActivity;
import ru.diamis.you_to_me_i_to_you.activities.filter.FilterActivity;
import ru.diamis.you_to_me_i_to_you.activities.news.NewsActivity;
import ru.diamis.you_to_me_i_to_you.activities.orders.MyOrdersActivity;
import ru.diamis.you_to_me_i_to_you.activities.sales.SalesActivity;
import ru.diamis.you_to_me_i_to_you.activities.settings.SettingsActivity;
import ru.diamis.you_to_me_i_to_you.services.UsersService;

public class NavigationActivity extends CommonAppActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnListFragmentInteractionListener<Advert> {
    public static final String TAG = "NavigationActivity";
    VKAccessTokenTracker vkAccessTokenTracker = new VKAccessTokenTracker() {
        @Override
        public void onVKAccessTokenChanged(VKAccessToken oldToken, VKAccessToken newToken) {
            if (newToken == null) {
// VKAccessToken is invalid
            }
        }
    };

    UsersService usersService = new UsersService();
    UserFilter userFilter = new UserFilter();
    User user = usersService.Get(userFilter).get(0);
    ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        handleIntent(getIntent());
        setContentView(R.layout.app_bar_navigation);
        InitHeaderAppBar();

        InitBottomAppBar();
        viewPager = findViewById(R.id.adverts_viewpager);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);
        final ImageView changeView = findViewById(R.id.change_view);
        changeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (viewPager.getCurrentItem()) {
                    case 1:
                        changeView.setImageDrawable(getResources().getDrawable(R.mipmap.ic_map_point));
                        viewPager.setCurrentItem(getItem(-1), true);
                        break;
                    case 0:
                        changeView.setImageDrawable(getResources().getDrawable(R.mipmap.ic_list));
                        viewPager.setCurrentItem(getItem(+1), true);
                        break;
                    default:break;
                }


            }
        });

        ImageView toFilter = findViewById(R.id.filter_logo);
        toFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),FilterActivity.class));
            }
        });


//        Button btnSelectMapLayer =findViewById(R.id.buttbtnSelectMapType);
//        btnSelectMapLayer.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //     MapTypeDialogProcess();
//            }
//        });

//TODO fix VKSDK initialize
//        vkAccessTokenTracker.startTracking();
//        VKSdk.initialize(this);
    }

    private void MapTypeDialogProcess() {
        final String[] SourceMapName = {getString(R.string.standart),getString(R.string.hybrid),getString(R.string.satellite)};
        new AlertDialog.Builder(NavigationActivity.this)
                .setItems(SourceMapName, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        Toast.makeText(getApplicationContext(),"Name: " + SourceMapName[item],Toast.LENGTH_SHORT).show();
                        dialog.dismiss();

                    }
                }).setCancelable(true).setNegativeButton(getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Toast.makeText(getApplicationContext(), "Вы ничего не выбрали",
                        Toast.LENGTH_LONG).show();
            }
        }).show();

    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }


    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            //use the query to search your data somehow
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu");
        getMenuInflater().inflate(R.menu.navigation, menu);
        getMenuInflater().inflate(R.menu.fragment_item_search, menu);
        getMenuInflater().inflate(R.menu.map, menu);
//        MenuItem searchItem = menu.findItem(R.id.menu_item_search);
//        final SearchView searchView = (SearchView) searchItem.getActionView();
//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String s) {
//                Log.d(TAG, "QueryTextSubmit: " + s);
//                updateItems();
//                return true;
//            }
//            @Override
//            public boolean onQueryTextChange(String s) {
//                Log.d(TAG, "QueryTextChange: " + s);
//                return false;
//            }
//        });
//
//
        return true;
    }

    //    private void updateItems() {
//       // new FetchItemsTask().execute();
//    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected");
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(getBaseContext(), FilterActivity.class);
            intent.putExtra("EXTRA_SESSION_ID", "hui");
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Log.d(TAG, "onNavigationItemSelected");
        switch (item.getItemId()) {
            case R.id.nav_my_services:
                runActivity(AdvertsActivity.class);
                break;
            case R.id.nav_my_company:
                runActivity(CompanyActivity.class);
                break;
            case R.id.nav_my_messages:
                runActivity(ChatActivity.class);
                break;
            case R.id.nav_my_orders:
                runActivity(MyOrdersActivity.class);
                break;
            case R.id.nav_my_news:
                runActivity(NewsActivity.class);
                break;
            case R.id.nav_my_sales:
                runActivity(SalesActivity.class);
                break;
            case R.id.nav_my_settings:
                Intent i = new Intent(NavigationActivity.this, SettingsActivity.class);
                i.putExtra("USER_UUID", user.getUuid());
                runActivity(SettingsActivity.class);
                break;

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onListFragmentInteraction(Advert item) {
        Toast toast = Toast.makeText(this, item.getUuid().toString(), Toast.LENGTH_SHORT);
        toast.show();

        Intent intent = new Intent(this, AdvertDetailActivity.class);
        intent.putExtra("SELECTED_ADVERT_UUID", item.getUuid());
        intent.putExtra("SELECTED_ADVERT_PRICE", item.getPrice());
        intent.putExtra("SELECTED_ADVERT_DESCRIPTION", item.getDescription());
        intent.putExtra("SELECTED_ADVERT_EXTENDED_DESCRIPTION", item.getExtendedDescription());
        intent.putExtra("SELECTED_ADVERT_CREATE_DATE", item.getCreateDate());
        intent.putExtra("SELECTED_ADVERT_UPDATE_DATE", item.getLastUpdateDate());
        intent.putExtra("SELECTED_ADVERT_VIEW_COUNT", item.getViewCount());
        startActivity(intent);
    }


}

