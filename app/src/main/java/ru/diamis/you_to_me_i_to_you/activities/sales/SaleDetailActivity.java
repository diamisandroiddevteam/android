package ru.diamis.you_to_me_i_to_you.activities.sales;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import ru.diamis.you_to_me_i_to_you.R;

public class SaleDetailActivity extends AppCompatActivity {
    TextView txtViewItemNumber;
    TextView  txtViewItemContent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_sale);


        Intent intent = getIntent();

        String fName = intent.getStringExtra("DEVICE_NAME");
        String lName = intent.getStringExtra("DEVICE_ADDRESS");

        String answer = (fName +" "+lName);

        txtViewItemNumber = findViewById(R.id.item_number);
        txtViewItemContent = findViewById(R.id.content);

        txtViewItemNumber.setText(fName);
        txtViewItemContent.setText(lName);
        Toast.makeText(getApplicationContext(),answer ,Toast.LENGTH_LONG);
    }
}
