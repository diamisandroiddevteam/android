package ru.diamis.you_to_me_i_to_you.activities.auth;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;
import java.util.UUID;

import ru.diamis.you_to_me_i_to_you.DAL.User;
import ru.diamis.you_to_me_i_to_you.Filters.UserFilter;
import ru.diamis.you_to_me_i_to_you.Helpers.ToastHelper;
import ru.diamis.you_to_me_i_to_you.Helpers.ValidationHelper;
import ru.diamis.you_to_me_i_to_you.R;
import ru.diamis.you_to_me_i_to_you.services.UsersService;

public class ResetPasswordActivity extends AppCompatActivity {
    public static final String TAG = "ResetPasswordActivity";
    private UsersService userService = new UsersService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

       TextView btnRegistry = findViewById(R.id.btnRegistration);
        btnRegistry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), RegistryActivity.class);
                startActivity(intent);
            }
        });

       Button btnForgotPassword =findViewById(R.id.reset_button);
       btnForgotPassword.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               UserFilter filter = new UserFilter();
               EditText edt = findViewById(R.id.editTextEmailOrTelephone);
               String emailOrPhone = edt.getText().toString();

               if (emailOrPhone.isEmpty()) {
                   ToastHelper.ShowMessage(getApplicationContext(), R.string.invalid_type_email_or_phone_try_to_use);
               }

               if (IsValidEmail(emailOrPhone) || IsValidPhone(emailOrPhone)) {
                   if (IsValidEmail(emailOrPhone)) {
                       filter.setEmail(emailOrPhone);
                   } else {
                       filter.setPhone(emailOrPhone);
                   }
               } else
                   ToastHelper.ShowMessage(getApplicationContext(), R.string.invalid_type_email_or_phone_try_to_use);


               List<User> answer = userService.Get(filter);
               if ((answer.isEmpty())) {
                   //TODO
                   ToastHelper.ShowMessage(getApplicationContext(), R.string.user_not_found);

               } else {
                    //TODO: fix bundle trouble
                   String userId = UUID.randomUUID().toString();//bundle.getString("UUID_USER");
                   Boolean successReset = userService.ResetPassword(UUID.fromString(userId));
                   if (successReset) {
                       Intent intent = new Intent();
                      // startActivity(intent,regis);
                   }

               }
           }
       });



        }
    private Boolean IsValidEmail(String input){
        return new ValidationHelper().IsValidEmail(input);}

    private Boolean IsValidPhone(String input){
        return new ValidationHelper().IsValidPhone(input);}

    }

