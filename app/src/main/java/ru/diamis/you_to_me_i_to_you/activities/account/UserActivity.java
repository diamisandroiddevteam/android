package ru.diamis.you_to_me_i_to_you.activities.account;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.InputStream;

import ru.diamis.you_to_me_i_to_you.R;
import ru.diamis.you_to_me_i_to_you.activities.advert.AdvertsActivity;
import ru.diamis.you_to_me_i_to_you.activities.chat.ChatActivity;
import ru.diamis.you_to_me_i_to_you.activities.common.CommonAppActivity;
import ru.diamis.you_to_me_i_to_you.activities.company.CompanyActivity;
import ru.diamis.you_to_me_i_to_you.activities.news.NewsActivity;
import ru.diamis.you_to_me_i_to_you.activities.orders.MyOrdersActivity;
import ru.diamis.you_to_me_i_to_you.activities.sales.SalesActivity;
import ru.diamis.you_to_me_i_to_you.activities.settings.SettingsActivity;

public class UserActivity extends CommonAppActivity {
    private final int CAMERA_RESULT = 0;
    private final int PICK_IMAGE = 1;
    private ImageView imageViewUserAvatar;
    private Uri mOutputFileUri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        TextView changePhoto = findViewById(R.id.textViewChangePhoto);

        changePhoto.setPaintFlags(changePhoto.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        imageViewUserAvatar = findViewById(R.id.imageViewUserAvatar);
        // registryButton.setText("This text will be underlined");
        changePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageDialogProcess();
            }
        });


        TextView MyAdvertsOrServices = findViewById(R.id.textView_My_adverts_or_services);
        MyAdvertsOrServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), AdvertsActivity.class));
            }
        });

        TextView MyOrders = findViewById(R.id.textView_my_orders);
        MyOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MyOrdersActivity.class));
            }
        });

        TextView MyMessages = findViewById(R.id.textView_my_messages);
        MyMessages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ChatActivity.class));
            }
        });

        TextView MyCompany = findViewById(R.id.textView_my_company);
        MyCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), CompanyActivity.class));
            }
        });

        TextView News = findViewById(R.id.textView_news);
        News.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), NewsActivity.class));
            }
        });

        TextView Sales = findViewById(R.id.textView_sales);
        Sales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), SalesActivity.class));
            }
        });

        TextView Settings = findViewById(R.id.textView_my_settings);
        Settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
            }
        });
        InitBottomAppBar();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PICK_IMAGE:
                if (resultCode == RESULT_OK) {
                    try {
                        //Получаем URI изображения, преобразуем его в Bitmap
                        //объект и отображаем в элементе ImageView нашего интерфейса:
                        final Uri imageUri = data.getData();
                        final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        imageViewUserAvatar.setImageBitmap(selectedImage);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case CAMERA_RESULT:
                if (data != null) {
                    if (data.hasExtra("data")) {
                        Bitmap thumbnailBitmap = data.getParcelableExtra("data");
                        // Какие-то действия с миниатюрой
                        imageViewUserAvatar.setImageBitmap(thumbnailBitmap);
                    }
                } else {
                    // Какие-то действия с полноценным изображением,
                    // сохраненным по адресу mOutputFileUri
                    imageViewUserAvatar.setImageURI(mOutputFileUri);
                }
                Bitmap thumbnailBitmap = (Bitmap) data.getExtras().get("data");
                imageViewUserAvatar.setImageBitmap(thumbnailBitmap);
                break;
        }
    }

    private void ImageDialogProcess() {
        final String[] SourceName = {"Камера", "Галерея"};

        new AlertDialog.Builder(getApplicationContext())
                .setTitle("Выберите источник")
                .setItems(SourceName, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {

                        switch (item) {
                            case CAMERA_RESULT:
                                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(cameraIntent, 0);
                                break;
                            case PICK_IMAGE:
                                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                                photoPickerIntent.setType("image/*");
                                startActivityForResult(photoPickerIntent, PICK_IMAGE);
                                break;
                            default:
                                break;
                        }
                        Toast.makeText(getApplicationContext(), "Name: " + SourceName[item], Toast.LENGTH_SHORT).show();
                        dialog.dismiss();

                    }
                }).show();
    }


}
