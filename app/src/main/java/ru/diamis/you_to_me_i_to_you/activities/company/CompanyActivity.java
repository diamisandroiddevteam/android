package ru.diamis.you_to_me_i_to_you.activities.company;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import ru.diamis.you_to_me_i_to_you.Interfaces.OnFragmentInteractionListener;
import ru.diamis.you_to_me_i_to_you.R;

public class CompanyActivity extends AppCompatActivity implements OnFragmentInteractionListener {
    public static final String TAG = "CompanyActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company);
        TabLayout tabLayout = findViewById(R.id.company_tab_layout);

        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.about_company)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.addresses)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.photos)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = findViewById(R.id.company_view_pager);
        final CompanyPageAdapter adapter = new CompanyPageAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


}
