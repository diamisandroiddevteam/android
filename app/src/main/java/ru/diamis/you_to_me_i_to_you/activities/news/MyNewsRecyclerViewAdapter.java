package ru.diamis.you_to_me_i_to_you.activities.news;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ru.diamis.you_to_me_i_to_you.DAL.News;
import ru.diamis.you_to_me_i_to_you.Interfaces.OnListFragmentInteractionListener;
import ru.diamis.you_to_me_i_to_you.R;

public class MyNewsRecyclerViewAdapter extends RecyclerView.Adapter<MyNewsRecyclerViewAdapter.ViewHolder> {

    private final List<News> mValues;
    private final OnListFragmentInteractionListener mListener;

    MyNewsRecyclerViewAdapter(List<News> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_news, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Resources resources = holder.mView.getContext().getResources();
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).getUuid().toString());
        holder.mContentView.setText(mValues.get(position).getTitle());
        holder.mPostDate.setText(String.format("%s :%s", resources.getString(R.string.textViewPostedTime), mValues.get(position).getPublicatedDate().toString()));
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }
    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView mIdView;
        final TextView mContentView;
        final TextView mPostDate;
        public News mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = view.findViewById(R.id.news_item_number);
            mContentView = view.findViewById(R.id.news_content);
            mPostDate = view.findViewById(R.id.textViewPublicationDateTime);


        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
