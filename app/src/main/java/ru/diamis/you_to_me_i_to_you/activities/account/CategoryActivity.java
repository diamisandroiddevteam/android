package ru.diamis.you_to_me_i_to_you.activities.account;

import android.os.Bundle;
import android.widget.ExpandableListView;
import android.widget.SearchView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ru.diamis.you_to_me_i_to_you.DAL.Category;
import ru.diamis.you_to_me_i_to_you.R;
import ru.diamis.you_to_me_i_to_you.activities.common.CommonAppActivity;
import ru.diamis.you_to_me_i_to_you.services.CategoryService;

public class CategoryActivity extends CommonAppActivity {
    public static final String TAG = "CategoryActivity";
    CategoryService categoryService = new CategoryService();


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_category);
        InitBottomAppBar();

        Map<String, String> map;
        // коллекция для групп
        ArrayList<Map<String, String>> groupDataList = new ArrayList<>();

        // заполняем коллекцию групп из массива с названиями групп
        ArrayList<Category> categories = new ArrayList<>();
        try {
            categories = categoryService.Get(null);
        } catch (Exception e) {
            categories = new ArrayList<>();
        }
        if (categories.isEmpty()) return;
        ArrayList<Map<String, String>> childDataItemList = new ArrayList<>();
        for (Category category : categories) {
            // заполняем список атрибутов для каждой группы
            map = new HashMap<>();
            map.put("groupName", category.getName()); // время года
            groupDataList.add(map);

            for (Category subCategory : category.getCategories()) {
                map = new HashMap<>();
                map.put("categoryName", subCategory.getName());
                childDataItemList.add(map);
            }
        }
        ArrayList<ArrayList<Map<String, String>>> childDataList = new ArrayList<>();
        childDataList.add(childDataItemList);
        // список атрибутов групп для чтения
        String groupFrom[] = new String[] { "groupName" };
        // список ID view-элементов, в которые будет помещены атрибуты групп
        int groupTo[] = new int[] { android.R.id.text1 };


        // в итоге получится сhildDataList = ArrayList<сhildDataItemList>

        // создаем коллекцию элементов для первой группы
//        ArrayList<Map<String, String>> сhildDataItemList = new ArrayList<>();
//        // заполняем список атрибутов для каждого элемента
//        for (String month : agrarianArray) {
//            map = new HashMap<>();
//            map.put("categoryName", month);
//            сhildDataItemList.add(map);
//        }
//        // добавляем в коллекцию коллекций
//        сhildDataList.add(сhildDataItemList);
//
//        // создаем коллекцию элементов для второй группы
//        сhildDataItemList = new ArrayList<>();
//        for (String month : estateArray) {
//            map = new HashMap<>();
//            map.put("categoryName", month);
//            сhildDataItemList.add(map);
//        }
//        сhildDataList.add(сhildDataItemList);
//
//        // создаем коллекцию элементов для третьей группы
//        сhildDataItemList = new ArrayList<>();
//        for (String month : transportArray) {
//            map = new HashMap<>();
//            map.put("categoryName", month);
//            сhildDataItemList.add(map);
//        }
//        сhildDataList.add(сhildDataItemList);
//
//        // создаем коллекцию элементов для четвертой группы
//        сhildDataItemList = new ArrayList<>();
//        for (String month : autoServiceArray) {
//            map = new HashMap<>();
//            map.put("categoryName", month);
//            сhildDataItemList.add(map);
//        }
//        сhildDataList.add(сhildDataItemList);

        // список атрибутов элементов для чтения
        String childFrom[] = new String[] { "categoryName" };
        // список ID view-элементов, в которые будет помещены атрибуты
        // элементов
        int childTo[] = new int[] { android.R.id.text1 };

        final SimpleExpandableListAdapter adapter = new SimpleExpandableListAdapter(
                this, groupDataList,
                android.R.layout.simple_expandable_list_item_1, groupFrom,
                groupTo, childDataList, android.R.layout.simple_list_item_1,
                childFrom, childTo);

        ExpandableListView expandableListView = findViewById(R.id.expListView);
        expandableListView.setAdapter(adapter);

        SearchView searchView = findViewById(R.id.simpleSearchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                String text = newText;
                //TODO
                Toast.makeText(getApplicationContext(), getString(R.string.will_be_in_next_version), Toast.LENGTH_SHORT).show();
                //                                         adapter.filter(text);
                return false;
            }
        });


        InitBottomAppBar();
    }

}
