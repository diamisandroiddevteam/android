package ru.diamis.you_to_me_i_to_you.activities.sales;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.File;
import java.util.Calendar;
import java.util.UUID;

import ru.diamis.you_to_me_i_to_you.DAL.Sale;
import ru.diamis.you_to_me_i_to_you.Helpers.DateTimeHelper;
import ru.diamis.you_to_me_i_to_you.Helpers.PictureUtils;
import ru.diamis.you_to_me_i_to_you.Interfaces.ICallbacks;
import ru.diamis.you_to_me_i_to_you.R;
import ru.diamis.you_to_me_i_to_you.services.SaleService;

public class SaleFragment extends Fragment
        implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    public static final String EXTRA_SALE_ID = "ru.diamis.you_to_me_i_to_yout.Id";

    private static final String DIALOG_DATE = "date";
    private static final String DIALOG_TIME = "time";
    private static final String DIALOG_IMAGE = "image";
    private static final int REQUEST_CONTACT = 0;
    private static final int REQUEST_PHOTO = 1;

    private SaleService saleService;
    private Sale sale;
    private File mPhotoFile;

    private Button mDateButton;
    private Button mTimeButton;
    private Button mSuspectButton;
    private Button mCallSuspectButton;
    private Button btnExit;
    private ImageView mPhotoView;
    private ICallbacks mCallbacks;

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallbacks = (ICallbacks)context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /* retrieve crimeId argument from Fragment Bundle */
        UUID crimeId = (UUID) getArguments().getSerializable(EXTRA_SALE_ID);
        saleService = new SaleService();
        //sale = saleService.get(crimeId);

        Calendar mCalendar = Calendar.getInstance();
        mCalendar.setTime(sale.getDate());

        mPhotoFile =  saleService.getPhotoFile(sale);

        setHasOptionsMenu(true); // turn on options menu handling
    }

    @Override
    public void onPause() {
        super.onPause();

        saleService.update(sale);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    public static SaleFragment newInstance(int saleId) {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_SALE_ID, saleId);
        SaleFragment fragment = new SaleFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) return;
        if (requestCode == REQUEST_CONTACT && data != null) {
            Uri contactUri = data.getData();
            // specify which fields we want the query to return values for
            String[] queryFields = new String[] {
                    ContactsContract.Contacts._ID,
                    ContactsContract.Contacts.DISPLAY_NAME,
                    ContactsContract.Contacts.HAS_PHONE_NUMBER
            };
            // perform the query - the contactUri is like a "where" clause here
            Cursor contactCursor = getActivity().getContentResolver()
                    .query(contactUri, queryFields, null, null, null);

            if (contactCursor == null) {
                return;
            }

            try {
                // double-check that we actually got results
                if (contactCursor.getCount() == 0) {
                    return;
                }

                // pull out the first column of the first row of data
                if (contactCursor.moveToFirst()) {
                    String contactId = contactCursor.getString(0); // _ID
                    String name = contactCursor.getString(1); // DISPLAY_NAME
                    String hasPhoneNumber = contactCursor.getString(2); // HAS_PHONE_NUMBER

                    // checks whether contact has at least one phone number
                    if (hasPhoneNumber.equals("1")) {
                        String phoneNumber = retrieveContactPhoneNumber(contactId);

                        if (phoneNumber != null) {
                            //  sale.setPhone(phoneNumber);
                            mCallSuspectButton.setEnabled(true);
                        }
                    } else {
                        //  sale.setPhone(null);
                        mCallSuspectButton.setEnabled(false);
                    }

                    sale.setName(name);
                    updateSale();
                    mSuspectButton.setText(name);
                }
            } finally {
                contactCursor.close();
            }
        } else if (requestCode == REQUEST_PHOTO) {
            updateSale();
            updatePhotoView();
        }
    }

    private void updateSale() {
        saleService.update(sale);
        mCallbacks.onUpdate(sale);
    }

    public void updateDate() {
        mDateButton.setText(DateTimeHelper.getDateString(getActivity(), sale.getDate()));
    }

    public void updateTime() {
        mTimeButton.setText(DateTimeHelper.getTimeString(getActivity(), sale.getDate()));
    }

    @TargetApi(11)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_sale, container, false);

        EditText mTitleField = v.findViewById(R.id.sale_title);
        mTitleField.setText(sale.getTitle());
        mTitleField.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                sale.setTitle(charSequence.toString());
                updateSale();
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
                // this space intentionally left blank
            }

            @Override
            public void afterTextChanged(Editable editable) {
                // this one too
            }
        });
        mTitleField.requestFocus();

        mDateButton = v.findViewById(R.id.sale_date);
        updateDate();
        mDateButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
//                DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(CrimeFragment.this,
//                        mCalendar.get(Calendar.YEAR),
//                        mCalendar.get(Calendar.MONTH),
//                        mCalendar.get(Calendar.DAY_OF_MONTH));
//
//                datePickerDialog.show(getActivity().getFragmentManager(), DIALOG_DATE);
            }
        });

        mTimeButton = v.findViewById(R.id.sale_time);
        updateTime();
        mTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // get system default hour format
                boolean is24HourMode = DateTimeHelper.is24HourFormat(getActivity());
//
//                TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(CrimeFragment.this,
//                        mCalendar.get(Calendar.HOUR_OF_DAY),
//                        mCalendar.get(Calendar.MINUTE), is24HourMode);
//
//                timePickerDialog.show(getActivity().getFragmentManager(), DIALOG_TIME);
            }
        });

        CheckBox mSolvedCheckBox = v.findViewById(R.id.sale_solved);
        mSolvedCheckBox.setChecked(sale.isSolved());
        mSolvedCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                // set the crime's solved property
                sale.setSolved(isChecked);
                updateSale();
            }
        });

        Button mReportButton = v.findViewById(R.id.sale_report);
        mReportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = ShareCompat.IntentBuilder.from(getActivity())
//                        .setText(getCrimeReport())
//                        .setSubject(getString(R.string.sale_report_subject,
//                                getString(R.string.app_name)))
//                        .setType("text/plain")
//                        .getIntent();
//                intent = Intent.createChooser(intent, getString(R.string.send_report));
//                startActivity(intent);
            }
        });

        final Intent pickContact = new Intent(Intent.ACTION_PICK,
                ContactsContract.Contacts.CONTENT_URI);
//        mSuspectButton = v.findViewById(R.id.sale_suspect);
//        mSuspectButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivityForResult(pickContact, REQUEST_CONTACT);
//            }
//        });

        if (sale.getSuspectName() != null) {
            mSuspectButton.setText(sale.getSuspectName());
        }

//        mCallSuspectButton = v.findViewById(R.id.call_crime_suspect);
//        mCallSuspectButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent callContact = new Intent(Intent.ACTION_DIAL,
//                        Uri.parse("tel:" + sale.getSuspectPhoneNumber()));
//                startActivity(callContact);
//            }
//        });

        if (sale.getSuspectPhoneNumber() == null) {
            mCallSuspectButton.setEnabled(false);
        }

        PackageManager packageManager = getActivity().getPackageManager();
        if (packageManager.resolveActivity(pickContact, PackageManager.MATCH_DEFAULT_ONLY) == null) {
            mSuspectButton.setEnabled(false);
            mCallSuspectButton.setEnabled(false);
        }

        btnExit = v.findViewById(R.id.btnExit);
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SaleListActivity.class);
                startActivity(intent);
                startActivity(intent);
            }
        });
        ImageButton mPhotoButton = v.findViewById(R.id.sale_camera);
        final Intent captureImage = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        boolean canTakePhoto = mPhotoFile != null &&
                captureImage.resolveActivity(packageManager) != null;
        mPhotoButton.setEnabled(canTakePhoto);

        if (canTakePhoto) {
            Uri uri = Uri.fromFile(mPhotoFile);
            captureImage.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        }

        mPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(captureImage, REQUEST_PHOTO);
            }
        });

        mPhotoView = v.findViewById(R.id.sale_photo);
        mPhotoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPhotoView.getDrawable() != null) {
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    ImageFragment.newInstance(mPhotoFile.getAbsolutePath())
                            .show(fm, DIALOG_IMAGE);
                } else {
                    Toast.makeText(getActivity(), R.string.no_photo_text, Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });
        updatePhotoView();

        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_sale, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_delete_sale:
                new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.delete_sale)
                        .setMessage(R.string.delete_sale_dialog_msg)
                        .setNeutralButton(android.R.string.cancel, null)
                        .setPositiveButton(R.string.delete_sale, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                             //   saleService.delete(sale);
                                if (null != getActivity().findViewById(R.id.detailFragmentContainer)) {
                                    mCallbacks.onUpdate(sale);
                                    mCallbacks.onDelete(sale);
                                } else {
                                    getActivity().finish();
                                }
                            }
                        })
                        .setIcon(R.drawable.ic_dialog_alert)
                        .show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




//    private String getCrimeReport() {
//        String solvedString = (sale.isSolved()) ? getString(R.string.sale_report_solved)
//                : getString(R.string.sale_report_unsolved);
//
//        String dateFormat = "EEE, MMM dd";
//        String dateString = DateFormat.format(dateFormat, sale.getDate()).toString();
//
//        String suspect = sale.getSuspectName();
//        suspect = (suspect == null) ? getString(R.string.sale_report_no_suspect)
//               : getString(R.string.sale_report_suspect, suspect);
//
//        return getString(R.string.sale_report, sale.getTitle(),
//                dateString, solvedString, suspect);
//    }

    private String retrieveContactPhoneNumber(String contactId) {
        Cursor phoneNumberCursor = getActivity().getContentResolver()
                .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                + " = " + contactId, null, null);

        if (phoneNumberCursor == null) {
            return null;
        }

        String phoneNumber = null;
        try {
            if (phoneNumberCursor.moveToFirst()) {
                phoneNumber = phoneNumberCursor
                        .getString(phoneNumberCursor
                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            }
        } finally {
            phoneNumberCursor.close();
        }

        return phoneNumber;
    }

    private void updatePhotoView() {
        if (mPhotoFile == null || !mPhotoFile.exists()) {
            mPhotoView.setImageDrawable(null);
        } else {
            Bitmap bitmap = PictureUtils.getScaledBitmap(mPhotoFile.getPath(), getActivity());
            mPhotoView.setImageBitmap(bitmap);
        }
    }
}
