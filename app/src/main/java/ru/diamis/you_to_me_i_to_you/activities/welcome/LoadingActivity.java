package ru.diamis.you_to_me_i_to_you.activities.welcome;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ru.diamis.you_to_me_i_to_you.R;

public class LoadingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
    }
}
