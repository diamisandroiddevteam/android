//package ru.diamis.you_to_me_i_to_you.activities.advert.adapters;
//
//
//import android.content.res.Resources;
//import android.support.annotation.NonNull;
//import android.support.v7.widget.RecyclerView;
//import android.view.View;
//import android.view.ViewGroup;
//
//import java.util.List;
//
//import ru.diamis.you_to_me_i_to_you.DAL.Adverts.Advert;
//import ru.diamis.you_to_me_i_to_you.Helpers.DateTimeHelper;
//import ru.diamis.you_to_me_i_to_you.Interfaces.OnListFragmentInteractionListener;
//import ru.diamis.you_to_me_i_to_you.R;
//
//class CustomAdapter extends RecyclerView.Adapter<AdvertsRecyclerViewAdapter.AdvertViewHolder>{
//    private final List<Advert> mValues;
//    private final OnListFragmentInteractionListener mListener;
//    public CustomAdapter(List<Advert> items, OnListFragmentInteractionListener listener) {
//        mValues = items;
//        mListener = listener;
//    }
//    @NonNull
//    @Override
//    public AdvertsRecyclerViewAdapter.AdvertViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        return null;
//    }
//
//    @Override
//    public void onBindViewHolder(@NonNull final AdvertsRecyclerViewAdapter.AdvertViewHolder holder, int position) {
//        int actualPosition = position % mValues.size();
//        Resources resources = holder.mView.getContext().getResources();
//        holder.mItem = mValues.get(position);
//        holder.mContentView.setText(mValues.get(position).getDescription());
//        holder.mAdvertTitle.setText(mValues.get(position).getTitle());
//        holder.mViewCount.setText(String.valueOf(mValues.get(position).getViewCount()));
//        String price = String.valueOf(String.format("%,.2f", (mValues.get(position).getPrice()))) + " " + resources.getString(R.string.rub);
//        holder.mPrice.setText(price);
//        holder.mCreateDateTime.setText(DateTimeHelper.getFormattedDate(mValues.get(position).getCreateDate()));
//        holder.mUpdatedDateTime.setText(DateTimeHelper.getFormattedDate(mValues.get(position).getLastUpdateDate()));
//        holder.mAdvertAvatar.setImageBitmap(mValues.get(position).getImage());
//        holder.mRatingBar.setMax(5);
//        holder.mRatingBar.setRating(mValues.get(position).getRating());
//
//        holder.mView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (null != mListener) {
//                    // Notify the active callbacks interface (the activity, if the
//                    // fragment is attached to one) that an item has been selected.
//                    mListener.onListFragmentInteraction(holder.mItem);
//                }
//            }
//        });
//    }
//
//    @Override
//        public int getItemCount() {
//            return Integer.MAX_VALUE; //важно, обеспечивает "бесконечность" скролла
//        }
//
//    }
//
