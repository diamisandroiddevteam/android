package ru.diamis.you_to_me_i_to_you.activities.orders;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class OrderPageAdapter extends FragmentPagerAdapter {
    int numberOfTabs;
    public OrderPageAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.numberOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new OrderListFragment();
            case 1:
                return OrderListFragment.newInstance(1);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }
}
