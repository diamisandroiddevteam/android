package ru.diamis.you_to_me_i_to_you.activities.news;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import ru.diamis.you_to_me_i_to_you.activities.orders.OrderListFragment;

class NewsPageAdapter extends FragmentPagerAdapter {
    int numberOfTabs;
    public NewsPageAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.numberOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new NewsListFragment();
            case 1:
                return NewsListFragment.newInstance(1);
            case 2:
                return NewsListFragment.newInstance(1);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }
}
