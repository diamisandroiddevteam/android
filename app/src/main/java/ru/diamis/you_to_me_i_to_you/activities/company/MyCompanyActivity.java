package ru.diamis.you_to_me_i_to_you.activities.company;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import ru.diamis.you_to_me_i_to_you.Helpers.ToastHelper;
import ru.diamis.you_to_me_i_to_you.Interfaces.OnFragmentInteractionListener;
import ru.diamis.you_to_me_i_to_you.R;
import ru.diamis.you_to_me_i_to_you.activities.common.CommonAppActivity;

public class MyCompanyActivity extends CommonAppActivity implements OnFragmentInteractionListener {
    public static final String TAG = "MyCompanyActivity ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_company);


        String[]tariffNamesArray= new String []{getString(R.string.baseTariff),getString(R.string.extendedTariff),getString(R.string.vipTariff)};

        ArrayAdapter<String> adapterTariffData = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,  tariffNamesArray);
        adapterTariffData.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner tariffSpinner = findViewById(R.id.tariffPlanSpinner);
        tariffSpinner.setAdapter(adapterTariffData);
        tariffSpinner.setPrompt("Title");
        tariffSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // показываем позиция нажатого элемента
                Toast.makeText(getBaseContext(), "Position = " + position, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        Button btnSaveInfo = findViewById(R.id.btnSave);
        btnSaveInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO  SaveInfoRequest
                ToastHelper.ShowMessage(getApplicationContext(),getString(R.string.will_be_in_next_version));
            }
        });

        InitBottomAppBar();
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
