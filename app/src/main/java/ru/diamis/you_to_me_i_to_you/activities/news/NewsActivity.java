package ru.diamis.you_to_me_i_to_you.activities.news;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import ru.diamis.you_to_me_i_to_you.DAL.News;
import ru.diamis.you_to_me_i_to_you.Interfaces.OnListFragmentInteractionListener;
import ru.diamis.you_to_me_i_to_you.R;
import ru.diamis.you_to_me_i_to_you.activities.settings.SettingsActivity;


public class NewsActivity extends AppCompatActivity implements OnListFragmentInteractionListener<News> {
    public static final String TAG = "NewsActivity";
    @Override
    public void onListFragmentInteraction(News item) {
        Toast toast = Toast.makeText(this,item.getUuid().toString(),Toast.LENGTH_SHORT);
        toast.show();

        Intent intent = new Intent(this,ScrollingActivity.class);
        intent.putExtra("NEWS_UUID", item.getUuid());
        intent.putExtra("NEWS_MESSAGE",item.getMessage());
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        TabLayout tabLayout = findViewById(R.id.news_tab_layout);

        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.news_in_country)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.news_in_world)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.news_in_subscribe)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = findViewById(R.id.news_view_pager);
        final NewsPageAdapter adapter = new NewsPageAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = this.getMenuInflater();
        inflater.inflate(R.menu.navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent settingsIntent = new Intent(this, SettingsActivity.class);
                startActivity(settingsIntent);
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

