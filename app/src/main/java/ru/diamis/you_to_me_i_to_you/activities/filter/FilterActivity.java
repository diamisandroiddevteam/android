package ru.diamis.you_to_me_i_to_you.activities.filter;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import ru.diamis.you_to_me_i_to_you.DAL.Category;
import ru.diamis.you_to_me_i_to_you.Interfaces.OnFragmentInteractionListener;
import ru.diamis.you_to_me_i_to_you.R;
import ru.diamis.you_to_me_i_to_you.activities.account.NavigationActivity;
import ru.diamis.you_to_me_i_to_you.activities.common.CommonAppActivity;
import ru.diamis.you_to_me_i_to_you.services.CategoryService;

public class FilterActivity extends CommonAppActivity implements OnFragmentInteractionListener {
    public static final String TAG = "FilterActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_filter);

        Button btnFindOnMap = findViewById(R.id.btnShowOnMap);
        btnFindOnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),NavigationActivity.class));
            }
        });
        Button btnFindOnList = findViewById(R.id.btnStartSearch);
        btnFindOnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),NavigationActivity.class));
            }
        });


        CategoryService categoryService = new CategoryService();
        List<Category> answer = categoryService.Get(null);

        String[] categoryNamesArray= new String [answer.size()];
        for (int i=0; i<answer.size(); i++){
            Category category = answer.get(i);
            categoryNamesArray[i]=category.getName();
            Log.i(TAG,"data["+i+"] "+  categoryNamesArray[i]);
        }
        ArrayAdapter<String> adapterCategoryData = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,  categoryNamesArray);
        adapterCategoryData.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        final ViewPager viewPager = findViewById(R.id.filterViewPager);

        final FilterAdapter adapter = new FilterAdapter(getSupportFragmentManager(), categoryNamesArray.length);

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() );

        Spinner spinnerCategory = findViewById(R.id.spinnerCategory);
        spinnerCategory.setAdapter(adapterCategoryData);
        spinnerCategory.setPrompt("Title");
        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                viewPager.setCurrentItem(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });


        final SeekBar seekBar = findViewById(R.id.searchRadiusSeekBar);
        seekBar.setProgress(20);
        final TextView textViewRadiusResult = findViewById(R.id.textViewRadiusResult);
        textViewRadiusResult.setText(String.valueOf(seekBar.getProgress()).concat(" "+getString(R.string.kilometres_identificator)));
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                textViewRadiusResult.setText(String.valueOf(seekBar.getProgress()).concat(" "+getString(R.string.kilometres_identificator)));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                textViewRadiusResult.setText(String.valueOf(seekBar.getProgress()).concat(" "+getString(R.string.kilometres_identificator)));
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                textViewRadiusResult.setText(String.valueOf(seekBar.getProgress()).concat(" "+getString(R.string.kilometres_identificator)));
            }
        });


        InitSortSpinner();
        InitBottomAppBar();
    }

    private void InitSortSpinner() {
        String[] sortTypeArray= new String []{
                getString(R.string.sort_by_ascending_price),
                getString(R.string.sort_by_descending_price),
                getString(R.string.sort_by_ascending_rating),
                getString(R.string.sort_by_descending_rating)
        };

        ArrayAdapter<String> sortTypeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,  sortTypeArray);
        sortTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner sortTypeSpinner = findViewById(R.id.spinnerSortBy);
        sortTypeSpinner.setAdapter(sortTypeAdapter);
        sortTypeSpinner.setPrompt("Title");
        sortTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                //TODO make sort adverts
                Toast.makeText(getBaseContext(), "Position = " + position, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}

