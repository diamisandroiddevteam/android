package ru.diamis.you_to_me_i_to_you.activities.chat;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import ru.diamis.you_to_me_i_to_you.Interfaces.OnListFragmentInteractionListener;

public class MemberListActivity extends AppCompatActivity implements OnListFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentManager fragMgr = getSupportFragmentManager();
        FragmentTransaction fragTrans = fragMgr.beginTransaction();

        MembersListFragment myFragment = new  MembersListFragment();

        fragTrans.replace(android.R.id.content, myFragment);
        fragTrans.addToBackStack(null);
        fragTrans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragTrans.commit();
    }

    @Override
    public void onListFragmentInteraction(Object item) {

    }
}
