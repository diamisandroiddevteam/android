package ru.diamis.you_to_me_i_to_you.Filters;

import java.util.UUID;

import ru.diamis.you_to_me_i_to_you.DAL.User;
import ru.diamis.you_to_me_i_to_you.Helpers.CryptoHelper;
import ru.diamis.you_to_me_i_to_you.Interfaces.IFilter;

public class UserFilter extends User
        implements IFilter {
        private String login;
        private String password;

    @Override
    public void SetId(UUID Id) {
        super.setUuid(Id);
    }

    @Override
    public UUID GetId() {
      return super.getUuid();
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        //TODO make clear with obfuscate password
        password = CryptoHelper.ToMd5(password);
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
