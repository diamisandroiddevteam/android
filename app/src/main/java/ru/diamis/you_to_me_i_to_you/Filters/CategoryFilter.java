package ru.diamis.you_to_me_i_to_you.Filters;

import java.util.UUID;

import ru.diamis.you_to_me_i_to_you.Interfaces.IFilter;

public class CategoryFilter implements IFilter {
    private Boolean includeAll;

    @Override
    public void SetId(UUID Id) {

    }

    @Override
    public UUID GetId() {
        return null;
    }

    public Boolean IsIncludeAll() {
        return includeAll;
    }

    public void setIncludeAll(Boolean includeAll) {
        this.includeAll = includeAll;
    }
}
