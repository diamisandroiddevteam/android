package ru.diamis.you_to_me_i_to_you.Filters;

import java.util.UUID;

import ru.diamis.you_to_me_i_to_you.Enums.AdvertStatusEnum;

public class AdvertFilter extends BaseFilter {
    private UUID ownerUuid;

    public void SetId(UUID guid){
        Id = guid;
    }
    private AdvertStatusEnum status;
    @Override
    public UUID GetId() {
        return Id;
    }

    public void SetStatus(AdvertStatusEnum status) {
        status = status;
    }

    public void SetOwnerId(UUID uuid) {
        ownerUuid = uuid;
    }

    public UUID GetOwnerId(UUID uuid) {
        return ownerUuid;
    }
}
