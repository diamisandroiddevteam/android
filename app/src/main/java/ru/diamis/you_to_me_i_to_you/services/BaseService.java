package ru.diamis.you_to_me_i_to_you.services;

import java.util.ArrayList;

import ru.diamis.you_to_me_i_to_you.Helpers.HttpClient;
import ru.diamis.you_to_me_i_to_you.Interfaces.IFilter;
import ru.diamis.you_to_me_i_to_you.Interfaces.IService;

public abstract class BaseService<T>implements IService<T> {
    protected HttpClient httpClient;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    private String url;
    public BaseService(){

       // httpClient = new HttpClient(SettingsHelper.baseUrl);

    }
    public BaseService(String url){
        url = url;
       // httpClient = new HttpClient();
    }
    @Override
    public Integer GetCount(IFilter filter) {
        return null;
    }

    @Override
    public Integer GetCount() {
        return null;
    }

    @Override
    public void Add(Object entity) {

    }

    @Override
    public void Add(IFilter filter) {

    }

    @Override
    public ArrayList<T> Get(IFilter filter) {
       return new ArrayList<>();
    }

    @Override
    public void Delete(Object entity) {

    }
    @Override
    public void Delete(IFilter filter) {

    }
    @Override
    public void Update(Object entity) {

    }

    @Override
    public void Update(IFilter filter) {

    }
}
