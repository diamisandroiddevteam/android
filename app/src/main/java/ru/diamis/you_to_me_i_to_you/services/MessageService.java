package ru.diamis.you_to_me_i_to_you.services;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import ru.diamis.you_to_me_i_to_you.DAL.Message;
import ru.diamis.you_to_me_i_to_you.Filters.MessageFilter;
import ru.diamis.you_to_me_i_to_you.Interfaces.IFilter;
import ru.diamis.you_to_me_i_to_you.Interfaces.IMessageService;

public class MessageService implements IMessageService {
    private ArrayList<Message> Items = new ArrayList<Message>();
    public MessageService(){


        for (int i=0; i<25; i++) {
            Message item = new Message();
            item.setMessage("test"+i);
            item.setUuid(UUID.randomUUID());
            Items.add(item);
        }
    }
    @Override
    public Integer GetCount(IFilter filter) {
        return null;
    }

    @Override
    public Integer GetCount() {
        return null;
    }

    @Override
    public void Add(IFilter filter) {

    }

    @Override
    public void Delete(IFilter filter) {

    }

    @Override
    public void Update(IFilter filter) {

    }

    @Override
    public void Add(Message entity) {

    }

    @Override
    public ArrayList<Message> Get(IFilter filter) {
       return Items;
    }

    @Override
    public void Delete(Message entity) {

    }

    @Override
    public void Update(Message entity) {

    }

    public void BanUser(String userGuid){

    }

    public void BanUser(UUID userGuid){

    }
    public void UnbanUser(String userGuid){

    }

    public void UnbanUser(UUID userGuid){

    }

    public ArrayList<Message> GetChats(IFilter filter){
        return new ArrayList<Message>();
    }

    @Override
    public List<Message> GetChats(MessageFilter filter) {
       List<Message> Chats= new ArrayList<Message>();
        for (int i=0; i<4; i++) {
            Message item = new Message();
            item.setMessage("test"+i);
            item.setUuid(UUID.randomUUID());
            Chats.add(item);
        }
        return Chats;
    }
}
