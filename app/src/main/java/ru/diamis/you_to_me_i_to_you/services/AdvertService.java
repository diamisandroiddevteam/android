package ru.diamis.you_to_me_i_to_you.services;

import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

import ru.diamis.you_to_me_i_to_you.DAL.Adverts.Advert;
import ru.diamis.you_to_me_i_to_you.Enums.AdvertStatusEnum;
import ru.diamis.you_to_me_i_to_you.Enums.AdvertTypeEnum;
import ru.diamis.you_to_me_i_to_you.Helpers.SettingsHelper;
import ru.diamis.you_to_me_i_to_you.Interfaces.IAdvertService;
import ru.diamis.you_to_me_i_to_you.Interfaces.IFilter;

/**
 *
 */
public class AdvertService implements IAdvertService  {
    public static final String TAG = "AdvertService";
    @Override
    public void Add(Advert entity) {

    }

    @Override
    public ArrayList<Advert> Get(IFilter filter) {
        SettingsHelper.isDebug = true;
        String requestUrl = SettingsHelper.testUrl + "adverts";
        Log.d(TAG, "Try to request GET - " + requestUrl);
//
//        String httpMethod = "GET";
//        AsyncTaskManager taskManager = new AsyncTaskManager();
//        taskManager.execute(httpMethod, requestUrl);
//        String rawAnswer = "";
//        try {
//            rawAnswer = taskManager.get();
//            Log.d(TAG, rawAnswer);
//            CollectionResponse<Advert> answer = new Serializer<>(CollectionResponse.class).Deserialize(rawAnswer);
//            if (answer.isSuccess()) return answer.Items;
//            else throw new ExecutionException(new Throwable());
//        } catch (InterruptedException e) {
//            Log.e(TAG,e.getMessage());
//        } catch (ExecutionException e) {
//            Log.e(TAG,e.getMessage());
//
//        }
//        return new ArrayList<>();
        ArrayList<Advert> answer= new ArrayList<>();
        for (int i = 0; i<25; i++) {
            Advert advert = new Advert ();
            advert.setTitle("Loren Ipsum "+ i);
            advert.setDescription("Loren Ipsum desc "+ i);
            advert.setExtendedDescription("Loren Ipsum ext descr "+ i);
            advert.setDescription("Sed ut perspiciatis, unde omnis iste natus error sit " +
                    "voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, " +
                    "quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt," +
                    " explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit " +
                    "aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem " +
                    "sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit, " +
                    "amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora " +
                    "incidunt, ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad " +
                    "minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, " +
                    "nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure " +
                    "reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae " +
                    "consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur? " +
                    "At vero eos et accusamus et iusto odio dignissimos ducimus, qui blanditiis" +
                    " praesentium voluptatum deleniti atque corrupti, quos dolores et quas " +
                    "molestias excepturi sint, obcaecati cupiditate non provident, similique" +
                    " sunt in culpa, qui officia deserunt mollitia animi, id est laborum et " +
                    "dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio." +
                    " Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil" +
                    " impedit, quo minus id, quod maxime placeat, facere possimus, omnis voluptas" +
                    " assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut" +
                    " officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates" +
                    " repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur" +
                    " a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur" +
                    " aut perferendis doloribus asperiores repellat.");
            advert.setPrice(500);
            advert.setUuid(UUID.randomUUID());
            advert.setViewCount(115);
            advert.setCreateDate(new Date());
            advert.setLastUpdateDate(new Date());
            String imgJson = "iVBORw0KGgoAAAANSUhEUgAAAIAAAABVCAIAAAAOr3sAAAAAAXNSR0IArs4c6QAAAARnQ" +
                    "U1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAHCiSURBVHhelfwHVJVptjUKv1VmybBzz" +
                    "jnnnAMbNnuTM5KDEsScUECiKGLOOUdUFDOgoqIi5pwqh67O4XT36dOnvYuiP0/de+79x/hrzPGMZ" +
                    "79sFOZcc661UAvxuuO2bNp+/twlgEggpVGYACadQybS/HHB4bsPXjx7fepkT/PytvbWlQXTiiViR" +
                    "VZmXs+Z87k5BXNmL6hf1pSSnMHjiqqrZum0JqfDC28wGqwmow0e0mlspdK4cGFDd/fF4uLq48d7z" +
                    "5y5kpKeD6hb1rF3/6n+GyMDgw8u9d3tuz5y7eajq9fuX+2/9//Alb67gIHrI4D+a/f7BobHno9er" +
                    "g3D517sv/cJF/runr96BwCXT/feK0OAc5dvA05fvHXm0u0zl272XL7Vc3UUZ68O9ly5MYazl28Cz" +
                    "l28efbC4Nnemz3nBs+cvQE43XO9+/TAydMDJ071j+HwyStHuq8e7u4/dLLvYHffgZNX9x2/vP/Y5" +
                    "QPHLh86+j84eOTSgcMX9x+6ANh38Dy83Laz++iJvp17zuza27N91ykkmJAsFspKiyvWrF6/cP6Sm" +
                    "CgMh8VnMbgAi8n+h9/9+cSxU12d61qa2gP+pI72znlzF21Yv6X33KXVnevWrd20ds1GeAKSLG9s3" +
                    "bRxG5FABfZJRBoIwGbxIyPQCxbUHz165tCh048fv718+eb168O79h7bve/4sRMXgcRbd5+BBhev3" +
                    "gHqBwYfwjlG8f/G9cGHgGs3HnxSYvSd1+/Dp1z+v+PSwDAoMXb+UpIxGXou3j57aVSMs1dun+v7G" +
                    "VcGf4FbvZdu9l66NSbAJw1AgFNnroEAnwDsjwkwhjENDhy/cvD4lcPH/o1fagCX3fvO7th9Gqg/c" +
                    "vwq3Pce6D1x6hqSnJh25NDxhmVNBp3ZanYA7yQClccRggNsFufO7XtuDd6pW1xfXjqjrGQ6vG3f3" +
                    "sM3rg+dPtXbd/VG77nLhw+daFrenpmR53HHr+naWFM9pyC/FO55uUVYDAmDJjIYgtmzF+/efXhwc" +
                    "Livb6i//87Qvac3bj24efsJAMr/cv9dwM9WeNj/f/j93xi89Rhw4+YjwP+IMQh42PcLXL3xAHDl+" +
                    "sgvxfilBucv3blw+S5IDrjUd+d839DFq7cAF67c/Bm3L16+feHS7fMXb104f/vc+VujGvTePH32x" +
                    "ikwwZlrnzAmwNFTfWM4crrv8KmrgEMnrgK/UONwAj7JMPZyz/5zoMeYKuAA+CiSlpK5qqPr4vkrL" +
                    "5+/YTN5WDQBgigiLDosJDLWE/9w5Mm6NRsL80sy0rLbWjry84oWLljaffLs0SPdd+88ACWuXrm+d" +
                    "88heJidlQ/n8WOnk5MygP3585bweRKtxmQyuVAo0rVrd27evH/t2r07d56cu3Bt8PZDYB9KeIz6G" +
                    "7cfA8YEGKP4f2NMsDH8UgkIroFbjz+h/+ajMSVAgzEZfqkBkH756vCVvvt9VyHEfo6ygTtX+ocAl" +
                    "/tuj2L0DXcuXblz8fLQpYt3zl8cOnfh9tnzt0CDM+cGQYZPANKPne4HHD8z8AmjT06O4nj3wNhlT" +
                    "AkgGiKo+8wNsAK8hCyCJ2vWH+jo3IVAmUOwXOi9/PrlO6fdAw1AKlYQ8RR0DM7l8PaevVhdWbt0S" +
                    "QOItGHdZmgD1VWzd+86dOF8/9DtB9ev3Rnov331yuDlS9fPnb1y6eK1vqs3Dx08efhQN5xxvqSJE" +
                    "8LgrJ25YNGixt7evidP3oAPbt9+NDT0+Nadx+ADIHHo7vM7918ODo26ATBW6YAxrm8NPR3DL+9jL" +
                    "/+twa3HY7hx+8nYZVQS0OD6CAAC6srAMOAyhFLfXcC1gYeAG9fgi39wHRrPtXuAgQGw5u2fcad/t" +
                    "Ovcu3z17qUr9y5evguOgciC4Dpz4dbp8zdP9Q6OAVIIfAC2+CXgCaQK4OTp62OXMSVABggcwKatx" +
                    "8bEgCBqat3c3LYFSU/NggYLDeDdmy9mVFRTSHSofTyWBG4AJTLTc86eOX/pwtUrl/r7rlzrOd27b" +
                    "eveA/uPn+/tuzYwNHzvye1bI3C5d/cx4Mb1u6DBnaGHd+88evzoFdwbG9qPH+uB51D+9+496em5P" +
                    "DLyHExw8eINMMFYEI2yeecpOAA0uH3v+S95v33nGQAUAoy9HLuPvYS3gU5j1H/CmAAAMBNgTIYxJ" +
                    "QB9A/cHbzwZvPHo5ige3rwxAhi8cQ++QvDoz7jXf320vY9q1nf/0tXh3it3AZ80+IQxH4AtxvCpX" +
                    "f8cUHD+G2NKgAyAjVuOruraAwKACVav3QcCgAxITlbuzu27Wppabw3e9nnj+FwBHktg0JhsJgdw6" +
                    "cLle3eGr/Vf77vSf/PGLRiHTp+6cLr70uGDp3tOX7l98+Hjh2+G7z4buvUIzoG+O69ffn3n9uPrA" +
                    "/fgDg/he7t4YeDO0AjoBMK8eP4elLtyZfDVqy/hWx0cHAENrt8cgUIGQu/ce3F3+OX/K/vwobET3" +
                    "nDv/is44Q4fhbeBbL/EWJpdvwUaQDr9v2DwFsj25NbNJ7duPb516+HNmw/AlIAbIMONe9cG7/ffG" +
                    "Ibefvn68Fh29faN4tzVuz2Xh8ZkgBlpdEzqvXHu/OAY4N577sa5s9fPnL32KaMAY54Ya9og0q69p" +
                    "7fuOL5735mde04tbehqaFoHT2AKSjzdfWbzxi379uw36Iw8Dh94R0Wj4QIvXzx7CcIMXr8J7O/dv" +
                    "e9a/yAUy/u331++OHjy+PkD+072XbkN1A9evw/nyPALkOTJo7f37z1/9OA1vARbPBh5OnT7/s3BY" +
                    "TAKyPDwwQuwQl/fLfjO7959evf+c8iiMfaBYlDilwJ8Yv8T9cMjr+8/eAMn3Mc0APeMAQaqscuYD" +
                    "KDBGMYkAfwfkeANP78fzqFHt26NgDsBP7N/b2Dwft/N+1cHRy4P3r90/f7Fa8Pn+4fHBDh7ZTSLA" +
                    "DAjjeLCzU843zvY23v93LlrZ88OgAafbPFJA8Dx7r4xMdZu2N++clvNrIb5i9qXt2yAHmBfuWLVk" +
                    "UNH9+89QCFRwQEEHBFMQCXTRALx/XsjgKuX+04e7+45fXZk+NH94ccQ/RA+94efghugortP9oIq8" +
                    "BLofvrkzcj9Z7du3n/39htg/MHI8+E7DwHQsX+BR4A7/8YTwO27oxhV4v9g7AnMS4A7w88AIBXg3" +
                    "sgLwNh97Pkno4zp9An/9sfP7Xqso4y+EyLu3ourNx/2QeGP6vSw//qdG7fuDz94ev3m3asDN/sH7" +
                    "14benj11oPLN0cu3rh/5sro0nD15uOxBnDt9jMQ4MiJi6N9BfaMS7euXBkahIYPi8Wpy1dgt+jpB" +
                    "zeMJlLvzbHuDQLA0gC8j20Pew+c3XfwHNR+Zs6MJcs6wQ3I7No5K9o6VnV0Lpi3EEjHYfAcFnfq5" +
                    "BDwgcPmfPbk+fWBG+d6egEgw9CtO8DgyRPnnj19CyTu2L5/544D0IchaoD9U93nX738Aop9lN+hh" +
                    "48evhy5/+TB8JORe49BNsDwvUc/4wkAfPAzngHGiB7D/2/qhx+8BPxShk/sg0U+4ZMAY30C8G9LD" +
                    "b/oBzeA7e6/uHH38e3hx3fvj1J/8VL/0J2R23cf3BgaGbh9//qdR+cH7mzee7yudc2l6w/6Bh9f6" +
                    "BuGxgvsXx14AL0EmvnZ3uvwi1+9egdWy3M9fefPDZw41guLDiTSxSv3ei8OQfmDEmO1D3fQAFSBl" +
                    "wcOn6+rXz1nfsua9fuOHL+MVJSVZ6ZnHD54qLy0jEQgkokkPBZHp9LglIjEH96937Zl6/q16wav3" +
                    "+g+cfLJo6dv33y5a+f+I4dPXr92G4bRA/uP9py50N83eO/uw61bdsEYevnSAMyp8PLpk1ePH714/" +
                    "OD544fP4Hw08uzh/aejetwfBTgGMPxvPP+E/y/SAfcfvvqET0p8ev/YmwFjnzumJZhpNGd+ttSoq" +
                    "MPPbo68AAFuDj+H+9C9x0OjI8MDKI5bN+8BRrPo1v0Dh09Pr56flls2b0nbviPnT/UM3rrz8vbdV" +
                    "1DRvRduw/wKZQ7TKigKm82NGxBf92HZhIX/1KlLu/ac7Dl/A5rEyTN9Z3qvd/f0Hz15CS7Hui/Dc" +
                    "9jv9hw4XTu3Yc2GPfsPnz1+6gpSOX1GdmZW35Wr9UuXiYUivVYXExWNQaFlEimPw13VsbLn9Jn79" +
                    "4b37t6zeeOmzpWru0/2nDxxpmNF18EDR2HzOrD/yJbNO+/eGYGgb23pmDN74fZtu/fvO/L40XNg/" +
                    "9nTV8+evHz6+MXTRy+fPBwVA2SArgCApALc/zdeAEZGXgI+8fhLxkcevR7Dg8dvAGP3sQ/9b53GX" +
                    "oIqYzb6v/kJtHn0BjSAnIExDH7rRyMvhm7cvXz2St/5/hcPX17pHWhY3JybVRLwZ6SnF82qXXa1f" +
                    "6T34uhI2n/tIeD64GgPv3bjIeyGox27/w44+OLFa9u27UtNze3o3LR42Ypd+7qBdwDQDeehY+fhB" +
                    "D3GJNmx50TlzCVj7IM2iEGnb1hWf67n7Pat23AYbFREJINGZzGYVDIFPrRx/YbVqzq3bt6yZdPm0" +
                    "92nNm3YuGfPvtu37zQ3tRcXl2Zl5u3bd2Drlp07d+4GVWbNmlNdNautbcXQ7eGREXDAyyePX4AAY" +
                    "xo8AUnGfPBgVAMo/08CwGz6M0YFGH7wHHD/4QvAyKOXgAePX33CwyevxwD3sY/+UqcxjMkw5oxPG" +
                    "LMFCAMpdP/RW7jcHXry4tGb58MvDm8/WFNY2TKvoX7m4ur8qtqimfNnLAw4k6hYDj6a0dG25ey52" +
                    "7dvPb9x/fHQ7Rcwxfb3Dd+7+xKWOBBg4MZw95mLrSvWrOralJlTUFReXVW7ZO3GvVDvp6EnXxg8c" +
                    "frqwaO98HLME6AEOGDeohZ4Ai+hnSA+b+ySRYs72lfULV4CpI8VPhaNUcjkEEG//fVvnj152tW5G" +
                    "mLKYbM3NS4/evT4gQOHKiurrVZ7Tk4evLxx4+amTVsaG5tqa2cvXVrf3X36/fsvXrx4df/+g2fP/" +
                    "keAUQ3GTPDwOWjwyQFj7D948OLBg1cPH0Jd/z+p/8T4J/bHXv4fDf4/nfHJHGMYE2Z0jrr/6vmT9" +
                    "+9efD18/f7udTvmFc8sjM9cu6T9cNfOzQ1r6krnLa9e1javZXrmDJfW57Ynb91ybPDa44F+SKq3D" +
                    "0beXxsYgQ0OjAVbNPTwYyfPzZq7pGbOoqT07Lhg2vzFLW0rNwPLIADUOwgALEO9A/VwB973HjxT3" +
                    "9QFb4DnYA5kWd3S1uYWSJjC/AImnQGkA/s0ClUkEMZ6vK9evHzy6DGED7SKpUvqZs2snTNnTnV1d" +
                    "UpKSnJyckNDw/79+wcHBy9cuAAPZ8+e3dHR8eLn/968eTMyMgIyAPsvHo9q8EkGiCPAWBDBVAq9+" +
                    "uHDl48evQI8fvz/hwBj73n06M3jx2/HAHcAqAha/jLW4CVg7P5w5M3D4dfPR94MD4zs7tpRmVGc7" +
                    "Uis8Gc3l87tqq3fVd91qG3r7uWbNixatWHJmu3tO5bObVu9ctf6rj2bNxy8dH7oycMv7955cfnyb" +
                    "VD34pWbZ85f3XPweHlVrTsuYHF67bGBBUtaIeJXdm2HAh/9id7PJti8/fCWHUcgc0AACKiWFRuhB" +
                    "4AzDhw5h0CqQF3Prp0l4PFBgOjIKA6LDd2YTmU47Y4Xz54/HHlw/OixI4cOD/T1Q0soLS5ZsGCB3" +
                    "W7PzMzctm3b4cOHly1bNmvWrAXz5peWlm/fuuPhyKNXL17fHbr31RdfP3/64vnTV/8bzx6/Ah8AR" +
                    "rv0z7w/efIG8PTpW7ACCAAk/iwA1PX/CPBLDT4JALzD5vFLDcZ88KnkxwwxpsGD+y/v3Xz05atvn" +
                    "tx+sq1j45z8GVXJebkmn5XINWGYdhwnVaCrcaa25lXvnNt6qH4t6LG9Y1tHfdf6lk07OvdsWL5lf" +
                    "ePmg5uO9xy+2Hfh1sCVu4cOnV6wuNEblyRV6G3OOJ8/rapmcXpmyfyFzUdPXrx4dejStXunzvavA" +
                    "ilXbQETnO4Z2L23e0XnVngJDjh4/AIyo6x017at7c1NmOgYgIDNpxIoLCqTy+BpFZp3L98+f/Ts/" +
                    "es3D+4NH9iz+8L5c+1tLWmpyXarDRwDjQFadFFBsdPuCsYFDu4/cuLwySvnr3759qvnj8A7r7969" +
                    "83L529efMKLV8+fvxzFs9fQnx8+ePru7ZcwmL5+9eHJ45egx/Nn7148+wIq99mTL548effw4dunT" +
                    "98Dp/AE6IP70xfvx6gfkwGyCwbikXtP4RMhzcBJr999M/zoxf2nr+8+eXn/+Zsnr78A3H/0Gpowj" +
                    "Kewt8NXde18f9fylXPyK+ZkFRU64hxktjYU7YjEW6bEGD4PM3w21T4hIhBNKubKZ5o9TSUzOuYs2" +
                    "dPQ1d2++1zbgbOth8+0HDmx4vCu5p1b27d3tmyYN3tpUjBbo7LqlXavPeg0+0qLqltb15y/dKP36" +
                    "s0L14bOXLx29vKNtpWbzsP+fHbg5MlLS+tXbtlx+OS5/u0HTiJSIa+2avqcmTVRYaGRIRFRoZHoS" +
                    "AyNSKeTGAqx8syJ06ePn7p76/b9O3c3r1/XurwRBMhIT3U6bJUzKqqrZsCQWlFSmpeZm589bUZp5" +
                    "e5tewcuX3828vz7L3548+T9s4cvXr14+/JnjAnwCa9evoNJCR7eH3706uX7Z09fv3n9xRuQ7M13j" +
                    "x68ffr4w/17kBhA8evnz794+fIrYB8kAfahRY8W/rO3T56/A8c8fvDy4f3nr199Cda5M/zkwdPXQ" +
                    "w+ePXr9YeTVuzuPXww9eg5iPHjyFgS4efPRnVsP9m7ZObu4YnZ+cWNFTbbRro7EmqPxCSSGKyzGE" +
                    "xIVOyUqfmp0/OSIhKlR6TGEHArTTyCmcfkVGmudJ70rrXJPRWNP3YZLq/ae6th+tHPr/jWbdq/Zu" +
                    "K1rY1fzqrrauunTqpJjU7JTp50+daG5bfUuqPBrt7ov9B87c6m750rrinUd7RsPHz5bW1u3Yct+W" +
                    "DW2gQAum7loWk681xM2ZTIZR0JHokhYMp8loOCpLCr79bNXv/3xN8cPH2mqb6goKU5NDIJabDrF4" +
                    "7Bmp6f43I6Vba093Sfbm1sGLvfXLViycc2m61dv3B+Ccn3z7uWHH779NRD9CWNKjOHtmy/ABDAmj" +
                    "ckAI9MXH76FQn7+9APE9MvnX0KwvHn1zcjwi2fPPgD7IAMkzBj7UPuPnr4BDZ49ewfvfPzg9Yvn7" +
                    "yHHRp+8/mIYtvEX728/fPH03Tcv3n838vjN8PDLmzce7t15fPHspTXTCluqZi7IKwjK1BYCxU2ix" +
                    "eKpjgiUNxITH4kJRuNTMcQUNCEpEpMSjclG4wqxmBI8pphMLGDSCgXcCq1ivte+LCVu1/wZx5oXn" +
                    "OlsPN7ZdKSr4+jGDQc3btm+euOmVZs7W9Zt27yvrq61tXPjgZPntu47vuvgqRWdW44cO79184F9+" +
                    "7rb2zZCW+5Ys+PI6StI/ZKFyxYvkImE4AAGmY6LweJRBB6Tr5ZpRFwh5M+p4yeu9/UDZy2NDfFel" +
                    "1ohjnVbrAZVRWlBXnba1Yu9+3Zt37FlM4i0Z/vuXVt33hy49cWbr7768N13X/8KZp43rz+M4fWr9" +
                    "58AYsATkAF4hzsIADK8f/cVTEfA+9PH7169+ArOr774CQQA9sfyB7IIHACFDwAZoFVA34bIAs1ev" +
                    "vzi2csPTyHKXn8x8uzd8LO3b7761e2R0XUMPqv3zED9oraS3Mo5xTWdc+a3l5XnaPUWLCGezgzSW" +
                    "a5otC8a6w1H+cKj/RGopBgcaJCOI+ZTGDNYrCU8xjIueYGQUi3Al/ExxeKYcjm6Sotb4mCtzTTur" +
                    "0zeVZO9piK3q7Zy14r2k7v2H991ZPDKnbzs0pqahZu27b904y6sHd29AydOXwEBzpzpa2rq6li1F" +
                    "Xry5p1HuzbvQ9atXtnTfdxptbDpNHAAHoUDQCcAmLTGk0dOPHnw8NmjxyePHoEU9zgtHrsxKcFl0" +
                    "8vhjPfYNq9bvWXjGtDgwP6969ashSZ8787wh3dfAbnv33391ZffQ8q/ffse6n0Mn/QAwMsxK0Aiv" +
                    "Xz5Gt426pJn7798//3TJ2+G7z2BRHowAhJ+BQU+1qKfP3//4sUHqHRQAkwApEPhv337zZv33z9+/" +
                    "uHuyAsgffjJO8C1mw/v3HvRe/ZaS31nee6MmqLa5bMaO+ctXz2jKpXHtUZFJTMYQTIlHoUNYgn+K" +
                    "LR3aoR70lRA3JSwQFhUSgwql0Aqp1Or6ag5nOiFYswSJWGpDt9oxK6wYLts+HVW7CYHZWucsMsnq" +
                    "1KxHdhIYVgYNxrrVttuXb6zdf3utLT8JfUdpy9cv/vk7cDtRxev3rk++KjnzMDmTQfqG1fDXNSyc" +
                    "uuOA2eQFS3LR+7edtmsJBwWBKCTaNCBhRwBsC/hi8+eOn3v9tDr58/qFs5LTw6qZSKrTjKjJLN2x" +
                    "rSMRM/OTavn1FRML8tf09m+unNFaUnRqZPdsAR88cVXX3/1/c3BO/fuPoCXo3j3FQDE+ATgHZ6AD" +
                    "K9evXn9+u2XX34NuxvcB64OgnuePnn1c1cYpf7tm6/hBN5fvfoSKh1kgAiCbvzq7dfQckGS91/+8" +
                    "PDpu1t3nwzeewIC3Lr/ouf89aPHL6xetXV21eLqwpmLKha21Na31TZ0zJgXpDNTKIRUEjEJj0vCY" +
                    "tIJhAw8IRgenRAaGT8lzD8pNHFKWNLU8JSQiLSwqNzoiOnkiBpGyFxOxFJRVKs0plMRs1kTs0OHO" +
                    "mAj7bCQtjo467yyGiXbEj6ViiB4ZBIKCT2+++Q3b39csqgZlrIL/Xc6N+0+d+XWtl3HrvYPwxJ3Y" +
                    "P+Z3GmVHau3rdtyaP32I0htVeWp48doJOLk8eP0Kh1oQMIS4ZSLZGKeaODKVcgfSBjoEzqVXCZkx" +
                    "7l0RdkJ2cnuqrJsv8eQmepLDnoKp2VUV8EWWLF7985NmzYdP37y/YcvL166cuDg4Q8fvvwfvP96T" +
                    "AnAmAAgFZT/u3cfvvzyS1gpenp61q/dAH0b1oUvP3wDzfntmy9hRgIxXr549+7tV69ffwVWANLfv" +
                    "fv21auvYTq6cPn66Gj09PWNoZGLA0PnrgwePwWr6boFCxqrymYXZ5TOLqjZtGz1vtaNizKn+6hCX" +
                    "0xUEYeSSyVkE3ClbGYGGpMwZWoJmZ4REZMeEpkZEjUtAl0YiS2EMwJdEhM9gxBRSw5bTIlqpaHW0" +
                    "tFbWNgdfOx2EW6Xjr5GQ1mqIJbzsO7IqVwEoSGfs8fF2AWG7Z07ntx7efXyrb0HT8MYumLd9u37T" +
                    "mzYcuDajYdnTvfHx2UurV9dV796257uts7tSG5mxtrOVXg0ik6mzKquIaDx0WFRIACNSJUKRb1ne" +
                    "qB73ui/0rB0oVTEhdhxmqXTMryZic6asiyHSbZ4/oym+rlzZ1UsWTKnuWlZfcMS0ODJs6cnu09Pr" +
                    "66Zt2gxUDyGMQE+AToB+ODbb78HAcAyr1+/nD9/bmNjPSzkTx49Hb57/82rt3fu3IOMApdAmj1/9" +
                    "gYEgLQBK8D55Zc/wq7Q2zuwbFnrzaEHX37z0/3HL/cdgl63Cdivrl5QNX12alxaQSB77aLWvY1rF" +
                    "qYWBTkqP4FZJuQW0LCFFEwOLjo5PCQfjy+nMjLDorJDojInhedNiiwJw1RHE2ehKAuwjDoSrZlJ7" +
                    "mCS1jOpWxiMHTTGNgplLQXfTsMuZqBKSSG+SET+GcJCEDaCSD8Ll08leUXmI5sPv3304d3rb0+d6" +
                    "bN6EjfuOtywYt2+Qz1XBoYhgk4cv7Rw8Yp1Gw8cO3l19YZ9SKzL2dxQj0Oh8WhM18pVcAH2Y8Kjy" +
                    "XiCTCTevH7D73/90/quVW6HOTcr1WXTBmINCR4NOCDVb81Nj6soyZxRljN/zoza2orVne379u86d" +
                    "uzIw8cPzl+8kJCUWDmzZmBg4Ouvv/7xx5+++ea7Lz58A2xCY/jm6x+/+vI70OD77398+/btu3dvn" +
                    "jx9sKRuwfKmZSs72huW1cN0233i5MoVHbDKQWMAu4AVXjx/+923v4HB/4svfgW4fn141apNq1ZtO" +
                    "NNz4emz17v3HYbJr66uec7sxRmpeXqpLtUV2NbU2b1669K0aV480xVNLBHKC+ikUga+hIorIGIKC" +
                    "LhiIqkIQ8gLj8maGF4wJbpsKrpiMmpmCHZxFLUZx+7EM47yRPtI5ANk5kmu7DBftYUjaWbxamlUX" +
                    "8R4JoLwxiOEn9nnIIjys9A4ijBBbju5/ei7Rx8e3Rv9uey6rXtjkzKXr1wHqTg49ARW6L6+u3v2n" +
                    "VpSvxL02Ln/FALtd2blDGwMSsTjb1y7bkwACB94CQ6A/aC9ubFrVZtCynPa9DkZAb2S67UqXCZpn" +
                    "EtTmBN0WpW52YHa6pLlyxe3tzfkF2QP3Rnctn3TufNnE5ODNod93759Hz58+Prrb6HYf/Xjb4FKG" +
                    "Dehlr/+6gfQAMIHBHj69PHBQ3uqqssbl9fNrJmxtG7h2Z7TtTOri4sKOleuun//ASxu8IkPRmCxf" +
                    "g+AteDbb383NPQYJo2OttXz5ixc3ti2bGkToLCgzGH1iNiisqyCA2s3n16/pdqXYIvCJpHp0yWKb" +
                    "DKpjEmcwSHPYFNKacRCIr4Ah8+PweaERichk/MnRE0Pwc8JJy2JprfheGsp4i0U/obwiMNk4nEOf" +
                    "zeLv4EtaWaLp0WgVED35wgZQYgIIg8dl8ShudExCRi8D0fJt/q7tx55PvTs6chL8Gh2Ybk1NjhvW" +
                    "cumnQf3HuzuvTB46dKtjZv3T69cAIPQnMXNCItGjfO4CRgsOiq6dfnoPgybMLQBEEAiEDbULVFIh" +
                    "OUl+fm5aXIJN85jjnVoIPoVIlpxXtKC2rKulQ1Z6QmlRVmLFs0CAdJSg4eP7MvMSklKTkjPSM7Pz" +
                    "6utrX327NmvfvXr0SD68M2H99/89Ks/wgkCfP/dT2/evPvVr3748Vffdp862rm6dVVnS/60LBhw7" +
                    "966sXzZksaGZS1NzUO3bu/dva/33KWB/punus/fvjUyOvs/fNPT09/a3LV0Yf26VWvWr95QVljud" +
                    "cdx6FyFSDazpGLv2g1ntmzPN1tM0TH5In4enZaOjqoVc0to2OlMYjmDCA7IiolJj4zOCEdnh6EzJ" +
                    "kQVhxFqoulzo+mLo2j1MYxWDKsDS+kix2wTEXdoBKsU/OkEtBlBhAiiQxA+grimjCtgERZbVAt00" +
                    "riQz4uYhBK5pMwZd2LT/psXrl88c6m3t0+mM85aXN9z5dqJs5fOX7l+4fLg8eO9VTMXzl3QsH7zP" +
                    "ghMRMTjUgh4DoMZ5/HevHadiMVhozEkHB5agkomNWpVXpc1Ic5p0sti3Sa5lGkzyRfOna4SM4ump" +
                    "ejVgkS/Mz83JTPN39i4CGCzGqoqy9paG5OS/VnZqQWFuUuWLLp8+fJ33/0AKQS1D7z/8P3vvvjwP" +
                    "Wjw5RffwvDz/v3bq30XV3e1NbUsLqvIm1FeUDW9ZODqhQVzZ8Z6nVWV0+8P3z104GBX11rY2iBea" +
                    "qrn9V0BSY6Vls5atWLD2rau0uyCWKtLJ1XplVqvxVldVHZ469aj6zc2lRZrwkOd0WHpZFQeOaaKQ" +
                    "yoihE+n4aczSGV0Uh4WmxoZFQyLSovA5sRQ4j4LzZyMLo6gVEXR5qGZDUR+B126kstfzsc2ynHTM" +
                    "IgaUh5BzJ8jVgTRI0g5CTuLTljIJM1n4GbS0Znhn1UJSJUqcYHZ2bPjyMuhxzf7bg7evOsJJLatX" +
                    "b/zyLF9x06eOnfxxKnzjY0rymfMamnvWrNh59pNOxAxnxcTEQ6Jn5QQOLh3H/gATMCi0UESg0YNS" +
                    "6/PY5cIWVIRMycz6HJoDFpJnNdSnJ+emhTrtGmz0oN1i2Yvmj8zJdE3f04VnJUVhXDxeq02m66xY" +
                    "cnSpUvABL29FyDuv/3mV99/9+uvvvzx++9+++MPvwUB/vrXv3/77dedq9vLKwrmzKsoKcsKxDuqp" +
                    "hft2bmprHhaSmL84vlzjh45tHRJ3YXei+d6LjQ2tLe3dTUsW9HWsqZz5ZbZVfPTYoMKJk8rlKr5Y" +
                    "jlHsLCq5uTu3Uc2bVq3aF5raYElZqoPPTk+bJQyF4LkRiBF2KgiHBaSJxdLSEfhkqIJyWhyOp6eG" +
                    "EXIwTJKybxKKr+Wxl9AFyym8Wcz6InYcapJiABBVOMRLTIqQ+aUz7vU4k4OfQ2TtFvJbyJH1dHQc" +
                    "1jYGVxsgZBZ5vCc2rLv3fDT5/cfQ3JOnzlzdt3SstmzG9o7Zi9YPGvuIoc7rrlt1eq1m1o61qzZu" +
                    "BVBRUYQMOiIkNBYlzs/JzcqLBz6AYUAEcTVKmWw+jJpeBoZY7eozUapz2sUC5gFeelzaisXL6gFc" +
                    "8BHszMSkwLeOK8NjAIC5GWnTMtJhYvNrJk9q6q8ori4uLCn59xXX30Dw8yvf/rDb379Z+ilIAAsw" +
                    "NAbfvrpx9VdK/IL06cVpGRm+8tLIYJyIfGsJnXdorngg+WN9Sva2h89eLxh3eb8aSVNyzs2rt8FU" +
                    "3ZyYh6XLpQz+EaeSMcT+gzmpTWzzh48eGb37gv7dv/08E59XkquhH58TlGzU5oejmRPRfLDkQQEi" +
                    "UOQ+HGTkkKjkmOIyRhKIo4eJDCmceTFQlW5RD1dpKzgikuojKwodNyUSSIEof8cO5YJSB42okkmW" +
                    "CvjN2LCm6Im7RTS7uUG9xrFyzi4Smp0EQM3Q6uscMUe6tr8/Oa9h7eHBwauJ2VkBDIz8qaXuxOCI" +
                    "pUGR6LaXN6WFau279qfM6146fJWhMdiRoRMBdLXd63Zs2MnRBCYgEGhwmIsE/EFHCYwbtDKqKQYH" +
                    "ocgkzDsdn2C3+t0WDxum9GgycvNiHXb0lMCdotWp5aUFuWAADUzCksLMxN8NuBx9qzqVas6Tp06A" +
                    "0sWOOCH73/zh9//7d3bb377mz/9/nd/vn79+hdfvt27b1tNbWlmdoI/YMvO9Gek+cBemWmBFa2NM" +
                    "8qLFi6Yt2/P3v6rA1s2bV+/ZnPXqo3TcsspJD4GRWcQ2DwsWUWhu+XKzsV1Aye6u3fsOLxhzce//" +
                    "fE3D262F6XlSSjnl06/OC+nxUDOmoDMIk0qigpPmRgai0z0TQwLRhOCWFoCgRlHZCYyRMlsUSpbm" +
                    "MkVThOICgS8DColgIoxfz4R3DMthrCUKa5DkVfgSNvZzDXE6HWsmE4eqktLn8ONLGfFZJFjykSCK" +
                    "qOlxO49vXnnVw+evBp53N/fL1EpOHKx1mkVa9Qyvc7icO49eAg2pGs3hwpLK+YtqkPsZtPUiROIW" +
                    "EznivbL53txqCgmlYSJjoBTxGcZdUqljA8jEMSO2SDnsuCNUS6nRadVkIgYh92UnQXFHp+TmWQ2K" +
                    "O1mVTDe4bCoE3324txkr01XVZFfUphbVloITfnMqZ6//sc/IPrBBBBEJ0+cgRGooWHZtu0bFy6al" +
                    "ZkdTEmLTU33paTHp2b4C4oy6upmd7Q1HNq7/dSxQxvXdB7eu3/96nU102t5DBEFxyJhGFQ8m46lK" +
                    "ehcr1LdubRh4PTZ7V3rew4ceD989+Pvf/XTvYGm7NiFHunXB9tvN0xrVEbMwCH9lQm7El3LtMpCM" +
                    "ikVhUrCk4Ikup/MjqVyM2W6JJHSzxLFs7gpfGG2RJzG5yRRaVUy3WyhfoXO2yLQL4qh7VYZLvl96" +
                    "3mUOuLkuZQJVfTxC9TE5W5FmYQ+x6Qt12pneBPObd/7FAju6x++e09j0Ms0KqZIyJVJ5VrtoqXL9" +
                    "h84dOHi5Q0bN8+aPbe8YgY4gB4VNpVBJcDsATzGRE6KCp9AxEXgMWEsOg4AoR/0uyFSUpP8c2qnB" +
                    "wIeiZSLQoe53GatVurxmGzAu99p0kllfGp60O22qDKDHqdR5rNp0/zO5Hi7WSPLTkseHrrz5Yev/" +
                    "vj7v3zz9Q/Hj53asm3r4aMHzp7rnjO3KicnacmSWZWVxWmZwbzp+QavOa80e3HdrKMHt21c1XR87" +
                    "5br53taFy/WimQUFImOZVBQVGoMlUtgUSPwUhrzyI5d50/2dLZ3vX/zzW9++N2j20Mff/vDV9d7t" +
                    "lan1ntZZ2pcV2Y5VtsiakjIuXL9hXkpR6tT2oPGfAEhgRiZyKQkcfmxdE5QoIhlSZwMYSxbFM+TJ" +
                    "PDFSWJptkxRJFNWq3XpRHq1VDVHrmwyGxu0shoGOm0yMlcQ1WplHi/z3euYucTKnaNnV5lkxU4XR" +
                    "NCrW8PDA4N3r92ymm0ul8dgsadl5Ta1dezcu6+tvePgwYOzZ83My82eXlqCjEMQFp1k1MmrZxQDl" +
                    "UR8GIBBi2HSUSwGWilnC3lUvUbqcZpqqyvqly0Cmg4f3RXnt+OJkdnZiXFxNrdTC9ORw6J0mZUp8" +
                    "bY4hzbJaw64DAlOfbxda9EIs5JjN61eebO//z//8vc3r943N7Vv2rSl/1rf2g2dy+oXZOUkzppVv" +
                    "mJFY0fb8tlzZ85atiC9LLewoqCyumjXllUDZw8/uHZhZkGOnM6Q0TlsHB01OZpP4mCmxEQhU/U8y" +
                    "dbOrstnela2rnr/7tt//esjRNztvv47505+c+1ka4Z+lib86kLf4GLvWnfUYglyeoZ2d5lxaTynQ" +
                    "outMDBK9LxkAcNNpbhorHiB3MORu9myeJEqUapJkmtSVdpsjS5XIssWCBqTkgoUigKZpFAmnMalz" +
                    "dGLqyWkjljZpfk5w+2VT9bOa4uT1GpJ0w08EODYxh3Prg/d77sxdPWG2+pMS0lPSkpxe2LjAsGS0" +
                    "vK5c+eWlRbHx3mCcd787HSEhEdNnfS5VMReMLfaZddRSJESES0i7DMmHS3gkZQybkqiNxDvgjkHB" +
                    "EgMxqWkxDvdBhYHbzBJDQbJihVLIbW9br1yVAOFUc03aQQmFTfOofZZlQYZy+/SZyZ5Tx7c9+HVq" +
                    "4f3Hvz+t386d/bilav9XWtXHzl+YP6CmWd7j71796ymuqJzRev06eXl1ZWOWFdikn92dflQ37mev" +
                    "VuzvXYDlymhUFloAgNNIkRgJyHjKVEELU96eu8hcPrqlta//P4//vzHv/7jb//6w0+/OX/kYM+21" +
                    "ceaZ26b7mv0ULprHBfnupttUdV8ZFeheoGTVGbAVtmYi4LaCrsslklwM2jZBnO23pYo13t5MhdH4" +
                    "uGKfEJxQCJPkytzFcoClbrcaMpTqkp0+iwRf57TVO8zd6U6yvnR1WJUvZXV31B+dGZGc0BTZZFOs" +
                    "9lObN41fLF/6GLftd5LDpOltLAI5pDU9DSXx5mWllJUPI1MwkJHCMZ5jBoZEjJ53JSJn8EwAwNlW" +
                    "/MSAZeCRU9hM7HQb8VCKuQ+hRgNYyiYID83IyHBXVCQzuVTlGqeSELfvXu9w6GJizWBV2wmWVKC3" +
                    "WGSee1qWJUDHn263+YwiG0GSXF+akl+TqzL+e3X3337zY+nz/T2nO09fPRQR2crTJ8dK5tOdR/tW" +
                    "NHStbJj1sxap9MJC/CGVSvuXOndv67Dp5a4ZHyHWKCkMSgR0bQYwmRkAhNHU3DFpw8cOXv02NY1a" +
                    "//yuz/8F5D/57/87Y9/fPd45MzODafXN55urV6da2zw0HoXBI7MsHcGOXUOUoFoUoE6ZmYsf1mWd" +
                    "WGqZZpZkijl5NssYMMN8xa3V9Yuyi2sDKQUOryZBlOyQpkoEScLhPlaXQKHPz+YvDApeWEguDwtc" +
                    "ZnfvrkoeVWKdXWSaUuOZ6hrYWe6vUBMyFNyUzW6I+u3Xe8+d73nQu+xbpNGU5iXW1gwLRCMmz2vx" +
                    "hvrkMn5Uikbal0hYQfi7EjYVEj8GJhhYt2W8pIciZBBwkfSKWhoBio5T8Snwcmg4qAVM2lEqZSrU" +
                    "PDUWoFARAUBVnXWJyW79FqB3aqIdekC8SaPTeG1K31OtUHF8TkUZjXPrBfHx1qhUU/Ly7l549Z33" +
                    "/7qXO+FfQf2b9++deGiOfPm12RlJ0NX37hhzbFDh7tWrCzKyt6zYcOj6/0NlWViVJhLyPJK+B6JQ" +
                    "MdiE0PCMSFRpBicQa65dPrs8I3Bo3v2/v5Xv/rp+x8+/uu///OPv//Td1/86csXBzsWHWio6G2tO" +
                    "FATV++mnpjt76nLXOLj5YqjalzCGR5RbaK+NslUEa+f4bfPz0qpLy6oLyrZ09S2p6l99/K2bUsb1" +
                    "81b0Dajaml+/vyMtFp/oNzpnpOYOi8tc0ZcwvJpBXPifWvL8tcVpW8sSqrzqk/XlX/85tH1NfVz3" +
                    "JoshTBOLN/dsf7y4ZNXTpw5vnefTilLDsYlJ/nj4p0Ol14kYQJveoPIYpJCYGSnx432ADwmatP6V" +
                    "Qf3bTfqpLBwQejj0KF8Dlkh5UAEgR4mvULEZ8glfLVaLBTSI6LGa/UiKh3F4xOtNoVRL/K6dUG/O" +
                    "SHOmBRvTg3Y0hMdTrM0waMJ+oxOu0qrFbc01y9aOP/167fPn7+E8j93vvfo0cMnTh5uaV1WU1M2s" +
                    "2b6rm1b582aPaOw6Gr38ZtnjtdkBK0sojhiQpyIkW1W+2RCLZNBjYhkYIlWvflc9+m7g4MHtu/4+" +
                    "K//+vivj7BT/OX3v/nm1ZOPf/rxxdXjR1uqe1qn96+aviFXuyJJdGBmwjyPIEeOr3DK5qY5q4Lmc" +
                    "r+xNN5UkxK7rDi7o2ZGa1VlfWnFhkXLdtQ3721uB4AYe5tb97e0AHbU1XfVzls/d1FdfgmgNim9L" +
                    "jurtTBneUZgY3n2fK/28MKKjy/vHFo8s9ioTFdKnQLJumVtZ/cdOXvg8MHt22w6tdth9HpH4fLoJ" +
                    "HIajQ5jpNyg52qUjDivBoHyDw+ZCPkD2xNEDfggJnIKHhOBjg6B6VOjFKkVQioJDQJolBK/38Xlk" +
                    "jk8IokUSSRG0OgxJHKE1SxzOdQaFVulYJr0Ap9H67DKXHaFWcsdvTvVaq3IYtXDOrZu3bq7d4dXr" +
                    "uxsampctnRRa0vDxo2d0F1Wr2w9f/r08rplD24O9uzZkKTl6YhTAlIyCUFs1PBEGTOo4suIGGJIS" +
                    "JzN/vLx01sD11e2tX7x+jUI8Lf/+Ouvf/j+69fPPv75x0fnD9/Yt3Jw8+K+VZVfHW/bX+mp1mGqL" +
                    "bRar3xWojXXpi5L8JQlumsyExbmZ9aXFbZUVQD7DcWl87Pytixp2Ne04nD76gMtHbsbWvYubzvU3" +
                    "nG0o2tnQ/PR1et3N6/ct6Lr8JpNrdWz1s9fOCc1aVFGoCkveWmKd2Nl/vOTB1eXF2Sq5T6h0MaXN" +
                    "tcuOrF975GtO3ZtWBfvsthMcp9Xl5UdZ3fInG6Z0cBWKakGLVCtUEhJSEToJCjtB8M3e04dARn8P" +
                    "gdQTybEAOmQPMC+XMIV8ugwJiUFfC67QSxmhoZ/xmLhCIRwNhsPblIp2AadUKvm6TRcs0EIVjDq+" +
                    "MlBm1rB9LrUEgnDateoNbLS0uKCgoLu7u6+vr7VnSs3bVyzeUPn3p0bzp46dPr44VvX+kZuDnUuX" +
                    "5ZmESVpiGbK53G8UHUUMjdZvyjTHVSyZaSY3ED8F8+f7dux68G94XOnz7x/+fKff//bf//zH3/54" +
                    "29++82b754Mfn/v3I3ty863l13vrPj4pnf3dHelgbAsWT8vyV6b6styWauzMmuy0pfPKFs1exZQ3" +
                    "15ZvbJ65uKcgkK7p8Id35BXsm3BsgNNHYdbO4+0rQaAHie6NhxdvfHYmq37Vq7fUN96csvOPW0rj" +
                    "69dc7C1cXVV8YqS3KWZibvr5i/JykhRq518kYWvWFg+6+Cm7TvXrt/W1ZkKi5FJEog3+GJVQL1Mh" +
                    "rNauQG/2mbhA/JyPAgWFUYjY3736+9hY4Iah20gOmIqjYyLDJtMp+DFAhhDmZD+SpkQ9mEQQyCgY" +
                    "TChZHIMjOJcLpHHI2m1QqmMwePgFTKmySAG2Cxyj0vtsMkNOr4OJJNxrTZDYWF+U+PyzpWrNm1cf" +
                    "/rE0bPdR3Zt6zqyd/OlniMPhgZGbt3IS0l1qMV5br5XOL7cS1tTE5sombK2JvDk3Lar+7uWVGQ/u" +
                    "H7p3LGj4ACInf7LV57cf/Cvv//9n3//j9/98MWre30fhno/fnGzq8T11amOax0lH78ZODQrqUJPX" +
                    "ZJsq89PnZWZMie/oK5iRmNFZWft7LVz5nVW17aXTp+XlBbP4tMRhIuM5yATxONDrVhatsI4PzFn4" +
                    "8zFx9vWHm5fc2zVhuNrtx1et31v56YdK7q2t67c1dq6p7nxcHvT8Y6Ww62NO+uXgi0ayitrswpTr" +
                    "HF1lfN2rl6/qqFh94Y1LpPcZhAA0QYdXSnHqZRYk5Fqt3JsFq7HLgz4lEhU+CQWnTB0c+DRyJ3Mt" +
                    "MTKimIOkwJdgUxAw4QKC5pEyDHpVX4fDFEmmFZZLBKTiefxKFhsGIkUTaejoaeDIRg0lERElYrpc" +
                    "ikDPKHT8M1GiVLFEUtZCrXIaNLW1FRlZabXzJi+Y8vGK+dP950/MbeqcHNn48uR6xe7D+Qmxav4X" +
                    "KOYsaDYtmGRf0d9yvnNlW1l5p1LM4ZOdL3sP/LmzuUzB3b84cfvP/73xz/++reXz51//vDxx//82" +
                    "6++fvfN64evhi7cPbVlSbr+xdH2/7i+Y2TznO9Or9k+PVDrlM32W+emB5YWF7XWzlleNRPiZdX06" +
                    "uaCoua8glKjWRcSLkHGuVE4w5Qw9fjJUmSi7LMpYmQKH5ksQEKFEyI9DFGWwTE/p6Rr4fKdKzbs7" +
                    "9qyf/X6/Z1du1ua97c172tp3NFQt7W+Ycvylk2N7ZD+W9vXNdQuBgEObNmyb/PaeKcmNcGYmWq0m" +
                    "xkGPVGvI5iMZL2OpFLgDWqq08KDKWgcl0UOxHu2blrrspkVEiGTSgqbMpHDoMKSLBPxJQIu+ABiC" +
                    "gYhDpMkErHEYrZQyMTjo8bE4HKpcNLpWKmUyeGQhEIqxA7crVYlmEOlEVodeq1OmZ2VsWzxoncvX" +
                    "hzdu2t9R1NNSWZ7fe2T2+f3bWpNcms9epmYRsjw6q6fWtmzo/qLoa0f/3ave+30nY25Ty7ufHPrd" +
                    "O/BTf/4ww9//d1vPv7jn6DB1bPnf/zwxV9//au//fTVn75+uqmhcnd92eW1c14caRvaOHdk25In+" +
                    "1esnOarsCuWZSWtmTljw/z5GxYsWl07u6O4ZEtVNSxWCUSCZfLEFCIhg0y0TRzvDpnqmhLiDAm3h" +
                    "0UZJ0cokBAeMoGFjJNOiOCPC2d8HsaYFCnG0LwKY1lS+pLSiv0rVx7qXAHY2968o6l1S1P7lubOj" +
                    "c2rOxa3LKtZuLF1ZUf90jWtS2cUp8yqTHPbOR4HjJ4Mp43utDLMeopWSTBp6R67GGEziLAEQP60L" +
                    "F9qM+nn1lZjoiO4TBqLRgYxrEYd3MEKbAbZalLDtCoUsKngEHQkChUKdwIhGhUTDieFgoH2QKPh+" +
                    "HwqTEoQTWazUqkWMJhEhVIsk4sWLpg3u6rq3IljNy6da182b1NH3YfH15bW5FhlZJ+eX5kTYMWEd" +
                    "CyufH7z0OCZlR//+8XHf77Ytnxa745l7253719TDz324z///vGf//33P/3lr7/93YWT3a9Ghv/87" +
                    "Rd/+PBk6/Lank0Nf392eXj38tubF11eUfnNuW3nVy5cXZ7ZUpC5df7sXUsWbZg9a0Nt7dZZM9vSk" +
                    "4uEnER0RCGbXCVi5kNrwUXM4JDzidF5BFQWHpuGxwexhLgYvCuKYI8gcJFxbGQCA5lERiZjkAkxy" +
                    "HgUMg6DjItCEBEqOtWonZub3TZrzuol9auXtq2qawMBVixqWr28dWXDsm1r2qaluVIDmiS/1G2jO" +
                    "awUh5XmsjFdNq7Lyo9zy5MTDLCIfQYOyMtOO3Z4H51MWLpoPoWAx8ZE08kkEY+rU8nhoUIqgA48b" +
                    "3YlBJRQwOFyGEwGBdY5iZhPwKNQMRFw4rDRYAgyBcdkkYB0MhUlEDJZbLJKLWUwKQq5NDmQUFlc2" +
                    "Lx4YeuiOff7z969dLQ03W6RYD0aaoZHcfnoZiOfuH9t08f//uHLZ1c//ve3H//7m7dDpx5ePtize" +
                    "82jgd6P//zrP//0x49/+/vHv/7tjz98f/nUyTfDt79+dPvY+qY7Jzb9+m730xNd1zcsOLp42sEF+" +
                    "cD+nsXVO5fM3r5owbaF83cvWrh34fzOgpxaizqHiqrkE2cKiNPp0TNZUUskhDoJYTYrcj4fO5uDm" +
                    "8HA5BGik6MjfWFhtskh+vFTzVOjNJMixJ+HggYEZAIGGY9DJhA/n0CeMIGAICgEiUQQ9GfjmNE4g" +
                    "0AVZ/YWpk5bMGNOV1PL6qb6lY0LElzKeJcgLSB3WShWA8GsI9hNVK+dH++SB2J1SfFGBKYdAEQQk" +
                    "EvAxMyfPTN86hQ+mwUCCDhsvVoFAujUssSE2KrpRRmpQT6PZTYZXE47j8vk8zgcNh0kAT3oNBKHT" +
                    "ROKOAIhm82hgQY0OpHOIPEFbJ1W6XU6CrKzZ1eUtyxeeOnY/iNbVvp0HLeSLCGPS3Xy19eXv7p9u" +
                    "jrTe+Xo9o9//c2vv3r5h29fPR68+ObO1ddDffvWrnh0/erHv/wFyv/jPz/+8cdf/efvfnP9XPfqp" +
                    "fP6j2z7jze3Xl/YdXVz3b29TY8PtBWpSG25vg0z8w8sX3SgZfm+5U3bFy1eX1nZkpZUqZFkkyKqO" +
                    "aj5gpilUlyTAtssQ7XKUR1K7GotsVEY2SDBLBHja7m4Eho2i4BOiIp0Tw1XIIgMGSdCJvGQyUxkC" +
                    "gWZTEDG45HPyePGkz7/jPgZgvkMATeEIMhUZHwIMjn8sxCdULWwqqatbuHcGQXJcdrsFL3dSPR7O" +
                    "F473W6iOC30WIco3qWIdahgb0UoxGiZmEMhYmRinkmnTvTH41BoEg7PpNJ4LKZZr4MsGhuEYCeAH" +
                    "kAk4EQCoU6jhQsRT+Bx2VqNisWEzOGw2DSDUaNQSsQSPsQOnUEWiXkCIUetklkM+gSv9+T+fbcvX" +
                    "qivKfdphWlOaX68MjdW3DY3/cyOxn98/2BxcdLdC8c//vO/Hg8/fHjzzocnz75+8vT2+fNn9u57d" +
                    "ff+x//6COx/8+b9x//8x+sH99YsX3T5yA6YfL671f3q7Jbhfc1XumavLfGeWl65ZVbB4ZYl2+sW7" +
                    "Wps2r5seUd5ZaXVEiChs8gxSzScZi15uTSiWRa+To/dZiNvt+K2GFCb9TFdisg1Wmy7lrRUTpjJx" +
                    "RVS0EnRIe7Jk2zjpuqRKTJkMrQEOjKejIwjIeNJyGc4KHwEgZM8YRxx0hTUuCkRn4dGjo+EEz05y" +
                    "qnVL6iqWDKrNNYm9ljZPiczNSBJihcmePhxTr7PIY21gycUNoMEgemFSo4iYMNh1cLEhMPejI2JJ" +
                    "GJRbDqFjMdAGwABpCIuARsJSUXCR5PxOFRMFImIR0dFQg6p5RKPwwotGt4GQWTQq6QSPvgAui6NT" +
                    "gIxVEo5h0G367VbOldePH6wODlWSgmP0zGKkrQ9e1tWLcn/7vmVH19d//jPn5pmld442/3Tlz++e" +
                    "vT+wY37b+8/fX/vwZ3eC/cuXBo8d+Fff/7bx//6b2i/X7x52dlcv7Wj6ejGjvun9x1qnXVxQ92x5" +
                    "dNXFsftmpd/qKG6u33p7rqFUPtdM2eXOjyxNFoai1qt5C7QMJbIMc3yqE511AYDdpsFv8eG2WNFb" +
                    "dFO7RSP61KGr9bi27SkJTJ8FR9TyIhJwYbFhk0xI58bkQlaZIIKmaRApkg/nyL6bLIQmcRHxrMRh" +
                    "P3ZZ9xJk5mTw/Djp0Qik0KRyRHjJtGxGEzIFA4ZlZ/uDcQqPFZmVqoG2E/yixJ94ni30GsTu8xCu" +
                    "1Fs1YsRIQ/LokfD4MOgRLPp6J+B4TCwIh6VRcOKuHQmBed1mKorinwui0ouYlCIAFAIExVGjIk0K" +
                    "MV8Gl4uoLEpGL1axKbjRHwaiYSiUPEcLl0o5BuUhqDL371n96GNnW4lVUWbYBOHbmguWN2QM3Buw" +
                    "7vnlz7+5/fvnw1//Oc/ls1ZcGTnwXMnLgz13f7iwYuX129+M3T7Xvfxo2s7n9/s+/ivv3/813+eO" +
                    "n18+P7Q68cPtqxoPbRq5YG2pqu7NuxtWrhxXsWB5kU969qOdzTtXVa3a/Hi6TZXjkSeRKdmMonFP" +
                    "EyVIHKuLKxeFdZpiF6rRW3U4bdoMZtVUzcqx2/VT9pui9liZy5XEGcJMNVSQokoJkic6Iqc4IkId" +
                    "U+Z7Bg/wY6Md4+b5J8YHpgc7R8f6UFCnEio9bNQNTJRgIyDdBKHoyRoPA+DoaBCiOhJkVOQqFBEw" +
                    "AqHgQc6cGV5XEpABoOQ3yPyOgUw/ifAXqZhxrlUCJcVRSNPJeImUIiT2YwIPidGIoSuS5FLqGI+W" +
                    "avkiXhkr9NQWpjpMGvlIg4UO8RRVOjkqNCJQLrXohUxCRxKDI+GDsZaHBalSs6DgZXNokIz0GnVa" +
                    "9pWHd25u3lOlU/D5UQhVt7k4qDgxumOQ1vnf/znFz9+8/C//vbrP//u1z9+88OZYz1H9hw/c+Ls0" +
                    "7ujfw/9zY0bX9649uzCmdsnD109vv93P33Vd+3Cb/746zv3bm1Zv2ZbR0fPxq07ly7b37r86KrWE" +
                    "11tp9auOL2u/WDzkqXZqRlScaZAkEqnZFGJpVzSTBFxtihmgSS8XhlRLw9pkoW3yzFdKvxGbdRGX" +
                    "eh63ZQ1OmgAuHl8UjE1OgU9MYBBEvETgpgIX1io9XMkbsqk1PDw9PCorKmROVOjiqIIFThmXhQ5c" +
                    "SraGxLjjESrQiIYn4+jT5kkJEQLGVFCTqSIG6WSYWH2d9nYbjvLHytMDshhGAX2Y53ilIAuGKfRK" +
                    "qigASISoDmsCAYthM0M53GiBLwYsQArExNkYpJSRjXpBVIR2WqUpgSdOhVPzKfA2EokoGFPRkVM4" +
                    "TMILrNaLWGpxEyliAE+oJNRsC3z2DToyfA75OakH9y1qaooPc4sYsYguHHInBzrqgXZH+6dGb568" +
                    "ONfv//d929//9O3QzcGrl2+eqWnd/vGrbOrZ70YeTTS1z/YffL51Yu3Tx7pP7b/6qnDvb0n//nxH" +
                    "3/8+x/v3R+6dvH89pUrj63uPNLacn7D+hMrW091Np9b17ptQUmFS5LMR2eKCRkcXCYDk88kTucxa" +
                    "rj0Kjqxho6fw8XXCCNrxdELxYRGJbVDT+o0YlpV4XXCsPkc4iwmvYSAz4wKSYsanxEzOSMsPGHC5" +
                    "KyYmGxUdB4GVYwnVJAo08nUciK1iEApZ3OzyFQ/AW/HoSSRk2lTEQEhzKJgpPj1cW5pvEeWHFAnB" +
                    "5Q/Q57gEyX4IKu5IIDbJoAB1GkRKsQEnYKBSERoIT9SwIuCi1SMEQli4C7ko8RCjFJG1qkZPHa0V" +
                    "EQ0G/jgDJGASCBEwsQZHTEZExMq4JANGrFewTNrREaVQCZiqqR8mJREQq7bZZs+vXzt2hXw24jYI" +
                    "WwC4tbgS4Kq3p0tgye3/Pr5UN/xvX/67ou7A31vnz6+dbXv+P69FQV5ZdNympYtuXDq5K0L517fv" +
                    "vH82pXbZ05sX9VSkJF48dLZf378L9jB/vDH3/QeP3pm17azG9ZeWr+mb/OGi2s7Di6d1ZTtKTFSs" +
                    "yShBYrIGXp8qRRdzEeXcrDT2aQqOqmSTKwmk2pYxEoxqkqJmaOgLVAw6rWURh1uoTSiij41HxOZG" +
                    "hqZGYmqoNFn8eiVFFw5mliBpRRj8fk4VBERP4NJnysWLlTI5srEM8S8TDohk0cJ8IgWepSWEWGRE" +
                    "rxWXiBWlpNh8blHcybeKwLG4UwJKkCAQJw0KUGVGK+G5cusY2tkNGA/3q1GOKwQFnMSnCCDRBQDk" +
                    "IpRChkeoNPQVQoykx4KAEnAHDIpGRZdKg0XGTkqAMS9TMyCJsBjEblMgk4pFvMYsLV5PY7WlqaSk" +
                    "hIUaioGjeBQyIaOsraFGbU5tp8e9w2fPvhy4MrA8ZMfRp72nb4AA09r3dLpBTnVJTlleakbV7UMX" +
                    "j47fLX3xpljN04d3btmRZLHMXNG2cmTx19/ePOrX//4wzdfblnZcmbL+vuHd19d2969fMGOmmkNy" +
                    "cYqM7XKhJltx8w2R1UpJ9cowqqEYVWcsFouzJ2kRULqYhFtgYxSo4yabcLNNTDmqCnzNYRFOtwsW" +
                    "XQxIzwDg4qfGp0wNaaIzJjNY9cyaDVEZi2ZPYvNBd4XyERzJHwwUymfWqHgVJulQQ4qSU5KNdBTn" +
                    "dysgCwvQ5uZpggERKmJyp+jhgclH+vix3n4wXgZXGJdwmC8wusQmXUstYws4eEMKk68WzsqAIM2i" +
                    "U6dyGJM4bDCeJwIMIFcijPo6FYzR6Mic9nhTPpU+JCQH61UUMRiOo9PJ5JQsP1yuTAXRTJpWIBWI" +
                    "RJyaNAhRDz2gvlzYfj5HPls4kQkIhyZU+u92rN6U1vZ9pbq2yd2Hutqf9U/cPNk7/CFa/vW7agqK" +
                    "JlVWrq4tryyMG3/5hXfvR45d2hn34l9g6eP9h8/MHjm5JyKks72lraW1itXrgxcuXxk146+o/v6d" +
                    "2641Ll8/+zC1bnuBR5erYXY4Ge1JTEX2aNnqSbM005eqA5ZqAhfoohu1GDbTOR2M6XFQKg3xMw1h" +
                    "C5yohdbyXO1hHl69GIbcZYeUyiMSSbg46IItknhrkmhmdiYGi5zLldcRWUvlCnnyWUzJaIyPiuPS" +
                    "84RkqepmYUWXolPkm5npLpZxVnq6SWW/DxNapo4NVWSn2tOS9KkBFWpiTDTSwFwSQlqcjOtBTnux" +
                    "HhtME6bkmAOxBpSEuypAQcCjAO5Y9SPNQMAixGmVdOsZp5aSRILUWAOHidMLAQByBwegS+iM9gEK" +
                    "h3D4hDDwsZRqVgulyoWsDVKiVmvQUeFexx2lUyKioya8DkCNpw3M/7Ole3bO2q2L6+6eWRb75Z19" +
                    "7pP9mzdOb+wYsmM2ur8/I1tzbcunawuSGyZVzrUe/j8vs39R3ZePbT9yuE9x7esM0nEtWWlW9dvb" +
                    "FvWeOP8+d2r2s+sX3l5TXNHmrfRK52pi2pNpK/N4jX5sHWWsEZ7VKM5cr50QoMustmAXmHFrXRgV" +
                    "7mjVrqntjs/b7YjDTak2RPSaEcv1EfWWWMa4khzHPgcSZQuZIIYGcdCEDGCxBHCy2Ssapm4lM1Jx" +
                    "xEziMQsOq1IKqyyaWv9puokU3mytjLXnJ0oTvJz0lMEGanCgJ8ZTGBlpksz04B6ZbxXAL0XvndwQ" +
                    "7xXkpFigK4A7EP42AwCWMEcJpnbqrTqYA+ghsLwI+JjFFISZD2HGU0hTiXhJ4sFeK2KLpcSoDHIJ" +
                    "FgBLwLSSS7HqzQ8oYRJIEcDRBJmDHoqCABugNpnUAk6ldxi0FKJOIfJFOtweKz67RuWD17a83Twx" +
                    "I722p3Lq/v3rr1zbPfy0ryyBF/9jPL66sqe/btO7d1ydPvqVJdqXePMr4f7Bg5sPb99zbmtq7s3r" +
                    "j65ZX28VlOckrp/w9bTO/ceX7fx6s5t22ZWlEjpsxS0hVp8exx+ZQC9JoWwLoW8whuzWDNpgWxin" +
                    "SKsQYVqNRBW2UgrHegVjtAV7vGdPmS1H+nwjlvpi2i2Yxbro5Y5sc1B5iwXMUUUYUCH0pHRv2suD" +
                    "RvvoYfHkqdaQj/TjUO8MehMrnCm3bkoKTg/zV+V5i5NsxRnmhLjBKnJIkBcLN3twHtdxPhYqj+W4" +
                    "XbQ/b5R9u1mGiSP08pyWjmpQb3PJUsNmhwmkdMshRXMpBF4bRqPVY1QCOEcBkbEIwm5kOM4Fg1NI" +
                    "0WRcKHw0qgbbbxmA9dkYIoEUToNmcOOROMnh8VMiMFOIdNjNHoJmYaGOEJjwlVKCQw/TpvR73XaT" +
                    "br6BfP2bdvy4OaNl3dvf/Nk+MDqpu4NLYfb56+fM215cWBRXuyCPP/BNfVbWhduX7lsxaIZJ3Z0p" +
                    "jgUO9sX/OHZnROrGg+31fVs6DizYfXWpYuaplec3LD55LrN5zZu2zp7XqlOXSplLjLwFysILUbcj" +
                    "gzayripawJR+/JYnV7UMm1IgyZmniBsviC6SUtbrqHUKaPXxdOXGSe2Occ3O8fVGcetS+ZA9FfwQ" +
                    "pv9woaAsNJJS+BHKdFhJAShfI4kqjiH2qs2z03nI0iOlFVhs2drzTODyeUJ/pxYe3mWvzDHXZBrD" +
                    "wZkKnWM3U4djf5YVoKbnhbPi7VSEmJ5HifLZqI6LHRovOnJ2kS/0mnhpQaNsU6ZxyaT8omJcZZYB" +
                    "6xmVo9NBQJEMakYaKEcBp5NI8DaRSWgCJgwGCgNGoFSRjfpOUY9C0wAAtDpU0i0CBQhFE+OoDLRC" +
                    "rWAQsdQqFhoy2IxW60QAvsJHltGYtz+bRsvnzr+8Pq1k9v2dG/eubeluaOqaLpHOT/ZUB0vPdRWc" +
                    "XzNnJMbF9VXpmxbMe/wpuavHl0rS3Fva5r37c1LZ9e2HWmt27qgZmfdvAub1p5cvepM15pDy1urH" +
                    "K5MDjefQ12sEzTqma0a4morptX4+cZA6LFi9o50Yqc7ps2GmSucOlsQPVdImsOnzGBgajjYlW5Rg" +
                    "5HU6iAut+MXm7BV8hhfOGKfhLgikUTG1ERhlJ0RLo4MjUEQ6sTPsqyyvc1lh1uKMsSoQoMwVaOzc" +
                    "2VumdpvMiV77dmpntQUa3KqKSlVn5iiSklWJgdlPifTpSd6dUSvnpLg4fs8MG5yfG5BUoIiLUkHa" +
                    "5fTAsuXBtauOJfGpOFB9Cd4jUGf+eefBeFjaEQ0nYSBk0HGMykEKgGDi4nAoULUco5SytQoGWolR" +
                    "SnHmYxUDieUxowkUiMpDBSNhRHL2CAAiRLDYOEtFpXXZUz0OeMdxqLM4LY1bdtXtx5Yt25fx+Z1c" +
                    "5ZnaQzpcr6TPLXQQF6ULjvant9Ubt3VWrC1ufTV0Mnng6c+/vHrqsyEnS1L/vL03oaZZbsXzzqzq" +
                    "mnzvKozq5pPtjYuCPhTWawkIn6+XjNLzJgnwDar8Jud9DblpF0JmMOZ5NMlgnW+6LXxuA43sYTxe" +
                    "QkjtIJNquRyprO4MwXSJSpdCYVWSMBl4iLi0JMUn4/+7X4ZgjAQRDgeUUYgsuhJSgwODztt6JRCl" +
                    "2bVjMCuBek1No6fgeKGR1HCcfjQGD6NLhNz2Gy8RE6zOEVmO88fVMV6hTYDzWfl+Mxsj4Yab2Jbt" +
                    "ESzngQLsMVAtZtZMPU7zHyLngPln+DVBn1Gv0eXmexKDdigD49uwlDvoAEJOwoaEceikqgE7JgAs" +
                    "AOL+ASpCD/aBqQxFjNVpcKRqKEYQgiJFj0mgEDMAPbFYqbVpLQaZR6zKt3vWFpbvql96YamRT07t" +
                    "x1qX7euZnGSQDxNIwqyIyrM5LkJrPU19p3Lku+car3bs+4/v7v36MqRv375pDTo2tVU982NK7uXz" +
                    "t04Z/qh5sW7l8xsyksuUIvSWOQiISebiFmkFC4QkpuUpHpRaL1g3GwSss48cbXp83XOqSsdIe2um" +
                    "BrpxALm+BT8hAI2OZ1I9kyJCkQSp5E4qVEE/8RwNYIwkdF/2QICUH8GxA7+51McGU0dP4UzdUqOV" +
                    "TU31bw8z1Fh5BlQIdwYNA1DCZsUQiMROWxaSNjnKNwknhjL4ERw+VEwuciEaKeRE3TJAhaJR88xq" +
                    "AgGDRGgUxH1KgoMnQ6zwGOXQO8FumHuTPDqk+LNQZ/e51QmxKoRKjGajI/Eo8IJ6AiofRaVSCfh4" +
                    "E7ChbEZMbCFwUgqk0aJRFM1mmi9gcBgRWBwUwFEcoRQTNMbJTIZy2pVgl10Co5NI8j0W5rnlncsn" +
                    "LFmaU3fwe0DO/Ysz8rLkQnSebhp0pgji9Mb0oS9qwpu7J735uqmizsbHl/a27N9xdf3+ssCrtbKk" +
                    "iu7tqybW7Vhzozmkow0FTtFRJ4mYyYSwmcqecVUVBFmclYIUi+M6NBErTKEbPPG7PNHHUsnHMuhH" +
                    "S3gLzZMdUxGMtmTeTDJEKPiiXhLaJhh4mSgW/DzeAOnA4tMM8asLPGWmBi7F2bMT5bII0d/qDn2o" +
                    "03C558HNfLKoH12srPEqvWLxHaZRsoWR0wJYdHIarWYQIqIQo2LjEGozNCoGIROC9GoqFoF1arjO" +
                    "E0ipYigkKA1Ciywr5ETFWI87KF6Fd1uFOoULJtBBDLA3WkWwyYMCPjUCBkfDlzj0aEAcAOLimeQs" +
                    "fCESoygU2D2R2k1BI0GJRZPEoknyJXhWg2DzkCBANGoiWRqtEYnFIloTrvaqBXGOzUJLnW8RVKR4" +
                    "a1Ic83N93fNKX1x5uieeTVFSmYmL6pCjTm9LGtxPP3AouC+uoyb+5cf6ag9t7V1/4rF147smJub3" +
                    "D6zonVGWcfMikK3wUKP8XLQHlqYBzuhRERMixlXy8culRIXcSNalVHrHdidCYS7iyxbnJ8fS0PtT" +
                    "UEfyGO3+Qjl8pDdszLiGZHqiPGcn4sdxADe43BIWwqrp94zuD3l6anSF921S1Jwv73V/v7CotWVy" +
                    "pnJPBRY4XOEMGmcWynN9tqm+VxJOkuCxqblScUsPoNEUkmFdotareTwBTioQo4gBkeaIJUTrXaRU" +
                    "IAT8KFHcmFmAUOo5Tiofa2CIhVgecxIIRutFJM1MgZsXmYtV69k2k18cAasxCkBA0LATAXg0aP4u" +
                    "SGj6ORomIIoxFA6derPs3+MWh2hUE4RiT8TSyfbrAKBkEyjx2BxUwmEcI1GIOCRXA6Nw6JI9VvT4" +
                    "ww+Pb8gYCgJGubkeeoK4irdihW57nIdaYmPt9zP66lLX5Orvr1p9u75mbf2th3tmL+nef7qORUt1" +
                    "cWzMxJX1FbWZqYmmTV2Ic0jIttYEWb8uERGSLEAs8zIWywltGlpx7NNW730dsPUBjVyqVJ4IHHS8" +
                    "czQzXETNwQx67OEi328nXOnOakRVmo05ud4qcsVn1udcHuXb2in/vZ2wZfnzb+/lfDjpaSGZOS/H" +
                    "lZ9fLPkeJty1UwxbSpCnIowsFEmlcKsU3sdHp3cKeMa2HimmM50Gw0JLmuCxwTp4XXJgQSeAMUXo" +
                    "9R6hlrH5IpwAglZYxBq9ByHje+ycSH6bUbu2MarFFPNWr5KQgMNwAFaOd3nkoMA8R5FWqIBwaAmY" +
                    "NETcZgpACI+lEaJopEiSfipZMIUWBFGfzTED5NIQnU6lEoVrlBE6rRMgYDC55OhHXE4BLdbr1HxU" +
                    "pJcLpvK71b7bTKPljXNr8v3KWZl2eekmevTzM1pxgVuQbkKO9fOuLVu3uJ46cje9pVlKbvra5eVZ" +
                    "CyryGudPb040Zdo1iboZUlGcYpJlGzkJ5vYDl6UBoOkCWMWeWXztLRGPW2FidJlJRRGIm3a8dv9M" +
                    "Rdn8HYGxx/ICT+QTzg4XXxqUfzSoLA+w2ImTPRyYoB93iRk9xLzvQPBe3u1w/tYX5wVfXdJ8Y9h3" +
                    "3/cTFhbhPzHvayP3yzuXafZ1WjkRSOYSQiPSjCo9WKR3B2bpDMGmAwVnUBnEfBuoz7Wqou1qLIS7" +
                    "S4TX8qNlvJjTFo6LEYMVrhQQtQY+Bqz2GiV+GKVXocI2IdcEnFRAhZKI6PB9CnhEcRcrEXHU0qIf" +
                    "q9Kr6LFueWJ8WoEg52Ew08BjF1I5DAKKYyAAzGmEvCTWMwoHjeGRJyIw37OZobKxASpkKpRChQKH" +
                    "p9PlSt5Lo8xNtZkMUlVMpZRxbLIafEGTo5HXh7QlfqU0z2ybCF+WbyhLtYwXcdfGGdckZ9SatUsz" +
                    "EhsmV7aWFVelZed6LZbVOAen9dhibfKZ6Zr64sMDWXOxgpfnAy7KM87J9G0MKDL50fB5L47XbnCF" +
                    "NlpmnQgiNkVG3KunHluvvjpzpS3R8pGdhR+dbZxloe0Y1bQjkNyZTEQ/ZA/p5aZvu7Ofr5H9+6g5" +
                    "JtT6j8OBP56O/uvQzkdBci/XtZ8/NBwZq1rd4tfEIOwoicLqUyVSK9V2rVap0RmojL4GBw2KjIk1" +
                    "mX02bQ+oyxWw0uyiOcVxNfPSEtzSxScSC51KqyxBiPf6lToTMJAgt6gpqukJJh84j1Kg5phNXAhi" +
                    "xikULdNDLVv0o62ZXgICln0bASFnYglTAFg8JMJxBAKNYJKiySSQsdkYNCi2EwUXMAodEoEbGc8O" +
                    "lYhYavkPImQoVTywQHxsFC4tNBh9ApGvEWc5lKUBg2LpvkaSgKVXvl8t6rRb8kTUQrk3IVB38qKk" +
                    "mUFBc2VNbm++GSnJ87qTPD6nFabzWJVqVRZyY6LO+burPO1lhsPtBfPTNfNz3XOTDS25McWKjCl4" +
                    "tADhdpNfuLuAPZUDnV/IPxsGePRtsT/HFn+x9uNP/Qve3FyQX0qd8fsuFT+pFIdXj4OUSLImaWuL" +
                    "48VPNvh/nDY8ZsLST9eLP7jzVk/XZ/eURr69bXyn4aXnFybvKs1U4xFWFEhYgpbzdeaVHa1Qi+Xy" +
                    "+lMCgobGRo2wWZRxDt1yS5tvI7vkZIzrcL2mTklAb1NQpAyIsQclFJBU2k5YhlVJiaxaWEQ/ZA/L" +
                    "qsITmjCLGoonxUFjAM0cgqwD/KANtAMEGinWHwI7mcQiGFkSiSASApn0lEkQhidGs1ioInYEGzMJ" +
                    "IgmHgMjZOL0co5FK9bJuVolz25VAPtOqxI6jIAe7VAwzAJ8goY1K925u6mmqzKtysLtyHDUJRjXV" +
                    "WS3FWVV+n0ZZotXqU51x3lNDoNMZ9PbjBqDQiwXCcQ1Ren9exYfaU5uLFAfai9anG9fmOeoy3XWZ" +
                    "1uzhWGVyvDuaucGP2FnIuFCmeRwGn5nauTwJs+f7sz9oX/mm56qod1FjVmcDdUuP3t8lhwr/AxRT" +
                    "UaOLs1817308f7yV0cL35+Z8dXF5Xf2z9w635SqQrYvtV7YWbKlIbFjTpKUMIUaOlFEoqrYIqfeo" +
                    "JUJYAeiUSI+/xyZMgVx2zUJbp3PJNLzcCYuKtnEnZXjyYtVOuRkBQyKApxWxdTp+RotVyzAC9jRY" +
                    "h5aLsIbNUwJHwMXPOozKmESnTQFhIGXkEKQTgCYkRAcIQxHCMXhQkEAPD4MqIfWCncOC0chRdAoo" +
                    "wLApASNGpozCCDnEk1Kll0rNCo5eiXXqhfbjVKTRiDlEmjYyTohUcvGZrs0MxLtHdV5SzJdzZnW1" +
                    "QUeyJBKj8rBwKpJWJdMHrC6THK91xZr1TqMaqtZY9Er9HwmN9vvvLBpzuHG5Ao3sS5Xk2kkTvdL1" +
                    "s5OW1PlT+VNyBeOOzHTuSmVcXCa4Oocy8VqzdmZssc7k/58d8E/Hi9/1l1+70DZkgxmV43bw544z" +
                    "chlfzb6Dxyb8n0D25aeWlXWXCALiEb/bTtrCsIJRaiTECUJUZAQ0mQENx7BT0D4aFSq3Z3mjg3Yz" +
                    "HI23igj6ZVkDhOFjpooF1INCnacReKCmBUTLSKcW0l1KakGIU7GjlFLKBadwG6Wu+wKh0lk1rF1S" +
                    "hrEDnANjMuEOBAAHYmQsONBD/AEPCRixsFDAILDhWOhwLFhwD6JFE0mR1FJMVRyFJuBJxHCYUtg0" +
                    "dAwHcFgyqCgQQAZC6Ph43UiikZM0UkYJhXXpOJrpSw5n8IiRApIUTIqemZOYmVa7OwM/9w056qSu" +
                    "OYc88JUQ55FoCVF2cR8vUBslOuterfLFhBwVAQUjUnkmBQGm9o4ryjn2s6lh+qSc3SRs5KE6Xp8i" +
                    "Ze7ZXHmlvkphYaYDB5yoMa2KUuwPU+0O196sFh5ao759ZGy/3q84uOr9c+6Z55dnz07hbW00OTkh" +
                    "/sVdF7kePxnCGMqwgKEIDraeA1tcqyKr6DjudjJHi0jJ0FZkW0rT3dV5gTmFOSm2p3xGqNboUpzW" +
                    "lNd2lS3zKIgQa4yyRFQXmIWWs7DcohTOPiJTMw4LmGSTojTiYkyLkYvo1s0fItOBLXoNstser5OT" +
                    "heyUGIOhkePAnlo+CkscpiIjYZfbexDTFIoGTMxaiqCYDCho39sggklEqOoVDSNhmHSsMA+jYzCo" +
                    "UOBehYNC5saARNGI8VwKWgQQMFGy9loKQcr4xBkPLKMSxFzSDIuTc6ji+hELgGVbDc4pHy/Tj4ny" +
                    "7840zw7KK9O0PikFGbERAWLziDRhDy5gKfWqJ1moy8hLi0zKSsnMd2lM5Ql+/q31x9uzC20ksp93" +
                    "DQjoTpNuau5sGtWfKY6LF8dcqohtTFAX57A3FSo3T3DsXaa8sBC74WunH31vh11vuYK/eJCXU223" +
                    "iHHpjqVJhFVL2QYRHSnmue3SnKC2gSHJDspLtnnykr0Fuf69TIqNgIBEiLHIVGffRbz+WTchBDCp" +
                    "BDylMk6NiXRKs30aawwtuv46QmWeIdcIyGS0Z+L2ZEGBZmOH2/RMB16nkHOgDxwGWRWNWSXFJQAi" +
                    "pUiErAs4+MFzBitjMYghsBDYF8lJoMMIIyEi+XSIkEDECA8JiYEhQolEKJpZAyVhIaTScPDUgbrM" +
                    "Y2I5tCJZFz06B0fwyGjFByCgoODU8zE82lYDhULbxCw6VIB16BWmNVKGYctpFJREycSpk41cmn8M" +
                    "MTFCQ0oaXYJjRg6ARseQSTQ1FqnNz7DH8hJTy8qL6ksKygpy85O8dgqM+PObViyd1nBjHhJgZfvk" +
                    "aMK/eLNTUWblmX7ZVPyrZjza8uXpIrrM5S756cfWDqtLsNUZGWWeQXpemJ5UFyWLF9SGTcz351g5" +
                    "2cmGD1WaWrA4vdofC6Z1ynITtdnZhjzp/lTk52lJYnVVRlpKSargYMKQyYhyGRYhieGkEMxSoZAQ" +
                    "WXG6XQaNoUaPh7sLqJFagQEnYxsVNH5rEgJH82ihnDoYbFOGcw5Fi3XbZR6DXKrQuDUiPVSJpALv" +
                    "AOAbrib1Gy4A/sGJROoByvwGdEcagQAdIIIigQBAHAh4qLwmAgAXIB6EjYKFmM+i/JvMfAxbAqGT" +
                    "8eIWHgJmyRik4F6Jo0o5HKUcoXVbJHCEECgoCJiwiaGYSJRHDJDyWZxIj5LUNHi1bwUh0HKZhJwR" +
                    "LXOnjGtIj23YlpRdXFZdWlpeW5qUmlOcrbfCvvzhoVFy0v8S0sTdq6onVUYm2BlzcgxNc9NrspUZ" +
                    "jtIx9ZUtlR4luY7qxJ1BW5ZrIRs45ECGkGuR5fpVWbFKVPj5HnpJr9HkuTXqRVUl1PqdIjdbqHRR" +
                    "PZ4uW4Px+kRur3SknJ/cbE3OVmTnKhVSEliLpGORfMoVOzUKB6RwcWRs+Lj/RajXsTUCghKDtqso" +
                    "DkMXK9NrJGTTQa2XEYScFEJcfoErxZ6g0XFM8u4Kg5ZK6BLWDgIHBJ6Ap0wVS4gAFxmMbAPSsAJn" +
                    "oBQAuoxEQgFO8lrkyHo6DBMTDicqKhQAjaaTsHTiFgCOgqPihz98RwOTcdjACwSnkcjC1kULpPAp" +
                    "GJAEiIuhoBHk0gEKpVOo7OJJDqVysVgKFFReByGjkaR8BiqnCfmxIQFDPLceFduUlAulNJpvJSMo" +
                    "tzSmvyKWXklMwpKSouK86Zl+lPi9KmxirwETVmiMd0q9ut5YP+a4uCy+fmt9cUdjSUrl01rmJU4p" +
                    "8hVnWsrSTb5dDw1l6AXsTw6rUEkVAnoAY8epmyXlW82soJBLfDuj9dYLXyzma1R4wMJkrg4ntfLd" +
                    "DrpiUnSWB83GBR7PVy9juB1S5w2qd9t9NoMOpmESyU7DFq7QeWxaj0WJWSXU8M0KygWDd2sYSrlJ" +
                    "AEfzeagtBqW26lKT3ElxhpdBoldzdcJqBoeWcUjwZzCp8ZwyVFSNl7BI6mFVFBI9PP8ImZiwSLwR" +
                    "EhHx1oUPqtyVAAcOhKLihhTAjQgYqKx0eGgAQAE4FJJAgYVIGTSpFyWWiE2GtR2m8lg0AmFQjKFh" +
                    "sFTsAQ6nsSm0AVUuphMF+GJ3MhoChbPlolVGh5vWtBfmJZWmJmjURokEp0/MceVkO6KT3L5A15/r" +
                    "NdrdlihfTHjHewMnyQ/YEpxqhLsysygraIouXpGxvTy5NLC2NrpwWmp+lgTy6ammiAmGFgwJRmP4" +
                    "bNZJp3aalK7nRqzReQP6OxOsVbPtNiEwYAhMWjMTLcHEzS5meaUoCIxQZSSKPU4GT4POxAviPPyz" +
                    "CaKyUD3x6nNBj4M06mJbpNBAt+ZXsvj8zASIU7JQ8Vb+fFOiVyAheSBVdSg56rU7FiP1maR2i1Sj" +
                    "w1WBI3bJFZx8HxiuJJLFNBQPEq0Uc5OcGohnYBukAEAMwt8yCBjwZOsoNOph2GEjeDQ4aN/4ZAQA" +
                    "7FDQEcQMZGQ+AA6AUWICSNjInl0ophDEzDJIiZFIeCa9RqTyaDX64UiCYlMj8GAX+hkuoDCEEZD4" +
                    "eNZBLowBsuYOBU9YUp0aFg0Jiwy2RNbnJ1XWlCmVhlkMp3V6ddaXDgyjSvmG8wqp0sVF6tM9Mszk" +
                    "hU5qdqcREuKz5zst+Vm+ivKsssrsgumBTNS7X6v0ucUu80Cn0PhjzU5bXqDUWNzWI0WrQsWCo/WE" +
                    "681Wnl2t8hg4hhMXJtVDBUa69JnJnvSgvbUBLPTJDCqqflZ1qBPmpWij3eLXRaOzcjm0EP43MhE2" +
                    "KqsgkBAY7Jw9CZ6MFXDFYUSyEhqQOOzC9RiHJ8RAfO70yy2GEUSEZXDwHCZWLmQbFRzIeXVIqJGT" +
                    "LKpWOhQBDUVQYcgoAGwDxoA0VY1H86gWw8lb9MINCJaYUY86AGeQAjYyDEBAJ/+cGb0z2fwMfhoG" +
                    "JUiIXYkXDqfQZKwaUalTKdVikQCMpkcGRUTFh4dFY3FERkUOg+SnUzjgwx8iYrKEsTgKCg8NRqDp" +
                    "5HIgdi41GBKSlIqlyOkjv5PGTlkOmNKWCiFTlCouBarNNarCAYUwXiJzy1I8Ki8diUUo9drDAbdS" +
                    "UnesU3bbZUmxxvBFql+a6zLqFJLJUqp0WZKSHT7E+3AfiDFZLbzNAa6WIrXadmpyW6PQ2/TqQMeZ" +
                    "2YwLjXe5TYpzbC4aFkwzECgx1olcU65yyyEvYnLCI+LlXu94kBA4fHyZKqY5HRlQoo0PihRydAem" +
                    "yA9aEoPmqGfuywyWHq0cjYAOgeLEqmR0QJedUaiOTvFlhyvg21UySfDfAghAxQD75A/cNGK6UA9x" +
                    "A44wGuWQ5GBPPBRhISPxGPCcKgQInZ06odZ8+epP4KICsVHT2WSUAohUylg8qg4AZ2gkfJdNrPTY" +
                    "XO7HB6Px+v1edxxTofXZnXr9GYyhYHDk2l0Jp5MIVIpfLFIppCCWgqFjMthKZVytUphsZisVrPdY" +
                    "fa4bfHxdr/f5oszxMXrAwm62FiZ1cyBHIdENhnEer3YYJABdCqhSsyEIop3KJPc2jibyglbg0Fhc" +
                    "5iTUhPUar7Pq/OP/gp6i5lnt4mUMqqQS3RadDIeh0+jq4Qih0Fn0ShNaqlFLcpJ8QS8WrtelOq3p" +
                    "8Y7dFKOTEARstFJfj1oIBFH6bR4hTw61svzxfL5vKkkHGLS0t02EWytMh7RaZTBDinnUWEMMSp5C" +
                    "gEZ2qxKSrQaWGlJuiS/BtwAUymQDoALUA/9gIkPgwiCBgC2gIfAPlAPF+gHCIuOIxOiYMyHYZ9OR" +
                    "oEAwD4eHUpCh1GwETw6XiVmK/gMJlgEFTZqC1SkiMcGGVKCgbSk5ORAMCWQDJfiwiKjQUchExl0s" +
                    "lIldbos3liHza53uC02p8lk1dnsRn+8x+2GxNe5HXqf2xTrMXjdeodTbXeozBaJUsMWigh2i9BhF" +
                    "piMsNbztSqhViXWSLkKHhXmvIBN7TdJXRohzFMGjVgq48nkfLmYpZNzDQou+MZmEI3+nW+txGvRW" +
                    "1RqBV+kESsMcpVdp1dJhSad1AXimaVJ8WaoYqNKAJ0WGm9mks9hlIs4uKQ4XYJHmZ1qcZg4SjE23" +
                    "iW36tgSEdrtFHndMhAJHYZQ0SFqIQN0S7DpYPanYaZyqGEgT06WrajQbTExDUo6MM7AhULOQHMG3" +
                    "oF9iP6xbgy2KMkOQBCN5Q88RDgMLJ0cTSUC+9EMSgxcKNgwKg6Gzgg42aQYkFopoIPmYhZRxCRxq" +
                    "HilROCxm5P8voxgQk5KUkludnVp8ewZFWkJcfB9iPk0p1UV7zXqNHw2B8cRkNRGaTDF7XBpExNdZ" +
                    "qPEClOzU/vvvdE++n86gD4MUhktapNFrlXTDGqqRkmTS2kKCVOrFBgVQrOMZ+DTPSpBvFYUNMsz4" +
                    "q3pASfoB2VuV2t1fImEQjVLJXqhwK5UuHS6rISgEr4QCkvGFUk5XJ1SyuMQJGKiVEJQq6ixXqXLr" +
                    "pAKaULOaKgaFFIGHoUOncQmoNi4SDWPFqtXWGVCv9ng1Mjysr2+eKXdJvR51MlxVp9VoxdxJHSiR" +
                    "cqDmoChUyHE67U0q5WdnKZxuXgeqxiohxEIPgQsj2URPIGXEE0gSVVROrjZoRMBPCYZQiGM/ukjG" +
                    "R9OI0XBnYCBLAodvWPDCDFTALD9mpSCoMecEXBnBj2FmUn5mcnTMpIK0lOLMtNLszPLc7MBZTkZs" +
                    "8uLK/Izs5Ni0xNcbqtSDoLzSUo1z2RV+QN2NhsPkjBhPqPDZIWy6yUWjdCsEUFhapQimVSg1SncM" +
                    "Au65LFOKZhAp+boVDyoTb9Dn+g0ymk4s5BuElDUTKySTZBxYNUkiNhUvUDshbGAL5ZAI8Lh3WqNW" +
                    "6M3iWUmsQIGAzGdKWExYaAEvS06nlxCtFoEMikxPlafl+7PTIp3wrhDJuolojSvK86os0hFMgrJA" +
                    "1EpENrF0lidDhIv3qPw2CUmDcemE/pd+oDLALU/LdlTnhNMcKmTfTq/V242MhITVV63SMxBGVUsr" +
                    "YTKIYfyaREqIQH2bYOcluCCEZCSl2TPiDfCdg1jXsCh0oupCIMYBd8JEA0asGhoAi6URokm4sMw0" +
                    "ZNwqCmgB5eGUQkZwH5yrDUlzj4tLVCQkQgyFGemlmelz8jNKs9MK0pJLM/IKElNLU5LrsjOKM/NL" +
                    "M3NKJ+WNaM4Lz0pPhjnsps0CiGbS8FCL1HxmTa1NOg2BVzGDL/HZ9PLBQw2HScVMgJxVqtebNbyA" +
                    "W6rHFR0mxWxNrVZyQfbxppkDrUIAPZPi3PF2U16iYCFQfOJBDmD6oaUFwv0Am6q25FotyTZ7W612" +
                    "qGUAa0JFj0UtVcnj7eoIawSvEarRgy/dVV+ls+grsrLLE5KSHdaEwyaFJvJr1e6FaIUmyHVbo5VS" +
                    "1PN2kSTPMtn8hhEaYnmxARDfJwaxlapAOMyC2w6doJDFm+TBDxKuAe8qrx0Z066ze+RZSYb8rPsc" +
                    "E7LMCfFyabnewrTzBnx6ky/pjDZnOPXB6yiRLsEkQtoagkLAgcAKYRFTyERwvHYEBxmKogB5gBVp" +
                    "BwyOCDergPWMoOxOcnxecn+aSkJJWnJwH5ZanJhMKEiPb00JQU0KMvMqMjOLsnMLM3Jnp4/bXrht" +
                    "NK87Py0lLR4X7zN5NAqLTKxUSwQUfHgZZ9JoxNz2SQ0HRdFQoewyDF0QiSfgdPKOD6nNuAxwogda" +
                    "1OaVFyfXQUNUC/lQASDfgGnNQiDgFFvkghtSjEgzqy1K8QWqSDFZfWbdZkeR4bbnutzFwXj8/2xw" +
                    "G+KxZhiN9s1MimHysZFwxeQ6rAAioK+wkBsccCXH+cuTYwvCcYVBbwVqQnFib7cWHttSrA0zlmS6" +
                    "E2wyTKSrcGA3ukWm23sYIIqwSfTifF2FT3BJs3yGz16vkvHM2lo8V4Y50TBeFmyX5ESkBdkmTMSl" +
                    "T4LY2ZJbHmWNd0r8+kZTgUl0Sr0qOkIfDUw5MC0A4AyhEGISoKRNApGI8DYaAQE2XVyi1LsNqhgn" +
                    "ssIeLODvpzEuMLkYGl6ypgAxUlJhcFgfjBYkJxUmJIyLTl5WmpKYUZ6cU5mUXZGcVZGSXZmSVZab" +
                    "iA+YDU5VXLclPEyJjnWqDbJhXoZ321SA+ngRdhE4eTRsRwaik/HKARUq1YEJlCJ6DIemUtGMbBRA" +
                    "irRIBXaNSqrSmFTSN06BYQ1wCYXuTXyVLcN6hq4zo51AfvFif68OE+Gy5b2M93k6NBJP//0DTXps" +
                    "1iNoiw1Md1pLkqIzXZYAJl2Y67bWpjgyfM5Mt2WHK+tyG3PtuqmxdlTvdrsNEdyksloZovFaJOOa" +
                    "tZS5awoBTNSTo9Ss9EiUpiMGaMU4ePc0tF/mWTj6ZV4rQLtNNEsWqJTS67MdwP7ASsv3sR1qRlpL" +
                    "oWBj0VgvOHT8GwKhkvDiXiwX+ApxGgiLiImchIqajI6ajI2Zio2crKcR5eyKAYp3+80JXptqT4nh" +
                    "ECO35efmADsT/PH5fh82bGxmbGxGT6AL83nS43zpfvjk3weQGpcbHYwoTAlsSA5mB0fm+y0QZ0CU" +
                    "/EWnZhG4FNwNq3MrBIqRQwWGS1gEiGURGyikIEXMnE86qgMMAvIuBQZmyphUhVcpkEqNsvlJrlUJ" +
                    "+BC+VsVIoOYC+Ufb9Im2k0utSxOpwqYdEkWAwRLvE4VNOkgUkZL3ucEA2VAgpm1uXGeivRkl1QQN" +
                    "KhznFaAXy1NMWmA/RSrLtmqK4xzBZVSn4QLKRRvlfvdynifSq+nC4UxAk6YRUn2GbgeNVPLQkuIo" +
                    "RJimIaD5VJD1TKSRcsCK6ilWLUUBGDE2zkZCapMv8qtoQasgnSvyqNl2aRUFTMG4ZAxIACMmyAAn" +
                    "01iUjFAfXTExIjQceEhn0eEfB4VNj5yymejb6BgwSvAFKxBPosuzqoP2swQuJkeV7rLke6C05Xid" +
                    "CY5HYkOB2zaAdcoEj2uoNuZ6HYmeVzQ6DJ8nsyfAQJkxDqnJcWneGx+uxEcQMOGj9bmlHFMEsail" +
                    "UNrcBqUBjmPgY8W0HEwCmslXLNCYlbIgHeTTGaUSnUiEaSZRwv5rgT2nSopsA/UW0Q8n1bpVcsdM" +
                    "pFVzHcrJCAA5HuyzWSU8KCMIPpcWnnArMmJcwb0qlyvY5rXXuBzggBpFl1+vCvRpE61aQvjHAVuS" +
                    "7JW5teB3kwpD6PXMkAAo55h1TNjjbwMjyovVpvj0iTqhbEylolPlLPRPHq4lIc2qCgOA9tj4SZ5Z" +
                    "EWZ1mkp5gSHKNkjCzqkXh1bRovETUak1Mj/C0tMWi0gokDtAAAAAElFTkSuQmCC";
            advert.setImage(imgJson);

            Random r = new Random();

            int randType = r.nextInt(AdvertTypeEnum.values().length);
            advert.setType(AdvertTypeEnum.values()[randType]);

            int randStatus = r.nextInt(AdvertStatusEnum.values().length);
            advert.setStatus(AdvertStatusEnum.values()[randStatus]);
            advert.setRating(3.7f);
            answer.add(advert);
        }
        return answer;
    }

    @Override
    public void Delete(Advert entity) {

    }

    @Override
    public void Update(Advert entity) {

    }

    @Override
    public void Promote() {

    }

    @Override
    public void Degrade() {

    }

    @Override
    public Integer GetCount(IFilter filter) {
        return 4;
    }

    @Override
    public Integer GetCount() {
        return null;
    }

    @Override
    public void Add(IFilter filter) {

    }

    @Override
    public void Delete(IFilter filter) {

    }

    @Override
    public void Update(IFilter filter) {

    }
}
