package ru.diamis.you_to_me_i_to_you.services;

import android.util.Log;

import java.util.concurrent.ExecutionException;

import ru.diamis.you_to_me_i_to_you.DAL.YandexKassa.PayRequest;
import ru.diamis.you_to_me_i_to_you.DAL.YandexKassa.checked.ApiPaymentResponse;
import ru.diamis.you_to_me_i_to_you.DAL.YandexKassa.checked.ApiRefund;
import ru.diamis.you_to_me_i_to_you.Helpers.AsyncTaskManager;
import ru.diamis.you_to_me_i_to_you.Helpers.Serializer;
import ru.diamis.you_to_me_i_to_you.Helpers.SettingsHelper;

public class PayService {
    private String TAG = "Pay service";
    private String url;
    private String login;
    private String password;

    public PayService() {
        setUrl("https://payment.yandex.net/api/v3/");
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ApiPaymentResponse Pay(PayRequest payRequest) {
        String requestUrl = SettingsHelper.testUrl + "payments";
        String httpMethod = "POST";
        Log.d(TAG, "Try to request " + httpMethod + " - " + requestUrl);


        AsyncTaskManager taskManager = new AsyncTaskManager();
        taskManager.execute(httpMethod, requestUrl);

        String rawAnswer;
        try {
            Serializer serializer = new Serializer<>(payRequest.getClass());
            Log.d(TAG, serializer.Serialize(payRequest.getClass()));
            rawAnswer = taskManager.get();
            Log.d(TAG, rawAnswer);
            ApiPaymentResponse answer = (ApiPaymentResponse) serializer.Deserialize(rawAnswer);
            return answer;
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        } catch (ExecutionException e1) {
            e1.printStackTrace();
        }
        return new ApiPaymentResponse();
    }

    public ApiRefund GetRefundPayment(String refundId) {
        return new ApiRefund();
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
