package ru.diamis.you_to_me_i_to_you.services;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import ru.diamis.you_to_me_i_to_you.R;

public class AndroidService extends Service {
    private MediaPlayer mPlayer;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;

    }
    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(this, "Служба создана",
                Toast.LENGTH_SHORT).show();

        mPlayer.setLooping(false);
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if ((flags & START_FLAG_RETRY) == 0) {
            // TODO Если это повторный запуск, выполнить какие-то действия.
            Toast.makeText(this, "service was restarted", Toast.LENGTH_SHORT).show();
            return super.onStartCommand(intent,flags,startId);
        }
        else {
            // TODO Альтернативные действия в фоновом режиме.
            Toast.makeText(this, "run service in background mode", Toast.LENGTH_SHORT).show();

        }
        
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Служба остановлена",
                Toast.LENGTH_SHORT).show();
        mPlayer.stop();
    }
}
