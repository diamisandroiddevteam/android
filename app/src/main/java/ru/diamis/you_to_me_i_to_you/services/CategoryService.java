package ru.diamis.you_to_me_i_to_you.services;

import android.util.Log;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import ru.diamis.you_to_me_i_to_you.DAL.Category;
import ru.diamis.you_to_me_i_to_you.Filters.CategoryFilter;
import ru.diamis.you_to_me_i_to_you.Helpers.AsyncTaskManager;
import ru.diamis.you_to_me_i_to_you.Helpers.CategoryCollectionResponse;
import ru.diamis.you_to_me_i_to_you.Helpers.CollectionResponse;
import ru.diamis.you_to_me_i_to_you.Helpers.Serializer;
import ru.diamis.you_to_me_i_to_you.Helpers.SettingsHelper;
import ru.diamis.you_to_me_i_to_you.Interfaces.IFilter;
import ru.diamis.you_to_me_i_to_you.Interfaces.IService;

public  class CategoryService implements IService<Category> {
    public static final String TAG = "CategoryService";
    private final String  url= SettingsHelper.baseUrl;


    public CategoryService(){

    }
    @Override
    public void Add(Category entity) {

    }

    @Override
    public ArrayList<Category> Get(IFilter filter) {
        return null;
    }


    // @GET(url+"Sections")
    public ArrayList <Category>Get(CategoryFilter filter) {
        ArrayList <Category> answer = new ArrayList <Category>();

        String requestUrl = SettingsHelper.testUrl + "category";
        Log.d(TAG, "Try to request GET - " + requestUrl);

        String httpMethod = "GET";
        AsyncTaskManager taskManager = new AsyncTaskManager();
        taskManager.execute(httpMethod, requestUrl);

        String rawAnswer;
        try {
            rawAnswer = taskManager.get();
            Log.d(TAG, rawAnswer);

            Serializer serializer = new Serializer<>(CategoryCollectionResponse.class);
            serializer.setAttributeName("message");
            CollectionResponse<Category> categoryResponse = (CategoryCollectionResponse) serializer.Deserialize(rawAnswer);
            if (categoryResponse.isSuccess()) {

                return categoryResponse.Items;
            }
            else throw new ExecutionException(new Throwable());
        } catch (InterruptedException e) {
            Log.e(TAG, e.getMessage());
        } catch (ExecutionException e) {
            Log.e(TAG, e.getMessage());

        }
        return  answer;

    }

    @Override
    public void Delete(Category entity) {

    }

    @Override
    public void Update(Category entity) {

    }

    @Override
    public Integer GetCount(IFilter filter) {
        return null;
    }

    @Override
    public Integer GetCount() {
        return null;
    }

    @Override
    public void Add(IFilter filter) {

    }

    @Override
    public void Delete(IFilter filter) {

    }

    @Override
    public void Update(IFilter filter) {

    }
}
