package ru.diamis.you_to_me_i_to_you.services;

import android.util.Log;

import java.util.ArrayList;
import java.util.UUID;

import ru.diamis.you_to_me_i_to_you.DAL.User;
import ru.diamis.you_to_me_i_to_you.Filters.UserFilter;
import ru.diamis.you_to_me_i_to_you.Helpers.CryptoHelper;
import ru.diamis.you_to_me_i_to_you.Interfaces.IFilter;
import ru.diamis.you_to_me_i_to_you.Interfaces.IPromoteService;

public class UsersService extends BaseService<User> implements IPromoteService {
    public static final String TAG = "UsersService";
    ArrayList<User> Items = new ArrayList<>();
    public UsersService(){

        User user = new User();
        user.setUuid(UUID.randomUUID());
        user.setPhone("89111233323");
        user.setEmail("test@local");
        user.setLogin("test");
        user.setPassword("test12345");
        Items.add(user);
    }


    @Override
    public ArrayList<User> Get(IFilter filter) {

        return Items;
    }

    @Override
    public void Promote() {
        //TODO:GET host/users/promote?id=uuid
        String bufUrl = getUrl();
        bufUrl = makeBeautifulURL(bufUrl);
        bufUrl= bufUrl.concat("promote/");


    }

    private String makeBeautifulURL(String bufUrl) {
        if (!bufUrl.endsWith("/")){
            bufUrl= bufUrl.concat("/");
        }
        return bufUrl;
    }

    @Override
    public void Degrade() {
        //TODO:GET host/users/degrade?id=uuid
        String bufUrl = getUrl();
        bufUrl = makeBeautifulURL(bufUrl);
        bufUrl = bufUrl.concat("degrade/");



    }


    public boolean ResetPassword(UUID userId){
        //TODO:GET host/reset_password?user_id=uuid
        Log.d(TAG,"Try to reset user with : "+ userId);

        return true;
    }

    public boolean IsExist(UserFilter filter) {
        //TODO:POST host/isExist?login=test||email=test
        //
        return ((filter.getLogin().equalsIgnoreCase("test") || filter.getEmail().equalsIgnoreCase("test@local")) && filter.getPassword().equalsIgnoreCase(CryptoHelper.ToMd5("test12345")));
    }
}
