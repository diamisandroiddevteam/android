package ru.diamis.you_to_me_i_to_you.services;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import ru.diamis.you_to_me_i_to_you.DAL.Sale;
import ru.diamis.you_to_me_i_to_you.Interfaces.IFilter;
import ru.diamis.you_to_me_i_to_you.Interfaces.ISaleService;

public class SaleService implements ISaleService {
    List<Sale> sales = new ArrayList<Sale>();
    private Context mAppContext;
    public SaleService(){
        for (int i=0;i<=25;i++) {
            Sale sale = new Sale();
            sale.setUuid(UUID.randomUUID());
            sales.add(sale);

        }
    }
    public SaleService(Context appContext) {
        mAppContext = appContext.getApplicationContext();

    }


    public List<Sale> getItems() {
        return sales;
    }
    
    public void update(Sale sale) {
        String uuidString = sale.getUuid().toString();



    }


    public File getPhotoFile(Sale sale) {
        File externalFilesDir = mAppContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        if (externalFilesDir == null) {
            return null;
        }

        return new File(externalFilesDir, sale.getPhotoFilename());
    }

    private boolean deletePhotoFileFromStorage(String filename) {
        File externalFilesDir = mAppContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        return (externalFilesDir != null) && new File(externalFilesDir, filename).delete();
    }

    @Override
    public Integer GetCount(IFilter filter) {
        return null;
    }

    @Override
    public Integer GetCount() {
        return null;
    }

    @Override
    public void Add(IFilter filter) {

    }

    @Override
    public void Delete(IFilter filter) {

    }

    @Override
    public void Update(IFilter filter) {

    }

    @Override
    public void Add(Sale entity) {

    }

    @Override
    public ArrayList<Sale> Get(IFilter filter) {
        Sale sale = new Sale();
        return new ArrayList<Sale>();
    }

    @Override
    public void Delete(Sale entity) {

    }

    @Override
    public void Update(Sale entity) {

    }
}
