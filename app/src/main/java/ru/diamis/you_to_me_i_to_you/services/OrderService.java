package ru.diamis.you_to_me_i_to_you.services;

import java.util.ArrayList;
import java.util.UUID;

import ru.diamis.you_to_me_i_to_you.DAL.Order;
import ru.diamis.you_to_me_i_to_you.Enums.OrderStateEnum;
import ru.diamis.you_to_me_i_to_you.Interfaces.IFilter;
import ru.diamis.you_to_me_i_to_you.Interfaces.IOrderService;

public class OrderService implements IOrderService {
    private ArrayList<Order> Items = new ArrayList<Order>();
    public OrderService(){
        for (int i=0; i<25; i++) {
            Order item = new Order ();
            item.setPhone("+7-911-123-33-"+i);
            item.setUuid(UUID.randomUUID());
            item.setOrderState(OrderStateEnum.Opened);
            item.setPrice(5000);
            item.setDescription("4 кг пшеницы");
            Items.add(item);
        }
    }
    public Order RepeatOrder(Order order){
        return new Order();
    }

    @Override
    public Integer GetCount(IFilter filter) {
        return null;
    }

    @Override
    public Integer GetCount() {
        return null;
    }

    @Override
    public void Add(IFilter filter) {

    }

    @Override
    public void Delete(IFilter filter) {

    }

    @Override
    public void Update(IFilter filter) {

    }

    @Override
    public void Add(Order entity) {

    }

    @Override
    public ArrayList<Order> Get(IFilter filter) {
       return Items;
    }

    @Override
    public void Delete(Order entity) {

    }

    @Override
    public void Update(Order entity) {

    }
}
