package ru.diamis.you_to_me_i_to_you.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import ru.diamis.you_to_me_i_to_you.DAL.News;
import ru.diamis.you_to_me_i_to_you.Interfaces.IFilter;
import ru.diamis.you_to_me_i_to_you.Interfaces.INewsService;

public class NewsService implements INewsService {
    private ArrayList<News> Items = new ArrayList<News>();
    public NewsService(){
        for (int i = 0; i<25; i++) {
            News item = new News ();
            item.setTitle("Lorem Ipsum");
            item.setMessage("test"+i);
            item.setUuid(UUID.randomUUID());
            item.setPublicatedDate(new Date(2018, 5, 1, 12, 34, 46));
            Items.add(item);
        }

    }
    @Override
    public void Add(News entity) {

    }

    @Override
    public ArrayList<News> Get(IFilter filter) {

       return Items;
    }

    @Override
    public void Delete(News entity) {

    }

    @Override
    public void Update(News entity) {

    }

    @Override
    public Integer GetCount(IFilter filter) {
        return null;
    }

    @Override
    public Integer GetCount() {
        return null;
    }

    @Override
    public void Add(IFilter filter) {

    }

    @Override
    public void Delete(IFilter filter) {

    }

    @Override
    public void Update(IFilter filter) {

    }
}
