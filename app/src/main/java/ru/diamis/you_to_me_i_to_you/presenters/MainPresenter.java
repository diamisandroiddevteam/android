package ru.diamis.you_to_me_i_to_you.presenters;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import ru.diamis.you_to_me_i_to_you.activities.account.UserActivity;

public class MainPresenter implements IMainPresenter {
    private View _view;
    private IModel _model;

    private Context _context;

    public MainPresenter(View view) {
        _view = view;
        _model = new Model();
    }

    public MainPresenter(Context context) {
        _context = context;
        _model = new Model();
    }

    public void InitHeaderAppBar(View view) {
        _view = view;
        ProcessUserLogo(_view);
    }

    private void ProcessUserLogo(View view) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(_context, UserActivity.class);
                _context.startActivity(intent);
            }
        });
    }
}
