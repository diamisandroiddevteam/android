package ru.diamis.you_to_me_i_to_you.Interfaces;

import android.net.Uri;

public interface OnFragmentInteractionListener {
    void onFragmentInteraction(Uri uri);
}
