package ru.diamis.you_to_me_i_to_you.Interfaces;

import java.io.Serializable;

/**
 * Created by Tom on 14.03.2018.
 */

public interface IPromoteService extends Serializable {

    void Promote();
    void Degrade();
}

