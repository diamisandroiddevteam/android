package ru.diamis.you_to_me_i_to_you.Interfaces;

import ru.diamis.you_to_me_i_to_you.DAL.Adverts.Advert;

public interface IAdvertService extends IService<Advert>, IPromoteService {
}
