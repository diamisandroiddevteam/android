package ru.diamis.you_to_me_i_to_you.Interfaces;

import ru.diamis.you_to_me_i_to_you.DAL.IBaseEntity;

public interface ICallbacks<IBaseEntity>  {
    void onSelect(IBaseEntity sale);
    void onCreate(IBaseEntity sale);
    void onUpdate(IBaseEntity sale);
    void onDelete(IBaseEntity sale);
}
