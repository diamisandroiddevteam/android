package ru.diamis.you_to_me_i_to_you.Interfaces;

public interface IService<T> extends IRepository<T> {
    Integer GetCount(IFilter filter);
    Integer GetCount();

    void Add(IFilter filter);

    void Delete(IFilter filter);

    void Update(IFilter filter);
}
