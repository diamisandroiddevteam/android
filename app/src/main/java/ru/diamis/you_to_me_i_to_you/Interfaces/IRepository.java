package ru.diamis.you_to_me_i_to_you.Interfaces;

import java.util.ArrayList;

public interface IRepository<T> {
    void Add(T entity);
    ArrayList<T> Get(IFilter filter);
    void Delete(T entity);
    void Update(T entity);
}
