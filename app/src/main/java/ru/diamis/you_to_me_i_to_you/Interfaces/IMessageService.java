package ru.diamis.you_to_me_i_to_you.Interfaces;

import java.util.List;

import ru.diamis.you_to_me_i_to_you.DAL.Message;
import ru.diamis.you_to_me_i_to_you.Filters.MessageFilter;

public interface IMessageService extends IService<Message> {
    List<Message> GetChats(MessageFilter filter);
}
