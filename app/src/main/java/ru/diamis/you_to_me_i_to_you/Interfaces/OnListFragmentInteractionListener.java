package ru.diamis.you_to_me_i_to_you.Interfaces;

public interface OnListFragmentInteractionListener<BaseEntity> {
    void onListFragmentInteraction(BaseEntity item);
}


