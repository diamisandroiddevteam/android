package ru.diamis.you_to_me_i_to_you.Interfaces;

import java.util.UUID;

public interface IFilter {
     void SetId(UUID Id);
     UUID GetId();
}
