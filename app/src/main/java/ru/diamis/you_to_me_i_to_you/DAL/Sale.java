package ru.diamis.you_to_me_i_to_you.DAL;

import java.util.Date;
import java.util.UUID;

public class Sale extends CommonEntity {
    private String name;
    private String photoFilename;
    private String title;
    private String suspectName;
    private String suspectPhoneNumber;
    private boolean solved;
    private int id;
    private UUID uuid;
    public void setPhotoFilename(String photoFilename) {
        photoFilename =photoFilename;
    }
    public String getPhotoFilename() {
        return photoFilename;
    }

    public Date getDate() {
        return new Date();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public String  getSuspectName() {
        return suspectName;
    }

    public boolean isSolved() {
        return solved;
    }

    public void setSolved(boolean solved) {
        this.solved = solved;
    }

    public String getSuspectPhoneNumber() {
        return suspectPhoneNumber;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
}
