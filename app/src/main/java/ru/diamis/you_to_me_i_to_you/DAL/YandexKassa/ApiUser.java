package ru.diamis.you_to_me_i_to_you.DAL.YandexKassa;

import com.google.gson.annotations.SerializedName;

public class ApiUser {
    @SerializedName("shopid")
    private String shopId;
    @SerializedName("api_key")
    private String apiKey;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
