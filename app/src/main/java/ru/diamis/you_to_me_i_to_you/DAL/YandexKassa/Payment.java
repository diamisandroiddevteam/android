package ru.diamis.you_to_me_i_to_you.DAL.YandexKassa;

import com.google.gson.annotations.SerializedName;

public class Payment
{
    @SerializedName("id")
    private String id;
    @SerializedName("saved")
    private Boolean saved;
    @SerializedName("type")
    private String type;

    public Payment() {
        type = "bank_card";
    }
    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public Boolean getSaved()
    {
        return saved;
    }

    public void setSaved(Boolean saved)
    {
        this.saved = saved;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

}

