package ru.diamis.you_to_me_i_to_you.DAL.YandexKassa.checked;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.UUID;

import ru.diamis.you_to_me_i_to_you.DAL.YandexKassa.Payment;

public class ApiPaymentResponse implements Serializable {
    @SerializedName("amount")
    private ApiAmount apiAmount;
    @SerializedName("id")
    private UUID id;
    @SerializedName("paid")
    private String paid;
    @SerializedName("test")
    private Boolean test;
    @SerializedName("status")
    private String status;
    @SerializedName("confirmation")
    private ApiConfirmation confirmation;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("payment_method")
    private Payment payment;
    @SerializedName("recipient")
    private Recipient recipient;
    @SerializedName("metadata")
    private String metadata;

    public ApiAmount getApiAmount() {
        return apiAmount;
    }

    public void setApiAmount(ApiAmount apiAmount) {
        this.apiAmount = apiAmount;
    }

    public UUID getId () {
        return id;
    }

    public void setId (UUID id) {
        this.id = id;
    }

    public String getPaid () {
        return paid;
    }

    public void setPaid (String paid) {
        this.paid = paid;
    }

    public Boolean getTest() {
        return test;
    }

    public void setTest(Boolean test) {
        this.test = test;
    }

    public String getStatus () {
        return status;
    }

    public void setStatus (String status) {
        this.status = status;
    }

    public ApiConfirmation getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(ApiConfirmation confirmation) {
        this.confirmation = confirmation;
    }

    public String getCreatedAt () {
        return created_at;
    }

    public void setCreatedAt (String createdAt) {
        this.created_at = created_at;
    }

    public Payment getPayment () {
        return payment;
    }

    public void setPayment (Payment payment) {
        this.payment = payment;
    }

    public Recipient getRecipient () {
        return recipient;
    }

    public void setRecipient (Recipient recipient) {
        this.recipient = recipient;
    }

    public String getMetadata () {
        return metadata;
    }

    public void setMetadata (String metadata) {
        this.metadata = metadata;
    }


}
