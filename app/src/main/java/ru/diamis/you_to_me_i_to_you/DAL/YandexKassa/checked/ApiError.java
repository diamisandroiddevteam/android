package ru.diamis.you_to_me_i_to_you.DAL.YandexKassa.checked;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.UUID;

public class ApiError implements Serializable {
    @SerializedName("type")
    private String Type;
    @SerializedName("id")
    private UUID Id;
    @SerializedName("code")
    private String Code;
    @SerializedName("description")
    private String Description;

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public UUID getId() {
        return Id;
    }

    public void setId(UUID id) {
        Id = id;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }
}
