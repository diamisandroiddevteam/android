package ru.diamis.you_to_me_i_to_you.DAL.Adverts;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.webkit.URLUtil;

import com.google.gson.annotations.SerializedName;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import ru.diamis.you_to_me_i_to_you.DAL.Category;
import ru.diamis.you_to_me_i_to_you.DAL.PromoteService;
import ru.diamis.you_to_me_i_to_you.Enums.AdvertStatusEnum;
import ru.diamis.you_to_me_i_to_you.Enums.AdvertTypeEnum;
import ru.diamis.you_to_me_i_to_you.Enums.TariffEnums;
import ru.diamis.you_to_me_i_to_you.Helpers.PictureUtils;

public class Advert extends PromoteService implements Serializable {
    @SerializedName("user_id")
    private UUID ownerId;
    @SerializedName("category_id")
    private Category category;
    @SerializedName("subcategory_id")
    private Category subCategory;
    @SerializedName("title")
    private String Title;
    @SerializedName("phone")
    private String Phone;
    @SerializedName("tariff_id")
    private TariffEnums TariffType;
    @SerializedName("price")
    private double Price;
    @SerializedName("view_count")
    private int viewCount;
    @SerializedName("description")
    private String Description;
    @SerializedName("extendedDescription")
    private String ExtendedDescription;
    @SerializedName("longitude")
    private double longitude;
    @SerializedName("latitude")
    private double latitude;
    private String Address;
    @SerializedName("updated_at")
    private Date lastUpdateDate;
    @SerializedName("created_at")
    private Date createDate;
    @SerializedName("title_image")
    private Bitmap image;
    @SerializedName("user_id")
    private UUID userId ;
    @SerializedName("advert_type")
    private AdvertTypeEnum type;
    @SerializedName("advert_type")
    private AdvertStatusEnum status;
    @SerializedName("advert_type")
    private float rating;
    private UUID uuid;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public int getViewCount() {
        return viewCount;
    }

    public void setViewCount(int viewCount) {
        this.viewCount = viewCount;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }


    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }


    List<Advert> GetSame(int minPricePercent, int maxPricePercent) {
        return null;
    }

    public UUID getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(UUID ownerId) {
        this.ownerId = ownerId;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }


    public void setType(AdvertTypeEnum type) {
        this.type = type;
    }

    public String getExtendedDescription() {
        return ExtendedDescription;
    }

    public void setExtendedDescription(String extendedDescription) {
        ExtendedDescription = extendedDescription;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap value) {
        image = value;
    }

    public void setImage(String jsonStringOrUrl) {
        if (URLUtil.isValidUrl(jsonStringOrUrl)) {
            try {
                URL url = new URL(jsonStringOrUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                image = BitmapFactory.decodeStream(input);
            } catch (MalformedURLException e) {
                Log.e("Advert", e.getMessage());
            } catch (IOException e) {
                Log.e("Advert", e.getMessage());
            }
        }
        image = new PictureUtils().getBitmapFromString(jsonStringOrUrl);

    }

    public Category getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(Category subCategory) {
        this.subCategory = subCategory;
    }

    public TariffEnums getTariffType() {
        return TariffType;
    }

    public void setTariffType(TariffEnums tariffType) {
        TariffType = tariffType;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public Advert(){
        setUuid(UUID.randomUUID());
    }

    public AdvertStatusEnum getStatus() {
        return status;
    }

    public void setStatus(AdvertStatusEnum status) {
        this.status = status;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }
}
