package ru.diamis.you_to_me_i_to_you.DAL;

public abstract class TextEntity extends CommonEntity {
    private String message;
    private String title;
    public String getMessage() {
        return message;
    }

    public void setMessage(String inputMessage) {
        message = inputMessage;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
