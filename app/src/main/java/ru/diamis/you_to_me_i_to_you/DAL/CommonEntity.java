package ru.diamis.you_to_me_i_to_you.DAL;

import java.util.UUID;

public class CommonEntity implements IBaseEntity {
    public CommonEntity() {
        setUuid(UUID.randomUUID());
    }
    public void setId(int id) {
        this.id = id;
    }

    private int id;

    public Integer getId() {
        return id;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = UUID.fromString(uuid);
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    private UUID uuid;
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
