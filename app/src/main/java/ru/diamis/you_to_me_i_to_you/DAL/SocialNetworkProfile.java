package ru.diamis.you_to_me_i_to_you.DAL;

import ru.diamis.you_to_me_i_to_you.Enums.SocialNetworkEnum;

/**
 * Created by Tom on 14.03.2018.
 */

public class SocialNetworkProfile {
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public SocialNetworkEnum getDescription() {
        return description;
    }

    public void setDescription(SocialNetworkEnum description) {
        this.description = description;
    }

    public String getSocialNetworkId() {
        return socialNetworkId;
    }

    public void setSocialNetworkId(String socialNetworkId) {
        this.socialNetworkId = socialNetworkId;
    }

    private User user;
    private SocialNetworkEnum description;
    private String socialNetworkId;
}
