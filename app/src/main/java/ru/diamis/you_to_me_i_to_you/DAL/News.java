package ru.diamis.you_to_me_i_to_you.DAL;

import java.util.Date;

public class News extends TextEntity {

    private Date PublicatedDate;
    private Date ExpirationDate;


    public Date getPublicatedDate() {
        return PublicatedDate;
    }

    public void setPublicatedDate(Date publicatedDate) {
        PublicatedDate = publicatedDate;
    }

    public Date getExpirationDate() {
        return ExpirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        ExpirationDate = expirationDate;
    }


}
