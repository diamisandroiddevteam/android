package ru.diamis.you_to_me_i_to_you.DAL;

import java.util.UUID;

/**
 * Created by Tom on 12.03.2018.
 */

public class ExtendedEntity extends CommonEntity {
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private String email;



    public ExtendedEntity() {
        setUuid(UUID.randomUUID());
    }
}
