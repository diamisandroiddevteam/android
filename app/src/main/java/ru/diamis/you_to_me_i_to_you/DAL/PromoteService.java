package ru.diamis.you_to_me_i_to_you.DAL;

import ru.diamis.you_to_me_i_to_you.Enums.TariffEnums;
import ru.diamis.you_to_me_i_to_you.Interfaces.IPromoteService;

public abstract class PromoteService implements IPromoteService {

    public boolean isDeleted;

    private TariffEnums Tariff;

    public TariffEnums getTariff() {
        return Tariff;
    }

    public void setTariff(TariffEnums tariff) {
        Tariff = tariff;
    }

    @Override
    public void Promote() {
        switch (Tariff) {
            case TariffOne:
            case TariffTwo:
                Tariff.next();
                break;
            case TariffThree:
            case Unknown:
            default:
                throw new UnsupportedOperationException();
        }
    }

    @Override
    public void Degrade() {
        switch (Tariff) {
            case TariffTwo:
            case TariffThree:
                Tariff.last();
                break;
            case TariffOne:
            case Unknown:
            default:
                throw new UnsupportedOperationException();
        }
    }
}


