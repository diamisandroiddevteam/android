package ru.diamis.you_to_me_i_to_you.DAL.YandexKassa.checked;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ApiAmount implements Serializable {
    @SerializedName("value")
    private double value;
    @SerializedName("currency")
    private String currency;

    public ApiAmount() {
        currency = "RUB";
    }

    public double getValue()
    {
        return value;
    }

    public void setValue(double Value)
    {
        this.value = Value;
    }

    public String getCurrency ()
    {
        return currency;
    }

    public void setCurrency (String currency)
    {
        this.currency = currency;
    }

}
