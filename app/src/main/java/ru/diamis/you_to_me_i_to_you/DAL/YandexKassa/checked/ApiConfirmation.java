package ru.diamis.you_to_me_i_to_you.DAL.YandexKassa.checked;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ApiConfirmation implements Serializable {
    @SerializedName("confirmation_url")
    private String confirmation_url;
    @SerializedName("type")
    private String type;
    @SerializedName("return_url")
    private String return_url;

    public ApiConfirmation() {
        return_url = null;
    }
    public String getConfirmation_url ()
    {
        return confirmation_url;
    }

    public void setConfirmation_url (String confirmation_url)
    {
        this.confirmation_url = confirmation_url;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getReturn_url ()
    {
        return return_url;
    }

    public void setReturn_url (String return_url)
    {
        this.return_url = return_url;
    }
}