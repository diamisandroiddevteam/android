package ru.diamis.you_to_me_i_to_you.DAL.YandexKassa;

import com.google.gson.annotations.SerializedName;

import ru.diamis.you_to_me_i_to_you.DAL.YandexKassa.checked.ApiAmount;
import ru.diamis.you_to_me_i_to_you.DAL.YandexKassa.checked.ApiConfirmation;


public class PayRequest {
    @SerializedName("amount")
    private ApiAmount payment;
    @SerializedName("payment_method_data")
    private Payment apiPayMethodData;
    @SerializedName("confirmation")
    private ApiConfirmation apiConfirmation;

    public PayRequest() {
        payment = new ApiAmount();
        apiPayMethodData = new Payment();
        apiConfirmation = new ApiConfirmation();
        apiConfirmation.setType("redirect");
        apiConfirmation.setReturn_url("https://url.test");
    }
}
