package ru.diamis.you_to_me_i_to_you.DAL.YandexKassa.checked;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

class ApiPaymentMethodData implements Serializable {
    @SerializedName("payment_method_data")
    String type;
}
