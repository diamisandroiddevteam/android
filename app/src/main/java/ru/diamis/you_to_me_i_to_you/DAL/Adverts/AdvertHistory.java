package ru.diamis.you_to_me_i_to_you.DAL.Adverts;

import java.util.Date;

import ru.diamis.you_to_me_i_to_you.Enums.AdvertStatusEnum;

/**
 * Created by Tom on 14.03.2018.
 */

public class AdvertHistory {
    private Advert advert;
    private AdvertStatusEnum status;
    private Date statusChangeDate;

    public Advert getAdvert() {
        return advert;
    }

    public void setAdvert(Advert advert) {
        this.advert = advert;
    }

    public AdvertStatusEnum getStatus() {
        return status;
    }

    public void setStatus(AdvertStatusEnum status) {
        this.status = status;
    }

    public Date getStatusChangeDate() {
        return statusChangeDate;
    }

    public void setStatusChangeDate(Date statusChangeDate) {
        this.statusChangeDate = statusChangeDate;
    }
}
