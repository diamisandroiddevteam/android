package ru.diamis.you_to_me_i_to_you.DAL;

import java.util.UUID;

public class Message extends TextEntity {

    private UUID userId;


    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }
}

