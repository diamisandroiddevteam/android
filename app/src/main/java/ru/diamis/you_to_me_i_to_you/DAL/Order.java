package ru.diamis.you_to_me_i_to_you.DAL;

import java.util.Date;

import ru.diamis.you_to_me_i_to_you.Enums.OrderStateEnum;

public class Order extends CommonEntity {
    public Order(){
        super();
        OrderState = OrderStateEnum.Opened;
        createDate = new Date();
        description = "";
        price = 0.0;

    }
    private double price;
    private User user;
    private OrderStateEnum OrderState;
    private Date createDate;
    private String description;
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public OrderStateEnum getOrderState() {
        return OrderState;
    }

    public void setOrderState(OrderStateEnum orderState) {
        OrderState = orderState;
    }

    public void setPrice(double value){
        price = value;
    }
    public double getPrice() {
        return price;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
