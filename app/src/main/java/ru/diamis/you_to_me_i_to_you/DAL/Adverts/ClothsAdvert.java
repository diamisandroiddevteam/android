package ru.diamis.you_to_me_i_to_you.DAL.Adverts;

public class ClothsAdvert extends Advert{
    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    private String Type;
     private String Size;
     private int Color;
     private int Year;

    public String getSize() {
        return Size;
    }

    public void setSize(String size) {
        Size = size;
    }

    public int getColor() {
        return Color;
    }

    public void setColor(int color) {
        Color = color;
    }

    public int getYear() {
        return Year;
    }

    public void setYear(int year) {
        Year = year;
    }
}
