package ru.diamis.you_to_me_i_to_you.DAL;

import java.io.Serializable;

import ru.diamis.you_to_me_i_to_you.Enums.UserRoles;
import ru.diamis.you_to_me_i_to_you.Enums.UserTypeEnum;
import ru.diamis.you_to_me_i_to_you.Enums.UsersLevel;
import ru.diamis.you_to_me_i_to_you.Helpers.CryptoHelper;
import ru.diamis.you_to_me_i_to_you.Interfaces.IApiClient;

/**
 * Created by Tom on 14.03.2018.
 */

public class User extends CommonEntity implements IApiClient, Serializable {
    private String Login;
    private String Password;
    private String Email;
    private boolean isDeleted;

    public User() {
       userRoles = UserRoles.Guest;
   }
    private UserRoles userRoles;
    private UsersLevel usersLevel;
    private UserTypeEnum userType;


    @Override
    public String getUrl() {
        return null;
    }

    @Override
    public void setUrl(String url) {

    }

    @Override
    public void setLogin(String login) {
        Login = login;
    }

    @Override
    public String getLogin() {
        return Login;
    }

    @Override
    public void setPassword(String password) {
        //TODO make clear with hash algorythm
        password = CryptoHelper.ToMd5(password);
        Password = password;

    }

    @Override
    public String getPassword() {
        return Password;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public boolean isDeleted() {
        return isDeleted;
    }
}
