package ru.diamis.you_to_me_i_to_you.DAL;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Tom on 14.03.2018.
 */

public class Category extends ExtendedEntity implements Serializable {
    @SerializedName("ID")
    private int Id;
    @SerializedName("NAME")
    private String Name;
    @SerializedName("PARENT")
    private String Parent;
    @SerializedName("SORT")
    private int Sort;
    @SerializedName("ACTIVE")
    private Boolean Active;
    // @JsonIgnoreProperties(value = { "intValue" })
    @SerializedName("CHILD")
    public List<Category> categories;

    public Category(int i) {
        Id = 1;

    }

    public Category(){
        categories = new LinkedList<Category>();
    }

    public Category(String name){
        categories = new LinkedList<Category>();
        Name = name;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getParent() {
        return Parent;
    }

    public void setParent(String parent) {
        Parent = parent;
    }

    public int getSort() {
        return Sort;
    }

    public void setSort(int sort) {
        Sort = sort;
    }

    public Boolean getActive() {
        return Active;
    }

    public void setActive(Boolean active) {
        Active = active;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
}
