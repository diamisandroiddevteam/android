package ru.diamis.you_to_me_i_to_you.Enums;

/**
 * Created by Tom on 14.03.2018.
 */

    public enum UserRoles {
        Guest,Moderator,Company,Unknown{
            @Override
            public ru.diamis.you_to_me_i_to_you.Enums.UserRoles next() {
                return null;
            }
    };

        public ru.diamis.you_to_me_i_to_you.Enums.UserRoles next() {
            return values()[ordinal() + 1];
        }
}

