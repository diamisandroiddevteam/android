package ru.diamis.you_to_me_i_to_you.Enums;

public enum OrderStateEnum {
    Opened,
    Processed,
    Confirmed,
    Paid,
    Canceled
}
