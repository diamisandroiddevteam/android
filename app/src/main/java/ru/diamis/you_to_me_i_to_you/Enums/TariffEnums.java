package ru.diamis.you_to_me_i_to_you.Enums;

import java.io.Serializable;

public enum TariffEnums implements Serializable {
    Unknown{
        public TariffEnums last() {
            return null;
        }
    },TariffOne,TariffTwo,TariffThree{
        @Override
        public TariffEnums next() {
            return null;
        }
    };

    public TariffEnums next() {
        return values()[ordinal() + 1];
    }
    public TariffEnums last() {
        return values()[ordinal() - 1];
    }
}
