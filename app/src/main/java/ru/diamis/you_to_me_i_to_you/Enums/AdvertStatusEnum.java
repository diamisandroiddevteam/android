package ru.diamis.you_to_me_i_to_you.Enums;

/**
 * Created by Tom on 14.03.2018.
 */

public enum AdvertStatusEnum {
    Created, OnModerate,Confirmed,Paid,Canceled,Finalized,Deleted
}
