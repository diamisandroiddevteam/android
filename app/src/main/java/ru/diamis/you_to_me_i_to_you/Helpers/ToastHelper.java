package ru.diamis.you_to_me_i_to_you.Helpers;

import android.content.Context;
import android.content.ContextWrapper;
import android.widget.Toast;
import android.content.Context;


public class ToastHelper {

     public static void ShowMessage(Context context, boolean isLong, String Message){
         int duration = isLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT;
         Toast toast = Toast.makeText(context, Message, duration);
         toast.show();
     }

    public static void ShowMessage(Context context, String Message) {
        ShowMessage(context,true, Message);
    }

    public static void ShowMessage(Context context, int id) {
        ShowMessage(context,true, id);
    }

    public static void ShowMessage(Context context, boolean isLong, int id) {
        int duration = isLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, id, duration);
        toast.show();
    }

}
