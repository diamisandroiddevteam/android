package ru.diamis.you_to_me_i_to_you.Helpers;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.diamis.you_to_me_i_to_you.DAL.Category;

public class CollectionResponse<T>extends ItemResponce<T>{
    @SerializedName("message")
    public ArrayList<T> Items;
    @SerializedName("totalCount")
    public Integer totalCount;

    public CollectionResponse() {
        Items = null;
        Status = "false";
    }
    }

