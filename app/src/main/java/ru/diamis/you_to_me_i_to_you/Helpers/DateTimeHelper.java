package ru.diamis.you_to_me_i_to_you.Helpers;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimeHelper {

    private final static String dateTimeFormat = "yyyy-MM-dd HH:mm:ss";

    public DateTimeHelper() {
    }

    public static String getDateString(Context context, Date date) {
        java.text.DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(context);
        return  dateFormat.format(date);
    }

    public static String getTimeString(Context context, Date date) {
        java.text.DateFormat timeFormat = android.text.format.DateFormat.getTimeFormat(context);
        return timeFormat.format(date);
    }

    public static String getDateTimeString(Context context, Date date) {
        return getDateString(context, date) + " " + getTimeString(context, date);
    }

    public static Boolean is24HourFormat(Context context) {
        return android.text.format.DateFormat.is24HourFormat(context);
    }

    public static String getFormattedDate(Date date) {
        SimpleDateFormat df = new SimpleDateFormat(dateTimeFormat);
        return df.format(date);
    }
}
