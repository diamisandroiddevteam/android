package ru.diamis.you_to_me_i_to_you.Helpers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

import ru.diamis.you_to_me_i_to_you.DAL.Category;

/**
 * Created by Tom on 26.03.2018.
 */

public class Serializer<T> implements JsonDeserializer<T> {
    public Class<T> typeParameterClass;
    private String AttributeName;
    private Gson gson;
    private String rawData;
    public Serializer(Class<T> typeParameterClass){
        this.typeParameterClass = typeParameterClass;
        gson =
                new GsonBuilder()
                        .registerTypeAdapter((new CollectionResponse<Category>()).getClass(), this)
                        .create();
    }
    public String Serialize(T obj){
        return gson.toJson(obj);
    }

    public T Deserialize(String input){
        rawData = input;
        return gson.fromJson(input, this.typeParameterClass);
    }

    public static <T> List<T> mapFromJsonArray(String respInArray, Type listType) {
        List<T> ret = new Gson().fromJson(respInArray, listType);
        return ret;
    }

    public static <T> List<T> stringToArray(String s, Class<T[]> clazz) {
        T[] arr = new Gson().fromJson(s, clazz);
        return Arrays.asList(arr); //or return Arrays.asList(new Gson().fromJson(s, clazz)); for a one-liner
    }

    @Override
    public T deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonElement content = json.getAsJsonObject().get(AttributeName);
        return gson.fromJson(content, typeOfT);
    }

    public void setAttributeName(String attributeName) {
        AttributeName = attributeName;
    }
}
