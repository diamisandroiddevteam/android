package ru.diamis.you_to_me_i_to_you.Helpers;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;

public class AsyncTaskManager extends AsyncTask<String, Void, String> {
    public final static String TAG = "AsyncTaskManager";

    @Override
    protected String doInBackground(String... strings) {

        HttpClient httpClient = new HttpClient();
        try {
            String rawResponse = httpClient.sendGet(strings[1]);
            Log.d(TAG, "Raw Response = ".concat(rawResponse));
            return rawResponse;
        } catch (IOException e) {
            Log.e(TAG, "Error in "+strings[0]+" ".concat(strings[1]) + e.getStackTrace());
            return "";
        }

    }

    protected void onPostExecute(Boolean result) {

    }
}