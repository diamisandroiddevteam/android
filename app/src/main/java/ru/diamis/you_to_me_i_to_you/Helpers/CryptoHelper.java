package ru.diamis.you_to_me_i_to_you.Helpers;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import ru.diamis.you_to_me_i_to_you.Enums.HashAlgorythType;

public class CryptoHelper {
    public static String ToMd5(String s) {
        try {
            MessageDigest digest = getMessageDigest(HashAlgorythType.Md5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();
            return convertToHex(messageDigest);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
private  static MessageDigest getMessageDigest(HashAlgorythType hashAlgoryth)throws NoSuchAlgorithmException {
    String HashAlgorythm = "";
        switch (hashAlgoryth) {
        case Md5: HashAlgorythm = "MD5" ;break;
        case Sha1: HashAlgorythm = "SHA-1";break;
        default: throw new NoSuchAlgorithmException();
    }

    return java.security.MessageDigest.getInstance(HashAlgorythm);
}
    private static String convertToHex(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (byte b : data) {
            int halfbyte = (b >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
                halfbyte = b & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static String ToSHA1(String text) {
        try {
            MessageDigest md = getMessageDigest(HashAlgorythType.Sha1);
            byte[] textBytes = text.getBytes("iso-8859-1");
            md.update(textBytes, 0, textBytes.length);
            byte[] sha1hash = md.digest();
            return convertToHex(sha1hash);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}