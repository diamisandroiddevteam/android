package ru.diamis.you_to_me_i_to_you.Helpers;

import java.util.jar.Attributes;

public class SettingsHelper extends Attributes{
    public static boolean isDebug = false;
    public static String  apiVersion= "1.1";
    public static final String productionUrl = "http://tebemne.diamis.ru/api/"+apiVersion+"/";
    public static final String testUrl = "http://tebemne.diamis.ru/tools/json/";
    public static final String baseUrl = isDebug?testUrl:productionUrl;
    public static final int lowBorderSameAdverts = -15;
    public static final int highBorderSameAdverts = 20;


    public static int AdvertPrveiewLength=64;
}
