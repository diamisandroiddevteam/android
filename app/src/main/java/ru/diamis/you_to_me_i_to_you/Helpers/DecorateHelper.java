package ru.diamis.you_to_me_i_to_you.Helpers;

import android.content.res.Resources;

import ru.diamis.you_to_me_i_to_you.Enums.OrderStateEnum;
import ru.diamis.you_to_me_i_to_you.R;

public class DecorateHelper {
    public static String getOrderState(OrderStateEnum orderState) {
        switch (orderState) {
            case Opened:
                return Resources.getSystem().getString(R.string.order_opened);
            case Confirmed:
                return Resources.getSystem().getString(R.string.order_confirmed);
            case Processed:
                return Resources.getSystem().getString(R.string.order_processed);
            case Paid:
                return Resources.getSystem().getString(R.string.order_paid);
            case Canceled:
                return Resources.getSystem().getString(R.string.order_canceled);
            default:
                return Resources.getSystem().getString(R.string.order_unsuspected);
        }
    }
}
