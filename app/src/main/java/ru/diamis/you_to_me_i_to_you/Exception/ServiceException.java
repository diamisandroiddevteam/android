package ru.diamis.you_to_me_i_to_you.Exception;

public class ServiceException extends Exception {
    public ServiceException(String concat) {
        super(concat);
    }
}
