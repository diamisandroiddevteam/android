package ru.diamis.you_to_me_i_to_you;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import ru.diamis.you_to_me_i_to_you.DAL.Adverts.Advert;
import ru.diamis.you_to_me_i_to_you.Filters.AdvertFilter;
import ru.diamis.you_to_me_i_to_you.services.AdvertService;
@RunWith(AndroidJUnit4.class)
public class AdvertServiceTest {
    @Test
    public void GetAdvertsRequest_FilterIsEmpty_ShouldReturnNotEmptyCollection() {
        AdvertService advertService = new AdvertService();
        ArrayList<Advert> answer = advertService.Get(new AdvertFilter());
        Assert.assertNotEquals(0,answer.size());

    }
}
