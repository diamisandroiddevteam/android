package ru.diamis.you_to_me_i_to_you;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import ru.diamis.you_to_me_i_to_you.DAL.YandexKassa.PayRequest;
import ru.diamis.you_to_me_i_to_you.DAL.YandexKassa.checked.ApiPaymentResponse;
import ru.diamis.you_to_me_i_to_you.services.PayService;

@RunWith(AndroidJUnit4.class)
public class PayServiceTests {
    @Test
    public void PayOrderTest_DataIsCorrect_ShouldReturn() {
        PayService payService = new PayService();
        payService.setLogin("54401");
        payService.setPassword("test_Fh8hUAVVBGUGbjmlzba6TB0iyUbos_lueTHE-axOwM0");
        PayRequest payRequest = new PayRequest();
        ApiPaymentResponse responce = payService.Pay(payRequest);
        Assert.assertEquals(responce.getStatus(), "pending");

    }

}
