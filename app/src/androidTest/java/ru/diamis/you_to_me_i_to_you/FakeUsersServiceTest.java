package ru.diamis.you_to_me_i_to_you;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import ru.diamis.you_to_me_i_to_you.Filters.UserFilter;
import ru.diamis.you_to_me_i_to_you.services.UsersService;

@RunWith(AndroidJUnit4.class)
public class FakeUsersServiceTest {
    @Test
    public void testIsExistRequestTest_UserIsExist_ShouldReturnTrue() {
        UsersService service = new UsersService();
        UserFilter filter = new UserFilter();
        filter.setLogin("test");
        filter.setEmail("test@local");
        filter.setPassword("test12345");
        Boolean answer = service.IsExist(filter);
        Assert.assertTrue (answer);

    }

    @Test
    public void IsExistRequestTest_UserIsNotExist_ShouldReturnFalse() {
        UsersService service = new UsersService();
        UserFilter filter = new UserFilter();
        filter.setLogin("bla");
        filter.setEmail("blabla@local");
        filter.setPassword("test123451231312");
        Boolean answer = service.IsExist(filter);
        Assert.assertFalse (answer);

    }

}
