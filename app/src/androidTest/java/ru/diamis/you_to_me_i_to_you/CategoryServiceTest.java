package ru.diamis.you_to_me_i_to_you;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import ru.diamis.you_to_me_i_to_you.DAL.Category;
import ru.diamis.you_to_me_i_to_you.Filters.CategoryFilter;
import ru.diamis.you_to_me_i_to_you.services.CategoryService;
@RunWith(AndroidJUnit4.class)
public class CategoryServiceTest {

    public static final String TAG = "CategoryServiceTest";

    @Test
    public void GetCategoriesRequest_DataIsCorrect_ShouldReturnNotEmptyCollection() {
        CategoryService categoryService = new CategoryService();
        ArrayList<Category> answer = categoryService.Get(new CategoryFilter());

        Assert.assertEquals(28, answer.size());
        Category category = answer.get(0);
        Log.d(TAG, category.getName());
        // System.out.print("size: "+answer.size());

//        for (Category category:stringToArray()
//             ) {
//            Log.d(TAG,category.getName());
//        }
    }
}
