package ru.diamis.you_to_me_i_to_you;

import org.junit.Assert;
import org.junit.Test;

import ru.diamis.you_to_me_i_to_you.Helpers.CryptoHelper;

public class CryptoHelperTest {
    @Test
    public void MD5HastTest_DataIsCorrect_ShouldReturnExpectedHash() {
       String input = "test";
       String answer = CryptoHelper.ToMd5(input);
        Assert.assertNotNull(answer);
        Assert.assertTrue(answer.equalsIgnoreCase("098f6bcd4621d373cade4e832627b4f6"));
    }

    @Test
    public void SHA1Test_DataIsCorrect_ShouldReturnExpectedHash(){
        String input = "test";
        String answer = CryptoHelper.ToSHA1(input);
        Assert.assertNotNull(answer);
        Assert.assertTrue(answer.equalsIgnoreCase("A94A8FE5CCB19BA61C4C0873D391E987982FBBD3"));
    }

}




