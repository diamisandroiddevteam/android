package ru.diamis.you_to_me_i_to_you;

import android.content.res.Resources;

import org.junit.Assert;
import org.junit.Test;

import ru.diamis.you_to_me_i_to_you.Enums.OrderStateEnum;
import ru.diamis.you_to_me_i_to_you.Helpers.DecorateHelper;

public class DecorateHelperTest {
    @Test
    public void GetOrderTest_ItemIsKnow_ShouldReturnExpectedString() {
        DecorateHelper helper = new DecorateHelper();
        Resources resources = Resources.getSystem();
        String answer = helper.getOrderState(OrderStateEnum.Opened);
        Assert.assertEquals(answer.toString(), resources.getString(R.string.order_opened));
    }
}
