package ru.diamis.you_to_me_i_to_you;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.UUID;

import ru.diamis.you_to_me_i_to_you.DAL.Category;
import ru.diamis.you_to_me_i_to_you.DAL.YandexKassa.checked.ApiPaymentResponse;
import ru.diamis.you_to_me_i_to_you.Helpers.CollectionResponse;
import ru.diamis.you_to_me_i_to_you.Helpers.Serializer;

public class SerializerTests {
    public static final String TAG = "SerializerTests";
    @Test
    public void Serialize_DataIsPrimitiveType_ShouldReturnExpectedString() {
        UUID id = UUID.randomUUID();
        Serializer serializer = new Serializer<UUID>(UUID.class);
        String answer = serializer.Serialize(id);
        System.out.println(answer);
        Assert.assertEquals(answer.toString(),answer);
    }
    @Test
    public void Serialize_DataIsObject_ShouldReturnExpectedString() {
        ApiPaymentResponse apiPayment = new ApiPaymentResponse();
        UUID id = UUID.randomUUID();
    apiPayment.setId(id);
        Serializer serializer = new Serializer<ApiPaymentResponse>(ApiPaymentResponse.class);
    String answer = serializer.Serialize(apiPayment);
    System.out.println(answer);
    Assert.assertEquals("{\"id\":\""+id+"\"}",answer);
    }

    @Test
    public void Serialize_DataIsPrimitiveCollection_ShouldReturnExpectedString() {
        ArrayList<UUID> apiPayments = new ArrayList<UUID>();
        int n=3;
        for (int i=0;i<n;i++) {
            apiPayments.add( UUID.randomUUID());
        }
        Serializer serializer = new Serializer<ArrayList<ApiPaymentResponse>>((Class<ArrayList<ApiPaymentResponse>>) apiPayments.getClass());
        String answer = serializer.Serialize(apiPayments);
        System.out.println(answer);

        Assert.assertEquals("[\""+apiPayments.get(0)+"\",\""+apiPayments.get(1)+"\",\""+apiPayments.get(2)+"\"]",answer);
    }

    @Test
    public void Serialize_DataIsObjectWithInnerCollection_ShouldReturnExpectedString() {
        Category category = new Category();
        category.setName("test Name");
        category.setCategories(new ArrayList<Category>());
        int n=3;
        for (int i=0;i<n;i++) {
            Category internalCategory = new Category();
            internalCategory.setId(i);
            category.categories.add(internalCategory);
        }
        Serializer serializer = new Serializer<>(Category.class);
        String answer = serializer.Serialize(category);
        System.out.println(answer);
        Assert.assertEquals("{\"ID\":0," +
                "\"NAME\":\"test Name\"," +
                "\"SORT\":0," +
                "\"IBLOCK_ID\":0," +
                "\"ITEMS\":" +
                    "[" +
                    "{\"ID\":0," +
                    "\"SORT\":0," +
                    "\"IBLOCK_ID\":0," +
                    "\"ITEMS\":[]}," +
                    "{\"ID\":1," +
                    "\"SORT\":0," +
                    "\"IBLOCK_ID\":0," +
                    "\"ITEMS\":[]}," +
                    "{\"ID\":2," +
                    "\"SORT\":0," +
                    "\"IBLOCK_ID\":0," +
                    "\"ITEMS\":[]}]}",answer);


    }
    @Test
    public void Deserialize_DataIsPrimitiveType_ShouldReturnExpectedObject(){
        String input = "90666bfc-09d6-4d36-a742-862b18ddb917";
        Serializer serializer = new Serializer<UUID>(UUID.class);
        UUID answer = (UUID)serializer.Deserialize(input);
        System.out.println(answer);
        Assert.assertEquals(answer,UUID.fromString(input));
    }

    @Test
    public void Deserialize_CategoryCollectionResponse_ShouldReturnExpectedObject() {
        CollectionResponse<Category> categoryCollectionResponse = new CollectionResponse<Category>();
        categoryCollectionResponse.Items = new ArrayList<Category>();

        for (int i = 0; i < 3; i++) {
            Category category = new Category();
            categoryCollectionResponse.Items.add(category);
        }
        Serializer serializer = new Serializer<CollectionResponse<Category>>((Class<CollectionResponse<Category>>) (new CollectionResponse<Category>()).getClass());
        String rawData = serializer.Serialize(categoryCollectionResponse);
        Log.d(TAG, rawData);
        CollectionResponse<Category> answer = (CollectionResponse<Category>) serializer.Deserialize(rawData);
        System.out.println(answer);
        Assert.assertEquals(answer.Items.size(), 3);

        for (Category category : answer.Items
                ) {

        }
    }
    @Test
    public void Deserialize_DataIsObject_ShouldReturnExpectedObject() {
        String id = "90666bfc-09d6-4d36-a742-862b18ddb917";
        String input = "{\"id\":\""+id+"\"}";
        Serializer serializer = new Serializer<ApiPaymentResponse>(ApiPaymentResponse.class);
        ApiPaymentResponse answer = (ApiPaymentResponse) serializer.Deserialize(input);
        System.out.println(input);
        Assert.assertEquals(answer.getId(),UUID.fromString(id));

    }

    @Test
    public void Deserialize_DataIsPrimitiveCollection_ShouldReturnExpectedObject() {


        String input = "[\"b59f26fb-520b-4373-933f-ee83d3891686\",\"29dd05a0-374e-4ce9-92ea-5f2d235f4a01\",\"d709e424-b731-4a64-88ce-cf5f837224e8\"]";
        Serializer serializer = new Serializer<ArrayList<UUID>>((Class<ArrayList<UUID>>) (new ArrayList<UUID>()).getClass());
        ArrayList<UUID> answer = (ArrayList<UUID>) serializer.Deserialize(input);
        System.out.println(answer);


    }

    @Test
    public void Deserialize_DataIsObjectWithInnerCollection_ShouldReturnExpectedObject() {
        String input = "{\"ID\":0," +
                "\"NAME\":\"test Name\"," +
                "\"SORT\":0," +
                "\"IBLOCK_ID\":0," +
                "\"ITEMS\":" +
                "[" +
                "{\"ID\":0," +
                "\"SORT\":0," +
                "\"IBLOCK_ID\":0," +
                "\"ITEMS\":[]}," +
                "{\"ID\":1," +
                "\"SORT\":0," +
                "\"IBLOCK_ID\":0," +
                "\"ITEMS\":[]}," +
                "{\"ID\":2," +
                "\"SORT\":0," +
                "\"IBLOCK_ID\":0," +
                "\"ITEMS\":[]}]}";

        Serializer serializer = new Serializer<Category>(Category.class);
        Category answer = (Category) serializer.Deserialize(input);
        System.out.println(input);
        Assert.assertEquals(answer.getName(), "test Name");
        Assert.assertEquals(answer.getCategories().size(), 3);

        for (Category categoryIter : answer.getCategories()) {
            Log.d("Name= ", categoryIter.getName());
        }
//        Assert.assertEquals(answer.getCategories().get(0).getId(),0);
//        Assert.assertEquals(answer.getCategories().get(1).getId(),1);
//        Assert.assertEquals(answer.getCategories().get(2).getId(),2);
    }

    public static <T> ArrayList<T> stringToArray(String s) {
        Gson g = new Gson();
        Type listType = new TypeToken<ArrayList<T>>() {
        }.getType();
        ArrayList<T> list = g.fromJson(s, listType);
        return list;
    }
    @Test
    public void DeserializeBigObject_ShouldNoThrow() {

        String input = TestData.BigDataForDeserializeInput_Part1 + TestData.BigDataForDeserializeInput_Part2;


        Serializer serializer = new Serializer<>((Class<CollectionResponse<Category>>) (new CollectionResponse<Category>()).getClass());
        serializer.setAttributeName("message");
        CollectionResponse<Category> answer = (CollectionResponse<Category>) serializer.Deserialize(input);
        System.out.println(input);
        Assert.assertEquals(answer.isSuccess(), true);
        Assert.assertEquals(answer.Items.size(), 28);
        Gson g = new Gson();
        Type listType = new TypeToken<ArrayList<Category>>() {
        }.getType();
        ArrayList<Category> list = g.fromJson(input, listType);
        Assert.assertEquals(list.size(), 28);
        for (Category category : list
                ) {

        }
    }

}




